<%-- 
    Document   : addNewUser
    Created on : 28 Nov, 2014, 10:25:55 AM
    Author     : nisha
--%>
<%@page import="java.net.URLEncoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>  
<%@page import="org.gadgeon.Config"%>  
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>WaterBit</title>
	<link rel="stylesheet" href="css/main.css" >
	<link rel="stylesheet" href="css/bstrap.css" >
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
          <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
         <link rel="shortcut icon" href="images/favicon .ico" type="image/x-icon">
        <link rel="icon" href="images/favicon .ico" type="image/x-icon">
        <script src="assets/nprogress/nprogress.js"></script>
	<link href="assets/nprogress/nprogress.css" rel="stylesheet" />
        <script src="js/alertify.min.js"></script>
	
	 <style>
        .sensoredit{
             
        }
    .sensoredit:hover {
  cursor:pointer;
          }
          #dialogoverlay{
	display: none;
	opacity: .8;
	position: fixed;
	top: 0px;
	left: 0px;
	background: #FFF;
	width: 100%;
	z-index: 10;
}
#dialogbox{
	display: none;
	position: fixed;
	background: #103D5F;
	border-radius:7px; 
	width:320px;
	z-index: 10;
}
#dialogbox > div{ background:#FFF; margin:8px; }
//#dialogbox > div > #dialogboxhead{ background: #103D5F; font-size:19px; padding:10px; color:#CCC; }
#dialogbox > div > #dialogboxbody{ background:#FFFAF0; padding:20px; color:#000; text-align:center;font-size: small; }
#dialogbox > div > #dialogboxfoot{ background: #103D5F; padding:2px; text-align:center; }

          

        </style>
       
      <style>
    .myButton {
	-moz-box-shadow: 0px 10px 14px -7px #276873;
	-webkit-box-shadow: 0px 10px 14px -7px #276873;
	box-shadow: 0px 10px 14px -7px #276873;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
	background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
	background-color:#599bb3;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	border-radius:4px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Times New Roman;
	font-size:12px;
	font-weight:bold;
	padding:2px 10px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}
.myButton:active {
	position:relative;
	top:1px;
}

</style>  <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
        <script src="js/alertify.min.js"></script>
    
   
   
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<% if (session.getAttribute("userid") != null) { 
         
int userid = (Integer)session.getAttribute( "userid" );
if(userid==1)
{
%>
 
   <script type="text/javascript">
        
        
        function enableButton(val)
        {
            if(val.checked==true)
            {
                document.getElementById("btnEdit").disabled = false;
                document.getElementById("btnEdit").style.visibility="visible";
            }
           
        }
        
        
        var checkflag = "false";
        function check(field) 
        {
            
            if (checkflag == "false") 
            {
                if(isNaN(document.myform.elements["rbox[]"].length))
                {
                    document.myform.elements["rbox[]"].checked = true;
                    checkflag = "true";
               
                }
                else
                {
                    for (i = 0; i < field.length; i++) 
                    {
                        field[i].checked = true;
                    }
                    checkflag = "true";
                }
                document.getElementById("btnEdit").style.visibility = "hidden";
                return "Clear All";
            }
            else 
            {
                if(isNaN(document.myform.elements["rbox[]"].length))
                {
                    document.myform.elements["rbox[]"].checked = false;
                    checkflag = "false";
        
                }
                else
                {
                    for (i = 0; i < field.length; i++) 
                    {
                        field[i].checked = false;
                    }
                checkflag = "false";
            }
            document.getElementById("btnEdit").style.visibility = "visible";
            return "Select All";
          
        }
        }
        function OnButton1()
        {
             var count=0;
             var uid = 0;
             var uname ;
             var uinfo;
             var uemail;
             var uphone = 0;
             var row = 0;
             
             if(isNaN(document.myform.elements["rbox[]"].length))
             {
                 uid = document.myform.elements["rbox[]"].value;
                 count =1;
             }
             else
             {
                for (i = 0; i < document.myform.elements["rbox[]"].length; i++)
                {
                   if(document.myform.elements["rbox[]"][i].checked == true)
                   {
		       
                      count++;
		      
                   }
                }
            }
                if(count>1)
                {
                    document.getElementById("btnEdit").style.visibility = "hidden";
                    alertify.alert("please select one user to edit");
                }
                else if(count<1)
                {
                     alertify.alert("please select atleast one user to edit");
                 }
                else if(count == 1)
                {
                    document.getElementById("btnEdit").style.visibility = "visible";
                    for (i = 0; i < document.myform.elements["rbox[]"].length; i++)
                    {
                       if(document.myform.elements["rbox[]"][i].checked == true)
                       {
                         uid = document.myform.elements["rbox[]"][i].value;
                         row = i;
                       }
                   }   
                
                
           
             
             
             uname = document.getElementById("myTable").rows[row+1].cells[1].innerHTML;
             uinfo = document.getElementById("myTable").rows[row+1].cells[2].innerHTML;
             uemail = document.getElementById("myTable").rows[row+1].cells[3].innerHTML;
             uphone = document.getElementById("myTable").rows[row+1].cells[4].innerHTML;
             if (window.XMLHttpRequest)
             {
                  xmlhttp=new XMLHttpRequest();
             }
             else
             {
                  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
             }
             xmlhttp.onreadystatechange=function()
             {
                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                  document.getElementById("result1").innerHTML=xmlhttp.responseText; 
                }
             }
             xmlhttp.open("POST","ajax.jsp?userid="+uid+"&info="+uinfo+"&email="+uemail+"&phone="+uphone,true);
             xmlhttp.send();
         }
        }
        function OnButton2()
        {
     
            var count=0;
             
             if(isNaN(document.myform.elements["rbox[]"].length))
             {
                 uid = document.myform.elements["rbox[]"].value;
                 count =1;
             }
             else
             {
                for (i = 0; i < document.myform.elements["rbox[]"].length; i++)
                {
                   if(document.myform.elements["rbox[]"][i].checked == true)
                   {

                      count++;
                   }
                }
               
            }
           //  alert(count);

                if(count >= 1)
                {
                    
                    
                               alertify.confirm("Do you want to delete sensor?", function (e) {
                            if (e) {
                                // user clicked "ok"
                                document.myform.action = "deleteUserAction.jsp?uname=" + op;
                                document.myform.submit();
                                return true;
                            } else {
                                window.location.href = addNewSensor.jsp;
                                return false;
                                // user clicked "cancel"
                            }
                        });
             
                }
                else if(count < 1)
                {
                  alertify.alert("Please select atleast one user");
                   return false;
                }
      
    }

        function validatetxtbox()
        {
            var uname = document.getElementById("user").value;
            var psswd1;
            var psswd2;
            psswd1 =document.getElementById("passwd").value;
            psswd2 =document.getElementById("passwd1").value;
            var usrInfo = document.getElementById("usrInfo").value;
            var userEmail = document.getElementById("userEmail").value;
            var usrPhone = document.getElementById("usrPhone").value;
            
            if(uname == "")
            {
                document.getElementById('une').innerHTML = "* User name field should not be empty";
                document.getElementById("user").focus();
                return(false);
            }
            else if(psswd1 == "")
            {
                document.getElementById('une').innerHTML = "* Please type a password";
                document.getElementById("passwd").focus();
                return(false);
            }
            else if(psswd1 != psswd2)
            {
                 document.getElementById('une').innerHTML = "* Password Must be equal";
                 document.getElementById("passwd").value ="";
                 document.getElementById("passwd1").value="";
                 document.getElementById("passwd").focus();
                 return(false);
            }
            else if(usrInfo == "")
            {
                document.getElementById('une').innerHTML = "* User information required";
                document.getElementById("usrInfo").focus();
                return(false);
            }
            else if(userEmail == "")
            {
                document.getElementById('une').innerHTML = "* Email id required";
                document.getElementById("userEmail").focus();
                return(false);
            }
            else if(usrPhone == "")
            {
                document.getElementById('une').innerHTML = "* Phone number required";
                document.getElementById("usrPhone").focus();
                return(false);
            }

            else
            {
                    var atpos = userEmail.indexOf("@");
                    var dotpos = userEmail.lastIndexOf(".");
                   var re1 = /^[-\w\.\$@]{1,30}$/; 
                   var re2 = /^[-\w\.\$@\*\!]{1,30}$/;
                   var re3 =/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/; 
                    if(!re1.test(uname))
                    {
                       // alert("hi");
                        document.getElementById('une').innerHTML = "* Username must contain only letters, numbers and @ and .!"; 
                        document.getElementById("user").focus(); 
                        return(false);
                    }
                     if(!re2.test(psswd1)) 
                     { 
                         document.getElementById('une').innerHTML = "* Password must contain only letters, numbers and special characters!"; 
                         document.getElementById("passwd").focus(); 
                         return(false);
                     }

                   
                    if(isNaN(usrPhone)) 
                     { 
                        // alert("haiii");
                        document.getElementById('une').innerHTML = "* Phone number must contain only numbers";
                        document.getElementById("usrPhone").focus();
                        return(false);
                    }
                    if(!re3.test(usrPhone))
                    {
                          document.getElementById('une').innerHTML = "* Phone number must be 10 digits";
                        document.getElementById("usrPhone").focus();
                        return(false);
                    }
                    
                return(true);
            }

        }
	
	
	
    </script>
    <script type="text/javascript">
        function function1()
        {
            //alert("hi");
            
            
             var usrInfo = document.getElementById("usrInfo").value;
            //var userEmail = document.getElementById("userEmail").value;
            var usrPhone = document.getElementById("usrPhone").value;
            var re3 =/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/; 
            
            if(usrInfo == "")
            {
               // alert(usrInfo);
                document.getElementById('une').innerHTML = "* User information required";
                document.getElementById("usrInfo").focus();
                return(false);
            }
            else if(usrPhone == "")
            {
                document.getElementById('une').innerHTML = "* Phone number required";
                document.getElementById("usrPhone").focus();
                return(false);
            }
            if(isNaN(usrPhone)) 
                     { 
                        // alert("haiii");
                        document.getElementById('une').innerHTML = "* Phone number must contain only numbers";
                        document.getElementById("usrPhone").focus();
                        return(false);
                    }
                    if(!re3.test(usrPhone))
                    {
                          document.getElementById('une').innerHTML = "* Phone number must be 10 digits";
                        document.getElementById("usrPhone").focus();
                        return(false);
                    }
                    document.getElementById("user").disabled = false;
                return(true);
        }
	
	
	function CustomAlert(){
			this.render = function(dialog){
				var winW = window.innerWidth;
			    var winH = window.innerHeight;
				var dialogoverlay = document.getElementById('dialogoverlay');
			    var dialogbox = document.getElementById('dialogbox');
				dialogoverlay.style.display = "block";
			    dialogoverlay.style.height = winH+"px";
				dialogbox.style.left = (winW/2) - (550 * .5)+"px";
			    dialogbox.style.top = "100px";
			    dialogbox.style.display = "block";

			      document.getElementById('dialogboxbody').innerHTML = '<img src="./images/Warning-icon.png"; width=30px; height=30px; /> '+dialog;
			       //document.getElementById('dialogboxbody').innerHTML = '<img src="' + imgName + '" />';
			    //document.getElementById('dialogboxbody').innerHTML = dialog;
				document.getElementById('dialogboxfoot').innerHTML = '<button onclick="Alert.ok()">OK</button>';

			}
			this.ok = function(){
				document.getElementById('dialogbox').style.display = "none";
				document.getElementById('dialogoverlay').style.display = "none";
				shortURL = top.location.href.substring(0, top.location.href.indexOf('?'));
				window.location.href = shortURL;
			}

		}
var Alert = new CustomAlert();  
    </script>


	
    </head>
    <body>
        <script>
  NProgress.set(0.0);
               NProgress.start();
              var mnc=0;
        </script>
	<div id="dialogoverlay"></div>
		<div id="dialogbox">
		  <div>
		    <div id="dialogboxbody"></div>
		    <div id="dialogboxfoot"></div>
		  </div>  
		</div>
	    
	<div class="container">
	    <%
	    String[] values = request.getParameterValues("val");
              if(values!=null)
              {
                 if(values[0].equals("1"))
                {
      %>  
      <script>
               alertify.alert("Already registered..!");
      </script>
      <%
               }   
             else if(values[0].equals("2"))
              {%>  
                <script>
                  alertify.alert("User registerd succefully..!");
                  </script>
                  <%
               }   
             else if(values[0].equals("3"))
              {%>  
                <script>
                  alertify.alert("Deleted..!");
                  </script>
                  <%
               }   
             else if(values[0].equals("4"))
              {%>  
                <script>
                  alertify.alert("Can not delete..!");
                  </script>
                  <%
               }   
               else if(values[0].equals("5"))
              {%>  
                <script>
                  alertify.alert("Edited Successfully..!");
                  </script>
                  <%
               }   
             else if(values[0].equals("6"))
              {%>  
                <script>
                  alertify.alert("Can not edit..!");
                  </script>
                  <%
               } 
                 %>   
            <script>
                shortURL = top.location.href.substring(0, top.location.href.indexOf('?'));
                window.location.href = shortURL;
            </script> 
            <%   
               
              }
              %>    
	      <%@include file="./adminHeader.jsp" %>
	      <div class="row">
		  <div id="result1">
		 <div class="col-lg-6">
		    <div class="panel panel-primary">
                            <div class="panel-heading">
                                Add New User
                            </div>
                            <form action="registerUserAction.jsp" method="post" onsubmit="return(validatetxtbox())">
                               
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                      
                                        <tbody>
                                            <tr class="success">
                                                <td>User Name </td><td><input type="text" name="user" id="user"></td>
                                            </tr>
                                            <tr class="success">
                                                <td>Password </td><td> <input type="password" name="passwd" id="passwd"/></td>
                                            </tr>
                                            <tr class="success">
                                                <td>Re-enter Password </td><td> <input type="password" name="passwd1" id="passwd1"/></td>
                                            </tr>
                                             <tr class="success">
                                                <td>User Info </td><td> 
                                                    <!--<input type="text" name="usrInfo" id="usrInfo"/>-->
                                                    <textarea name="usrInfo" id="usrInfo" cols="30" rows="4"></textarea>
                                                    </td>
                                            </tr>
                                            <tr class="success">
                                                <td>Email ID </td><td> <input type="email" name="userEmail" id="userEmail"/></td>
                                            </tr><tr class="success">
                                                <td>Phone Number </td><td> <input type="text" name="usrPhone" id="usrPhone"/></td>
                                            </tr>
                                            <tr class="success">
                                                <td ></td>
                                                <td><input class="myButton" type="submit" value="Submit"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" class="myButton" value="Clear"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <font color='red'> <div id="une"> </div> </font>
                                </div>
                                    </div>
			    </form>
                        </div> 
		 </div>
		  </div>
		   <%
   try{
    URL jsonpage = new URL("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getUserList");
    URLConnection urlcon = jsonpage.openConnection();
   
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    DocumentBuilder db = dbf.newDocumentBuilder();

    Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getUserList");
    NodeList userids =  doc.getElementsByTagName("userid");
    NodeList username = doc.getElementsByTagName("username");
    NodeList userinfo = doc.getElementsByTagName("userInfo");
    NodeList email = doc.getElementsByTagName("email");
    NodeList phone = doc.getElementsByTagName("phone");
%>
		 <div class="col-lg-6">
		     <div class="panel panel-primary">
			 <div class="panel-heading">
                                Users
                            </div>
                            <div class="panel-body">
                                  <form name="myform" id="myform" method="post">
                                <div class="table-responsive">
                                   <table class="table" id="myTable">
                                        <input type=button class="myButton" value="Select All" onClick="this.value=check(this.form)">
                                      <thead><th>Users</th><th>User Name</th><th>User Info</th><th>Email</th><th>Phone Number</th></thead>
                                        <tbody>
                                             <%
                                                for(int i=0;i<=username.getLength()-1;i++)
                                                {
                                                  
                                                    %>
                                            <tr class="success">
                                               
                                               
                                                <td><input type="checkbox" name="rbox[]" id="rbox[]" onclick="enableButton(this);" value="<%=userids.item(i).getFirstChild().getNodeValue()%>"></td><td><%=username.item(i).getFirstChild().getNodeValue()%></td><td><%=userinfo.item(i).getFirstChild().getNodeValue()%></td><td><%=email.item(i).getFirstChild().getNodeValue()%></td><td><%=phone.item(i).getFirstChild().getNodeValue()%></td>
                                               
                                            </tr>
                                             <% 
                                               }
                                             
              }catch(Exception e)
              {
                  
              }
                                             %>
                                            <tr class="success">
                                                <td><INPUT type="button" class="myButton" id="btnEdit" value="Edit User" name="btnEdit" onclick="OnButton1();"></td>
                                                <td><INPUT type="button" class="myButton" value="Delete User" name="btnDelete" onclick="OnButton2();"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="uid">
                                <input type="hidden" name="uname">
                                <input type="hidden" name="uinfo">
                                <input type="hidden" name="uemail">
                                <input type="hidden" name="uph">
                                </div>
                                </form>
                            </div>
	     </div>
	</div>
	     <script src="js/bootstrap.min.js"></script> 
              <script>
            if(mnc==0)
    {
	NProgress.set(1);
    }
	mnc=1;
            
        </script>
    </body>
    <% 
                                                }
      }
	else
	{
	    response.sendRedirect("index.jsp");
		    }
		%>
</html>


