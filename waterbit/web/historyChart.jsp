<%-- 
    Document   : historyChart
    Created on : 4 Dec, 2014, 3:00:21 PM
    Author     : nisha
--%>
<%@page import="org.gadgeon.Config"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>WaterBit</title>
	<link rel="stylesheet" href="css/main.css" >
	<link rel="stylesheet" href="css/bstrap.css" >
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<style>
	    .col-center {
		position: absolute;
		top: 0;
		left: 10%;
		//margin-left: -12.5%;
		margin-right: 10%;
	      }
              
              
               ul.nav li.dropdown:hover > ul.dropdown-menu {
		display: block;    
	    }
	    .caret-right {
		display: inline-block;
		width: 0;
		height: 0;
		margin-left: 5px;
		vertical-align: middle;
		border-left: 4px solid;
		border-bottom: 4px solid transparent;
		border-top: 4px solid transparent;
	    }
              
              
	</style>
    </head>
    <body style="height: 100%;">
	    <div class="container">
	    <%if (session.getAttribute("userid") != null) { 

		int userid = (Integer)session.getAttribute( "userid" );
		    if(userid==1)
		    {
		    %> 

			  <%@include file="./adminHeader.jsp" %>
			  <%
		    }
		    else{
		      %> 

			  <%@include file="./userHeader.jsp" %>
			  <%  
		    }
			  //  }
       %>
	    <div class="row-fluid">
		<div class="col-lg-12">
		    <div class="panel panel-primary" style="height: 760px; background-color: #dff0d8">
			<div class="panel-heading">
                              History Chart
                            </div>
                            <div class="panel-body">
                                <%@ include file="historypanelNew_1.jsp" %>
			    </div>
		    </div>
		</div>
		
	    
	    <%
		    }
		else
	    {
		response.sendRedirect("index.jsp");
			}
		    %>
	</div>
	</div>
        <!--<div class="col-lg-12" style="background: none repeat scroll 0% 0% #103D5F; color: #FFF; height:30px; text-align: right">Powered by Gadgeon Smart Systems Pvt. Ltd.</div>-->
    </body>
</html>