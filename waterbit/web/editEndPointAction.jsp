<%-- 
    Document   : editEndPointAction
    Created on : 10 Feb, 2015, 1:44:47 PM
    Author     : nisha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.io.*"%>
<%@page import="java.util.ArrayList"%>

<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%> 
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            int userid;
            if (session.getAttribute("userid") != null) {
                userid = (Integer) session.getAttribute("userid");
            } else {
                userid = 0;
            }
            String enddeviceName = request.getParameter("sName");
            String enddeviceinfo = URLEncoder.encode(request.getParameter("sinfor"), "UTF-8");
            String latitude = request.getParameter("sLatitude");
            String longitude = request.getParameter("sLongitude");
             String reportInterval = request.getParameter("reportInterval");
            String location = URLEncoder.encode(request.getParameter("city1"), "UTF-8");
            try {
                URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + enddeviceName + "/" + enddeviceinfo + "/" + latitude + "/" + longitude + "/" + location + "/"+reportInterval+"/updateEndPoint");
                out.println(jsonpage);
                URLConnection urlcon = jsonpage.openConnection();
                BufferedReader buffread = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));
                String strTemp = buffread.readLine();
                out.println(strTemp);
                if (strTemp.equals("nok")) {
                    response.sendRedirect("addNewEndDevice.jsp?val=31");
                } else {
                    response.sendRedirect("addNewEndDevice.jsp?val=32");
                }
            } catch (Exception e) {

            }
        %>
        <%!
            private String normalize(String mac) {

                return mac.replaceAll("(\\.|\\,|\\:|\\-)", "");
            }
        %>
    </body>
</html>
