    
<div id="result">
    <%@page import="org.w3c.dom.Node"%>
    <%@page import="org.w3c.dom.NodeList"%>
    <%@page import="org.w3c.dom.Document"%>
    <%@page import="javax.xml.parsers.DocumentBuilder"%>
    <%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
    <%@page import="java.net.URLConnection"%>
    <%@page import="java.io.InputStreamReader"%>
    <%@page import="java.net.URL"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="org.gadgeon.Config"%>
    <%
        String sensortype = request.getParameter("sensortype");
        String graphprofile = request.getParameter("graphprofile");

        URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + graphprofile + "/GetProfileByName");
        URLConnection urlcon = jsonpage.openConnection();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + graphprofile + "/GetProfileByName");

        NodeList xLabel = doc.getElementsByTagName("xlabel");
        NodeList yLabel = doc.getElementsByTagName("ylabel");
        NodeList min = doc.getElementsByTagName("min");
        NodeList max = doc.getElementsByTagName("max");

        jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype + "/GetProfileByType");
        urlcon = jsonpage.openConnection();

        dbf = DocumentBuilderFactory.newInstance();

        db = dbf.newDocumentBuilder();

        doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype + "/GetProfileByType");

        NodeList profileName = doc.getElementsByTagName("graphprofile");
        
    %>
    <div class="panel panel-default"  style="border-bottom: none; background-color: #dff0d8;">
        <div class="panel-heading">
            Graph Profile
        </div>
        <form action="addNewGraphProfileAction.jsp" method="post"  onsubmit="return(validatetxtbox())">
            <div class="panel-body" >
                <div class="table-responsive"><table class="table">

                        <%
                            if (profileName.getLength() != 0) {

                        %>
                        <tbody>
                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Profile </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="graphprofile" id="graphprofile" onchange="return(graphProfileByName())"  >
                                        <%                                                 for (int i = 0; i <= profileName.getLength() - 1; i++) {

                                         if( profileName.item(i).getFirstChild().getNodeValue().compareTo(graphprofile)!=0 )
                                        {
                                           %>  
                                            
                                        <option value="<%=profileName.item(i).getFirstChild().getNodeValue()%>"
                                                selected> <%=profileName.item(i).getFirstChild().getNodeValue()%>
                                        </option>
                                         <% 
                                        }
                                            }
                                        %>
                                          <option value="<%=graphprofile%>"
                                                selected> <%=graphprofile%>
                                        </option>
                                    </select></td>
                            </tr>

                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">X-Label</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="xLabel" id="xLabel" readonly="true" value="<%=xLabel.item(0).getFirstChild().getNodeValue()%>"/></td>
                            </tr>

                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Y-Label</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="yLabel" id="yLabel" readonly="true" value="<%=yLabel.item(0).getFirstChild().getNodeValue()%>"/></td>
                            </tr>

                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Value Range - Min</td> <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type= "number" name="min" id="min" readonly="true" value="<%=min.item(0).getFirstChild().getNodeValue()%>"/></td></td>
                            </tr>
			    <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Value Range - Max</td> <td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type= "number" name="max" id="max" readonly="true" value="<%=max.item(0).getFirstChild().getNodeValue()%>"/></td>
                            </tr>

                        </tbody>
        <%
                            }
                        %>
                    </table>
                    <font color='red'> <div id="une"> </div> </font>
                </div>

            </div>
        </form>
    </div>
