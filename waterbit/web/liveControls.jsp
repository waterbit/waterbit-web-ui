
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gadgeon IOT</title>
        <link rel="stylesheet" href="css/main.css" >
        <link rel="stylesheet" href="css/bstrap.css" >
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" >
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

        <style>
            .myButton {
                -moz-box-shadow: 0px 10px 14px -7px #276873;
                -webkit-box-shadow: 0px 10px 14px -7px #276873;
                box-shadow: 0px 10px 14px -7px #276873;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
                background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
                background-color:#599bb3;
                -moz-border-radius:3px;
                -webkit-border-radius:3px;
                border-radius:4px;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-family:Times New Roman;
                font-size:10px;
                font-weight:bold;
                padding:2px 10px;
                text-decoration:none;
                text-shadow:0px 1px 0px #3d768a;
            }
            .myButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
                background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
                background-color:#408c99;
            }
            .myButton:active {
                position:relative;
                top:1px;
            }


            #dialogoverlay{
                display: none;
                opacity: .8;
                position: fixed;
                top: 0px;
                left: 0px;
                background: #FFF;
                width: 100%;
                z-index: 10;
            }
            #dialogbox{
                display: none;
                position: fixed;
                background: #103D5F;
                border-radius:7px; 
                width:320px;
                z-index: 10;
            }
            #dialogbox > div{ background:#FFF; margin:8px; }
            //#dialogbox > div > #dialogboxhead{ background: #103D5F; font-size:19px; padding:10px; color:#CCC; }
            #dialogbox > div > #dialogboxbody{ background:#FFFAF0; padding:20px; color:#000; text-align:center;font-size: small; }
            #dialogbox > div > #dialogboxfoot{ background: #103D5F; padding:2px; text-align:center; }



        </style>






        <script type="text/javascript">
            var checkflag = "false";
            var checkCond = 0;
            var valCheck = 0;
            var devicestatus = 0;

            setInterval(function () {
                $.get("getRelayList.jsp", function (data11, status) {

                    // alert(data11.trim()); 
                    if (checkCond == 0) {
                        var img = document.getElementsByClassName("imge");
                        var uname;
                        var sensorstatus = data11.split(";");
                        // alert(img[0]);
                        for (var i = 0; i < sensorstatus.length; i++)
                        {
                            var senname = sensorstatus[i].substring(0, sensorstatus[i].indexOf(":"));
                            var status = sensorstatus[i].substring(sensorstatus[i].indexOf(":") + 1);

                            for (var j = 0; j < img.length; j++)
                            {
                                //alert(im)
                                if (img[j].alt.trim() == senname.trim())
                                {
                                    img[j].height = 25;
                                    img[j].width = 50;
                                    if (status.trim() == "0")
                                    {
                                        img[j].src = "images/off.png";
                                    }
                                    else
                                        img[j].src = "images/on.png";
                                    break;
                                }
                            }
                        }
                        valCheck = 0;
                    }
                    if (checkCond != 0)
                    {
                        if (valCheck >= 9)
                            checkCond = 0;
                        valCheck += 3;
                    }
                    // livedata(3,data11);

                });
            }, 2500);
            function check(field)
            {

                if (checkflag == "false")
                {
                    if (isNaN(document.myform.elements["rbox[]"].length))
                    {
                        document.myform.elements["rbox[]"].checked = true;
                        checkflag = "true";

                    }
                    else
                    {
                        for (i = 0; i < field.length; i++)
                        {
                            field[i].checked = true;
                        }
                        checkflag = "true";
                    }
                    return "Clear All";
                }
                else
                {
                    if (isNaN(document.myform.elements["rbox[]"].length))
                    {
                        document.myform.elements["rbox[]"].checked = false;
                        checkflag = "false";

                    }
                    else
                    {
                        for (i = 0; i < field.length; i++)
                        {
                            field[i].checked = false;
                        }
                        checkflag = "false";
                    }
                    return "Select All";

                }
            }

            function OnButton1()
            {

                var count = 0;

                if (isNaN(document.myform.elements["rbox[]"].length))
                {
                    //  uid = document.myform.elements["rbox[]"].value;
                    count = 1;
                }
                else
                {
                    for (i = 0; i < document.myform.elements["rbox[]"].length; i++)
                    {
                        if (document.myform.elements["rbox[]"][i].checked == true)
                        {

                            count++;
                        }
                    }
                }

                if (count >= 1)
                {
                    document.myform.action = "clearAlarm.jsp"

                    // document.Form1.target = "_blank";    // Open in a new window

                    document.myform.submit();             // Submit the page

                    return true;
                }
                else if (count < 1)
                {
                    alert("Please select atleast one user");
                    return false;
                }
            }

            function funAjaxCall()
            {
                var count = 0;
                var uid = 0;
                var row = 0;
                var sentype;
                for (i = 0; i < document.form1.elements["rbox1[]"].length; i++)
                {
                    if (document.form1.elements["rbox1[]"][i].checked == true)
                    {

                        count++;
                    }
                }

                if (count == 1)
                {
                    for (i = 0; i < document.form1.elements["rbox1[]"].length; i++)
                    {
                        if (document.form1.elements["rbox1[]"][i].checked == true)
                        {
                            //uid = document.form1.elements["rbox1[]"][i].value;
                            row = i;

                        }
                    }
                }



                sentype = document.getElementById("senFrm").rows[row + 1].cells[2].innerHTML;

                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("result1").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("POST", "ajax__info.jsp?sentype=" + sentype, true);
                xmlhttp.send();



            }

            function checkRelay(img)
            {


                //alert(img.alt);

                //var sens =document.myform.elements.rbox1[];
                //alert(sens);
                var val;
                checkCond = 1;
                if (img.src.indexOf("images/off.png") > 0)
                {
                    devicestatus = 1;
                    //img.src="images/off.jpg";
                    val = "ON";
                }
                else
                        // img.src="images/on.jpg";
                        {
                            val = "OF";
                            valCheck = -3;
                            devicestatus = 2;
                        }
                /*    if(val=="ON")
                 img.src="images/on.jpg";
                 else
                 img.src="images/off.jpg";*/
                img.src = "images/loading.gif";
                img.width = 25;
                img.height = 25;
                $.get("relayOn.jsp?status=" + val + "&relay=" + img.alt, function (data, status) {
                    //alert("Data: " + data + "\nStatus: " + status);

                    /* if(data.trim()=="0")
                     img.src="images/off.jpg";
                     else 
                     img.src="images/on.jpg";*/
                    //alert(data);
                });
            }
            function rowSelect(row, sensor)
            {
                //row.className="";
                var tr = document.getElementsByClassName("mysensor");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = "#E6E6FA";

                }
                row.style.background = "#48D1CC";
                //var str=row.cells[0].innerHTML.trim();
                // row.cells[0].innerHTML=str.substr(0,str.length-1)+" checked >"
                //alert(str.substr(0,str.length-1));
                //  rewrite(sensor);
            }

            var Count = 0;
            function ImageRefresh() {
                document.CameraImage.src = "http://192.168.0.144:8080/SnapshotJPEG?Resolution=640x480&RPeriod=65535&Size=STD&PresetOperation=Move&SendMethod=1&Language=0&Quality=Standard&Count=" + Count;
                Count++;
            }
        </script>


    </head>


    <body>
        <%
            String[] values = request.getParameterValues("val");
            if (values != null) {
                if (values[0].equals("3")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    Alert.render("Old password is incorrect..!");
            }
        </script>
        <%
                                 }

                             }%>
        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>  
        </div>

        <div class="container">
            <%
                if (session.getAttribute("userid") != null) {

                    int userid = (Integer) session.getAttribute("userid");
                    if (userid == 1) {
            %> 

            <%@include file="./adminHeader.jsp" %>
            <%
            } else {
            %> 

            <%@include file="./userHeader.jsp" %>
            <%
                }
            %>
            <div class="row">
                <div class="col-lg-5" >
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            My Relay Controls
                        </div>

                        <div class="panel-body">
                            <form name="form1" id="form1" method="post">
                                <table class="table" id="senFrm">
                                    <thead>
                                        <tr>

                                            <th>Sensor Name</th>
                                            <th>Sensor Type</th>
                                            <th>Location</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                        <%
                                            try {

                                                URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
                                                URLConnection urlcon = jsonpage.openConnection();

                                                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

                                                DocumentBuilder db = dbf.newDocumentBuilder();

                                                Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
                                                NodeList sensorname = doc.getElementsByTagName("sensorname");
                                                NodeList sensortype = doc.getElementsByTagName("sensortype");
                                                NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
  // NodeList alarmcount = doc.getElementsByTagName("alarmcount");

                                                for (int i = 0; i <= sensorname.getLength() - 1; i++) {
                                                     
                     
                     
                                                    if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")) {
                             
                  

                                        %>       

                                        <tr style="background-color: #E6E6FA;" class="mysensor" onclick="rowSelect(this, '<%=sensorname.item(i).getFirstChild().getNodeValue()%>')"  >

                                             <!--   <td><input type="radio" name="rbox1[]" id="rbox1[]" value="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" onclick="funAjaxCall();"></td>-->
                                            <td><%  String senval = sensorname.item(i).getFirstChild().getNodeValue();
                                                    senval = senval.substring(0, senval.lastIndexOf("_"));
                                                    out.print(senval);%></td>
                                            <td ><img  src="images/switch-turn-off-icon.png" alt="RelaySensor" style="width: 20px;"/></td>
                                            <td ><%=sensorinfo.item(i).getFirstChild().getNodeValue()%></td>
                                            <td ><img class="imge"  src="images/off.png" alt="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" width="50" height="25" onclick="checkRelay(this)" /> </td>
                                        </tr>
                                        <% }
                                                }
                                            } catch (Exception e) {
                                            }


                                        %> 

                                    </tbody>
                                </table>
                            </form>

                        </div>

                    </div>
                </div>
					
					
					<div class="col-lg-8" style="width:46.1%;height:42%">
                      <div class="panel panel-primary" style="height:551px;width:671px;">

                            <div class="panel-heading">
                                IP Camera 
                            </div>
                            <div class="panel-body"  style="height:540px;width:650px">
                               
  
				    <%@include file="./zzz.jsp" %>
                                
                            </div>
                      </div>  
                     
                                                 
                     
                <div id="result1">       
                    <div class="col-lg-8" style=" width:640px">
                       

                        </div>

                    </div>

                 </div>
					
            </div>
        </div>

        <script>
            <%                 URL jsonpage2 = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorList");
                URLConnection urlcon2 = jsonpage2.openConnection();
                DocumentBuilderFactory dbf2 = DocumentBuilderFactory.newInstance();
                DocumentBuilder db2 = dbf2.newDocumentBuilder();
                Document doc2 = db2.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + session.getAttribute("userid") + "/getSensorListByUserId");
                NodeList sensorname2 = doc2.getElementsByTagName("sensorname");
                NodeList sensortype2 = doc2.getElementsByTagName("sensortype");
                NodeList sensorinfo2 = doc2.getElementsByTagName("sensorinfo");
                NodeList sensorstatus2 = doc2.getElementsByTagName("status");
                String val2 = "";
                for (int i = 0; i < sensorname2.getLength(); i++) {
                    if (sensortype2.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")) {
                        val2 += sensorname2.item(i).getFirstChild().getNodeValue() + ":" + sensorstatus2.item(i).getFirstChild().getNodeValue() + ";";
                    }

                }
            %>
    var img = document.getElementsByClassName("imge");
    var uname;
    var data11 = '<%=val2%>';
    // alert(data11);
    var sensorstatus = data11.split(";");
    //alert(sensorstatus.length);
    for (var i = 0; i < sensorstatus.length - 1; i++)
    {
        var senname = sensorstatus[i].substring(0, sensorstatus[i].indexOf(":"));
        var status = sensorstatus[i].substring(sensorstatus[i].indexOf(":") + 1);
        // alert(senname+" "+status+" : "+img[0]);
        for (var j = 0; j < img.length; j++)
        {

            if (img[j].alt.trim() == senname.trim())
            {
                img[j].height = 25;
                img[j].width = 50;
                if (status.trim() == "0")
                {
                    img[j].src = "images/off.png";
                }
                else
                    img[j].src = "images/on.png";
                break;
            }
        }
    }
        </script>
    </body>

    <%
        } else {
            response.sendRedirect("index.jsp");
        }
    %>
    <!-- END BODY -->
</html>