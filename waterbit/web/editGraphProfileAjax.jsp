    
<%@page import="java.net.URLEncoder"%>
<div id="result1">
    <%@page import="org.w3c.dom.Node"%>
    <%@page import="org.w3c.dom.NodeList"%>
    <%@page import="org.w3c.dom.Document"%>
    <%@page import="javax.xml.parsers.DocumentBuilder"%>
    <%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
    <%@page import="java.net.URLConnection"%>
    <%@page import="java.io.InputStreamReader"%>
    <%@page import="java.net.URL"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="org.gadgeon.Config"%>
    <%
        String profile = URLEncoder.encode(request.getParameter("profile"), "UTF-8");
        URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + profile + "/GetProfileByName");
        URLConnection urlcon = jsonpage.openConnection();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + profile + "/GetProfileByName");

        NodeList sensorType = doc.getElementsByTagName("sensortype");
        NodeList xLabel = doc.getElementsByTagName("xlabel");
        NodeList yLabel = doc.getElementsByTagName("ylabel");
        NodeList min = doc.getElementsByTagName("min");
        NodeList max = doc.getElementsByTagName("max");

        jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorType");
        urlcon = jsonpage.openConnection();

        dbf = DocumentBuilderFactory.newInstance();

        db = dbf.newDocumentBuilder();

        doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorType");

        NodeList sensortypes = doc.getElementsByTagName("sensortype");
        
        

    %>
  

    <div class="col-lg-6">
		    <div class="panel panel-primary" style="background-color: #dff0d8;height:445px;">
            <div class="panel-heading">
                Update Graph Profile
            </div>
            <form action="editGraphProfileAction.jsp" method="post"  onsubmit="return(validatetxtbox())">
                <div class="panel-body">
                    <div class="table-responsive"><table class="table">
                            <tbody>

                                <tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Graph Profile Name</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="profileName" id="profileName" readonly="true" value="<%=profile%>"
                                                                           /></td>
                                </tr>

                                <tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Sensor Type </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="prop" id="prop" disabled="true">
                                            <%                                                 for (int i = 0; i <= sensortypes.getLength() - 1; i++) {
                                                    if (sensortypes.item(i).getFirstChild().getNodeValue().compareTo(sensorType.item(0).getFirstChild().getNodeValue()) != 0) {
                                            %>
                                            <option value="<%=sensortypes.item(i).getFirstChild().getNodeValue()%>"
                                                    selected> <%=sensortypes.item(i).getFirstChild().getNodeValue()%>
                                            </option>

                                            <%
                                                    }
                                                }
                                            %>
                                            <option value="<%=sensorType.item(0).getFirstChild().getNodeValue()%>"
                                                    selected> <%=sensorType.item(0).getFirstChild().getNodeValue()%>
                                            </option>
                                        </select></td>
        
                                </tr>

                                <tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">X-Label</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="xLabel" id="xLabel"  value="<%=xLabel.item(0).getFirstChild().getNodeValue()%>"/></td>
 
                                </tr>

                                <tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Y-Label</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="yLabel" id="yLabel" value="<%=yLabel.item(0).getFirstChild().getNodeValue()%>"/></td>
                                </tr>

                                <tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Value Range - Min (Y-axis)</td> <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type= "number" name="min" id="min" onkeypress="return isNumber(event)" value="<%=min.item(0).getFirstChild().getNodeValue()%>"/></td>
                                </tr>
				<tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Value Range - Max (Y-axis)</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type= "number" name="max" id="max" onkeypress="return isNumber(event)" value="<%=max.item(0).getFirstChild().getNodeValue()%>"/></td>
                                </tr>

                                <tr class="success">
                                    <td style="border-top: #599bb3; border-style: dashed; border-width: 1px; border: none"></td>
                                    <td style="border-top: #599bb3; border-style: dashed; border-width: 1px; border: none"><input type="submit" value="Update" class="myButton"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Clear" onclick="return(OnCancel())" class="myButton"></td>
                                </tr>
                            </tbody>
                        </table>
                        <font color='red'> <div id="une"> </div> </font>
                    </div>

                </div>
            </form>
        </div>

    </div>

</div>