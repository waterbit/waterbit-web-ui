
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>
<!DOCTYPE html>
<html lang="en">
    <head>

        <title>WaterBit</title>


        <link rel="stylesheet" href="css/main.css" >
        <link rel="stylesheet" href="css/bstrap.css" >
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" >
        <!--<link rel="stylesheet" href="css/dialog.css" >-->
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
	<link rel="stylesheet" href="css/style.css" />
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	
	<script src="assets/nprogress/nprogress.js"></script>
	<link href="assets/nprogress/nprogress.css" rel="stylesheet" />

        <style>
            .sensoredit{

            }
            .sensoredit:hover {
                cursor:pointer;
            }

            ul.nav li.dropdown:hover > ul.dropdown-menu {
                display: block;    
            }
            .caret-right {
                display: inline-block;
                width: 0;
                height: 0;
                margin-left: 5px;
                vertical-align: middle;
                border-left: 4px solid;
                border-bottom: 4px solid transparent;
                border-top: 4px solid transparent;
            }
	    #li1:hover
	    {
		background-color: #e7e7e7;
		height:58px;
		color: black ;
		border-radius: 5px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
	    }
	    .bgseleceted{
		background-color: #48D1CC;
		cursor: pointer;
	    }
	    .bgunseleceted{
		background-color: #fff;
		cursor: pointer;
	    }
	    .undefined{
		cursor: pointer;
	    }
        </style>

        <style>
            .mysensor{


            }
            .myevent{


            }
            .mysensor:hover {
                cursor:pointer;
            }
            .myevent:hover {
                cursor:pointer;
            }

        </style>
        <style>
            .myButton {
                -moz-box-shadow: 0px 10px 14px -7px #276873;
                -webkit-box-shadow: 0px 10px 14px -7px #276873;
                box-shadow: 0px 10px 14px -7px #276873;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
                background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
                background-color:#599bb3;
                -moz-border-radius:3px;
                -webkit-border-radius:3px;
                border-radius:4px;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-family:Times New Roman;
                font-size:12px;
                font-weight:bold;
                //padding:2px 10px;
                text-decoration:none;
                text-shadow:0px 1px 0px #3d768a;
		float: left;
            }
            .myButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
                background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
                background-color:#408c99;
            }
            .myButton:active {
                position:relative;
                top:1px;
            }
            #div_g{
                text-transform: capitalize;
            }
            #lastDataRetr{
                text-transform: capitalize;
            }

	    #maximizeBtn{
		cursor: pointer;
	    }



        </style>

        <% try {
		if (session.getAttribute("userid") != null) {

		    int userid = (Integer) session.getAttribute("userid");
		    if (userid > 1) {
        %> 
        <script src="js/alertify.min.js"></script>
        <script type="text/javascript">
            var isMaximized=0;
            var livedata_data="";
            var livedata_options="";
	    var liveDataInt;
	    var checkflag = "false";
	    var resizedMap;
	    setInterval(function () {
		$.get("getsensordetails.jsp", function (data11, status) {

		    var cnt = document.getElementsByName("rbox[]");
		    var img = document.getElementsByName("im");
		    var iconim = document.getElementsByName("icon");
		    var uname;
		    var sensorstatus = data11.trim().split("*");
		    var table = document.getElementById("mytab1");
                
		    var x = table.rows.length - 1;
		    var y = sensorstatus.length - 1;
		    if (y > x)
		    {
			$.ajaxSetup({
			    // Disable caching of AJAX responses */
			    cache: false
			});

			window.setTimeout(function () {
			    window.location.reload();

			}, 1000);
			alertify.success("New Sensor Found");
		    }
		    else if (y < x)
		    {

			$.ajaxSetup({
			    // Disable caching of AJAX responses */
			    cache: false
			});
			setTimeout(function () {
			    window.location.reload();
			}, 1000);

			alertify.error("Sensor Deleted");
		    }
		    else
		    {
			for (var i = 0; i < sensorstatus.length - 1; i++)
			{

			    var senname = sensorstatus[i].substring(0, sensorstatus[i].indexOf(":"));
			    var sentype = sensorstatus[i].substring(sensorstatus[i].indexOf(":") + 1, sensorstatus[i].indexOf(";"));
			    var seninfo = sensorstatus[i].substring(sensorstatus[i].indexOf(";") + 1, sensorstatus[i].indexOf("<"));
			    var status = sensorstatus[i].substring(sensorstatus[i].indexOf("<") + 1, sensorstatus[i].indexOf(">"));
			    var senalarmcnt = sensorstatus[i].substring(sensorstatus[i].indexOf(">") + 1, sensorstatus[i].indexOf("#"));
			    var alarmstatus = sensorstatus[i].substring(sensorstatus[i].indexOf("#") + 1, sensorstatus[i].indexOf("$"));
			    var lastuptime = sensorstatus[i].substring(sensorstatus[i].indexOf("$") + 1);
			    var table = document.getElementById("mytab1");

			    var name = senname.split("_");
			    row = document.getElementById(senname + "alarm");
			    row.cells[1].innerHTML = name[0];
			    row.cells[3].innerHTML = seninfo;
			    row.cells[4].innerHTML = senalarmcnt;
			    var table1 = document.getElementById("mytab2");
			    row1 = document.getElementById(senname);
			    row1.cells[0].innerHTML = name[0];

			    row1.cells[2].innerHTML = seninfo;
			    row1.cells[3].innerHTML = lastuptime;

			    for (var j = 0; j < img.length; j++)
			    {

				if (img[j].alt.trim() == senname.trim())
				{
                                   
				    if (status.trim() == "0")
				    {
					img[j].src = "images/red_offline_icon.png";
				    }
				    else
				    {
					img[j].src = "images/live1.png";
					break;
				    }
				}
			    }

			}
		    }

		});
	    }, 60000);

	    function checks(field)
	    {
                
		if (checkflag == "false")
		{
		    if (isNaN(document.myform.elements["rbox[]"].length))
		    {
			document.myform.elements["rbox[]"].checked = true;
			checkflag = "true";

		    }
		    else
		    {
			for (i = 0; i < field.length; i++)
			{
			    field[i].checked = true;
			}
			checkflag = "true";
		    }
		    rowSelecteventAll("#48D1CC");

		    return "Clear All";
		}
		else
		{
		    if (isNaN(document.myform.elements["rbox[]"].length))
		    {
			document.myform.elements["rbox[]"].checked = false;
			checkflag = "false";

		    }
		    else
		    {
			for (i = 0; i < field.length; i++)
			{
			    field[i].checked = false;
			}
			checkflag = "false";
		    }
		    rowSelecteventAll("#fff");
		    return "Select All";

		}
	    }

	    function OnButton1()
	    {

		var count = 0;

		if (isNaN(document.myform.elements["rbox[]"].length))
		{
		    count = 1;
		}
		else
		{
		    for (i = 1; i < document.myform.elements["rbox[]"].length; i++)
		    {
			if (document.myform.elements["rbox[]"][i].checked == true)
			{

			    count++;
			}
		    }
		}
                
		if (count >= 1)
		{
		    document.myform.action = "userClearAlarm.jsp";
		    var list = document.myform.elements["rbox[]"];
		    var arg = "";
		    for (var icnt = 1; icnt < list.length; icnt++)
		    {
			if (list[icnt].checked)
			    if (arg == "")
				arg = "rbox[]=" + list[icnt].value;
			    else
				arg += "&" + "rbox[]=" + list[icnt].value;
		    }
		    $.get("userClearAlarm.jsp?" + arg, function (data11, status) {


			$.get("getsensordetails.jsp", function (data11, status) {

			    $("#btnselectall").val("Select All");
			    var sensorstatus = data11.trim().split("*");
			    var table = document.getElementById("mytab1");

			    var x = table.rows.length - 1;
			    var y = sensorstatus.length - 1;

			    {
				for (var i = 0; i < sensorstatus.length - 1; i++)
				{

				    var senname = sensorstatus[i].substring(0, sensorstatus[i].indexOf(":"));
				    var sentype = sensorstatus[i].substring(sensorstatus[i].indexOf(":") + 1, sensorstatus[i].indexOf(";"));
				    var seninfo = sensorstatus[i].substring(sensorstatus[i].indexOf(";") + 1, sensorstatus[i].indexOf("<"));
				    var status = sensorstatus[i].substring(sensorstatus[i].indexOf("<") + 1, sensorstatus[i].indexOf(">"));
				    var senalarmcnt = sensorstatus[i].substring(sensorstatus[i].indexOf(">") + 1, sensorstatus[i].indexOf("#"));
				    var alarmstatus = sensorstatus[i].substring(sensorstatus[i].indexOf("#") + 1, sensorstatus[i].indexOf("$"));
				    var lastuptime = sensorstatus[i].substring(sensorstatus[i].indexOf("$") + 1);
				    var table = document.getElementById("mytab1");

				    var name = senname.split("_");
				    row = table.rows[i + 1];
				    row.cells[1].innerHTML = name[0];
				    row.cells[3].innerHTML = seninfo;
				    row.cells[4].innerHTML = senalarmcnt;


				}
			    }
			    alertify.success("Alarm cleared");
			    checkflag = "false";

			});
			if (count > 1)
			{
			    for (i = 1; i < document.myform.elements["rbox[]"].length; i++)
			    {
				document.myform.elements["rbox[]"][i].checked = false;
			    }
			    rowSelecteventAll("#fff");
			}
			if (count != 1)
			{
			    rowSelecteventAll("#fff");
			}
		    });

		    return true;
		}
		else if (count < 1)
		{
		    alertify.alert("Please select atleast one sensor");
		    return false;
		}
	    }
            
function getCurrentValue()
{
    
    
   
   var valSensor= document.getElementById("sensorlabel").innerHTML;
       $.get("getSensorValue.jsp?sensor=" + valSensor, function (data12, status) {
           if(data12.trim()=="ok")
           {
             rewrite(valSensor) ;
           }
           else if(data12.trim()=="offline")
           {
              alertify.alert("Device is Offline"); 
           }
           else
           {
              alertify.alert("Can't Connect To Device"); 
           }
		
		    });
}
	    function rewrite(txt)
	    {

		clearInterval(liveDataInt);
		if (txt.trim() != '') {
		    $.get("graphdetail.jsp?sensor=" + txt, function (data12, status) {
			data12 = data12.trim().split(":");

			xval = data12[0];
			yval = data12[1];
			min = data12[2];
			   var interval=data12[3].split("~");
		            max = interval[0];
		            document.getElementById("interlabel").innerHTML = interval[1]+" Seconds";
			$.get("livedatademo.jsp?sensor=" + txt, function (data11, status) {
			    senval = txt;
			    document.getElementById("sensorlabel").innerHTML = senval;

			    livedata(txt, data11);

			});
		    });
		}

		$.ajaxSetup({
		    // Disable caching of AJAX responses */
		    cache: false
		});
		liveDataInt = window.setInterval(function () {

		    $.get("livedatademo.jsp?sensor=" + txt, function (data11, status) {
			senval = txt;
			document.getElementById("sensorlabel").innerHTML = senval;
			livedata(txt, data11);

		    });
		


		}, 60000
			);
	    }

	    function rowSelect(row, sensor)
	    {
		var $table = $('#mytab2');
		$bodyCells = $table.find('tbody').children();
		for (var i = 0; i < $bodyCells.length; i++)
		{
		    if ($bodyCells[i].id == sensor)
			$bodyCells[i].style.background = "#48D1CC";
		    else
			$bodyCells[i].style.background = "#fff";
		}
		rewrite(sensor);
		showMarker(sensor);
		rowSelectevent(row, sensor);
	    }
	    function rowSelectGraph(row, sensor)
	    {

		var $table = $('#mytab1');
		$bodyCells = $table.find('tbody').children();
		for (var i = 0; i < $bodyCells.length; i++)
		{
		    if ($bodyCells[i].id == sensor)
			$bodyCells[i].style.background = "#48D1CC";
		    else
			$bodyCells[i].style.background = "#fff";
		}
		var $table = $('#mytab2');
		$bodyCells = $table.find('tbody').children();
		for (var i = 0; i < $bodyCells.length; i++)
		{
		    if ($bodyCells[i].id == sensor)
			$bodyCells[i].style.background = "#48D1CC";
		    else
			$bodyCells[i].style.background = "#fff";
		}
		rewrite(sensor);
	    }
	    function rowSelectevent(row, sensor)
	    {

		rowSelectGraph(sensor, sensor);

		var $table = $('#mytab1'),
			$bodyCells = $table.find('tbody').children();
		for (var i = 0; i < $bodyCells.length; i++)
		{
		    if ($bodyCells[i].id == sensor + "alarm")
			$bodyCells[i].style.background = "#48D1CC"
		    else
			$bodyCells[i].style.background = "#fff";
		}
	
              var str = row.cells[0].innerHTML.trim();
             row.cells[0].innerHTML = str.replace("type=\"checkbox\"","type=\"checkbox\" checked ");

		showMarker(sensor);
	    }
	    function rowSelecteventgraph(row, sensor)
	    {

		row = (document.getElementById(row + "alarm"));

		for (i = 1; i < document.myform.elements["rbox[]"].length; i++)
		{
		    document.myform.elements["rbox[]"][i].checked = false;
		}

		var $table1 = $('#mytab1');
		$bodyCells1 = $table1.find('tbody').children();
		for (var i = 0; i < $bodyCells1.length; i++)
		{
		    if ($bodyCells1[i].id == sensor+"alarm")
			$bodyCells1[i].style.background = "#48D1CC";
		    else
			$bodyCells1[i].style.background = "#fff";
		}
		var $table = $('#mytab2');
		$bodyCells = $table.find('tbody').children();
		for (var j = 0; j < $bodyCells.length; j++)
		{
		    if ($bodyCells[j].id == sensor)
			$bodyCells[j].style.background = "#48D1CC";
		    else
			$bodyCells[j].style.background = "#fff";
		}
		
		 var str = row.cells[0].innerHTML.trim();
		 row.cells[0].innerHTML =  str.replace("type=\"checkbox\"","type=\"checkbox\" checked ");//str.substr(0, str.length - 1) + " checked >";
		
	    }
	    function rowSelecteventAll(color)
	    {
		
		var $table = $('#mytab1'),
			$bodyCells = $table.find('tbody').children();
		for (var i = 0; i < $bodyCells.length; i++)
		{

		    $bodyCells[i].style.background = color;
		    var str = $bodyCells[i].cells[0].innerHTML.trim();
		}
	    }
	    $(document).ready(function () {

		
		$("#maximizeBtn").click(function () {
		    
		    if ($('#draggable2').is(':hidden')) {
			resizedMap = 0;
                        isMaximized=0;
			$("#draggable2").show();
                        $("#draggable1").show();
                         g = new Dygraph(document.getElementById("div_g"),livedata_data,livedata_options);
			$("#tables").show();
			var wid = $("#draggable").width();
			var mapWid = wid - 30;
                        $("#map_canvas").css('cssText','top : 15% !important');
                        $("#draggable").css('cssText','height: '+$("#draggable1").css('height')+' !important;');
                     
                        
                        $("#map_canvas").css('height',  Number($("#draggable1").css('height').replace("px","")*0.8)+'px');
			
			mapPloat();
		    }
		    else
		    {
			$("#draggable2").hide();
			$("#tables").hide();
                        isMaximized=1;
			var wid = $(document).width() - 27;
			var mapWid = wid - 30;
                        $("#draggable").css('cssText','height: 540px !important;');
		
			$("#draggable1").hide();
                       
                        
                        $("#map_canvas").css('height',  '480px');
			if ($('#draggable2').is(':hidden'))
			{
			    if (wid < 1224)
			    {
                                
				
                                $("#map_canvas").css('cssText','top : 9% !important');
                                $("#map_canvas").css('height',  '480px');
			
				
			    }
                            if((wid+27)>1200)
                            {
                                $("#draggable").css('width',  '200%');
                                $("#map_canvas").css('width', '190%');
                            }
                            
			}
			mapPloat();
		    }
		});
		$("#closeDialog").click(function () {
	  
		    window.location.href = "userDashBoard.jsp";
		});
	    });


	    function funAddNewSensor()
	    {

		Alert.render();

	    }
	    function OnCancel()
	    {

		window.location.href = "userDashBoard.jsp";

	    }
	    function validatetxtbox()
	    {
		var endDevName = document.getElementById("endDevName").value;
		var prop = document.getElementById("prop").value;
		if (document.getElementById("graphprofile").length != 0)
		{
		    var graphprofile = document.getElementById("graphprofile").value;
		} else {
		    var graphprofile = "";
		}
		var sinfor = document.getElementById("sinfor").value;
		var city = document.getElementById("city").value;
		var maxTemp = document.getElementById("maxTemp").value;
		var minTemp = document.getElementById("minTemp").value;

		if (endDevName == "")
		{
		    document.getElementById('une').innerHTML = "* Please select end device";
		    document.getElementById("endDevName").focus();
		    return(false);
		}


		else if (graphprofile == "")
		{
		    document.getElementById('une').innerHTML = "* Please select graph profile";
		    document.getElementById("graphprofile").focus();
		    return(false);
		}
		else if (sinfor == "")
		{
		    document.getElementById('une').innerHTML = "* Please type sensor information";
		    document.getElementById("sinfor").focus();
		    return(false);
		}

		else if (minTemp == "")
		{
		    document.getElementById('une').innerHTML = "* Please enter minimum threshold value";
		    document.getElementById("city").focus();
		    return(false);
		}
		else if (maxTemp == "")
		{
		    document.getElementById('une').innerHTML = "* Please enter maximum threshold value";
		    document.getElementById("city").focus();
		    return(false);
		}
		else if (maxTemp < minTemp)
		{

		    document.getElementById('une').innerHTML = "* Maximum Threshold value should be grater than minimum threshold value";
		    document.getElementById("minTemp").focus();
		    return(false);
		}

		else
		{
		    return(true);
		}


	    }
	
        </script>
	<style>
            @media screen and (max-width:767px){
                #map_canvas{
                    
                    left: 3.5% !important;
                    width: 93% !important;
                }
            }
            @media screen and (max-width:1200px){
                #map_canvas{
                    
                    left: 2.5% !important;
                }
            }
	    @media screen and (min-width : 1200px)
	    {
                
		.row { height: calc(50% - 40px) !important;}
		#draggable{height: 100% !important;}
		#draggable1{height: 100% !important;}
		#draggable2{height: 100% !important;}
		#leftColtop{height: 100% !important;}
		#leftColDown{height: 100% !important;}
		#rightColDown{height: 100% !important;}
		#tables{height: 100% !important;}
                #mytab2-div{height: auto ;}
                #mytab1-div{height: auto ;}
		.container{
		    height: 100% !important;
		}
		#rightColtop{height: 100% !important;}
		
	    }
	    @media only screen 
            and (min-width : 1224px) {
		/* Styles */
		#leftColtop
		{
		    margin-top: -1.2%;
		    padding-right: 3px;
		  
		}
		#rightColtop{
		    margin-top: -1.2%; 
		    padding-left: 3px;
		    
		}
		#leftColDown
		{
		    margin-top: -.7%;
		    padding-right: 3px;
		    height:100%;
		}
		#rightColDown
		{
		    margin-top: -.7%;
		    padding-left: 3px;
		    height:100%;
		}
            }




	    table, td, a {
		color: #000;
		font: normal normal 12px Verdana, Geneva, Arial, Helvetica, sans-serif
	    }
	    thead.fixedHeader tr {
		position: relative;


	    }
	    html>body thead.fixedHeader tr {
		display: block;
		box-shadow: 0 -30px 50px #337AB7 inset;
		border: 0px solid #337AB7;
		color:white;
		margin-right: 12px;
		border-radius:5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	    }

	    /* make the TH elements pretty */
	    thead.fixedHeader th {


		font-weight: normal;
		padding: 4px 3px;
		text-align: left
	    }

	    /* make the A elements pretty. makes for nice clickable headers                */
	    thead.fixedHeader a, thead.fixedHeader a:link, thead.fixedHeader a:visited {
		color: #FFF;
		display: block;
		text-decoration: none;
		width: 100%

	    }

	    /* make the A elements pretty. makes for nice clickable headers                */
	    /* WARNING: swapping the background on hover may cause problems in WinIE 6.x   */
	    thead.fixedHeader a:hover {
		color: #FFF;
		display: block;
		text-decoration: underline;
		width: 100%
	    }

	    /* define the table content to be scrollable                                              */
	    /* set TBODY element to have block level attributes. All other non-IE browsers            */
	    /* this enables overflow to work on TBODY element. All other non-IE, non-Mozilla browsers */
	    /* induced side effect is that child TDs no longer accept width: auto                     */
	    html>body tbody.scrollContent {
		display: block;
		height: 262px;
		overflow: auto;
		width: 100%
	    }

	    /* make TD elements pretty. Provide alternating classes for striping the table */
	    /* http://www.alistapart.com/articles/zebratables/                             */
	    tbody.scrollContent td, tbody.scrollContent tr.normalRow td {
		/*	background: #FFF;
			border-bottom: none;
			border-left: none;
			border-right: 1px solid #CCC;
			border-top: 1px solid #DDD;
			padding: 2px 3px 3px 4px;*/

	    }

	    tbody.scrollContent tr.alternateRow td {
		/*	background: #EEE;
			border-bottom: none;
			border-left: none;
			border-right: 1px solid #CCC;
			border-top: 1px solid #DDD;
			padding: 2px 3px 3px 4px*/
	    }
	    @media only screen 
            and (min-width : 1224px) {
		/* define width of TH elements: 1st, 2nd, and 3rd respectively.          */
		/* Add 16px to last TH for scrollbar padding. All other non-IE browsers. */
		/* http://www.w3.org/TR/REC-CSS2/selector.html#adjacent-selectors        */
		html>body thead.fixedHeader th {
		    width: 160px
		}

		html>body thead.fixedHeader th + th {
		    width: 160px
		}

		html>body thead.fixedHeader th + th + th {
		    width: 160px
		}
		html>body thead.fixedHeader th + th + th +th {
		    width: 240px;
		}

		/* define width of TD elements: 1st, 2nd, and 3rd respectively.          */
		/* All other non-IE browsers.                                            */
		/* http://www.w3.org/TR/REC-CSS2/selector.html#adjacent-selectors        */
		html>body tbody.scrollContent td {
		    width: 160px;
		}

		html>body tbody.scrollContent td + td {
		    width: 160px
		}
		html>body tbody.scrollContent td + td + td{
		    width: 160px
		}
		html>body tbody.scrollContent td + td + td + td {
		    width: 290px;
		}
	    }
	    @media only screen 
            and (max-width : 1224px) {
		/* define width of TH elements: 1st, 2nd, and 3rd respectively.          */
		/* Add 16px to last TH for scrollbar padding. All other non-IE browsers. */
		/* http://www.w3.org/TR/REC-CSS2/selector.html#adjacent-selectors        */
		html>body thead.fixedHeader th {
		    width: 200px
		}

		html>body thead.fixedHeader th + th {
		    width: 240px
		}

		html>body thead.fixedHeader th + th + th {
		    width: 240px
		}
		html>body thead.fixedHeader th + th + th +th {
		    width: 290px;
		}
		html>body thead.tiny_tab_tr
		{


		}
		/* define width of TD elements: 1st, 2nd, and 3rd respectively.          */
		/* All other non-IE browsers.                                            */
		/* http://www.w3.org/TR/REC-CSS2/selector.html#adjacent-selectors        */
		html>body tbody.scrollContent td {
		    width: 200px;
		}

		html>body tbody.scrollContent td + td {
		    width: 240px
		}
		html>body tbody.scrollContent td + td + td{
		    width: 240px
		}
		html>body tbody.scrollContent td + td + td + td {
		    width: 290px;
		}
	    }
	    html>body tbody.scrollContent
	    {
		overflow: scroll;
	    }
	    table.sortable thead {

		cursor: default;
	    }
	    
	    
	</style>
    </head>
    <body  style="height: 100%;" onresize="resizeEvent()">
	<script>
            var checkResize=0;
            function resizeEvent()
            {
       
                if($("body").width()<1200)
                {
                    if(isMaximized==1){
                      
                        $("#draggable").css('width',  '100%');
                        var hh=$("#map_canvas").height();
                        $("#map_canvas").css('cssText','top : 9% !important');
                        $("#map_canvas").css('width', '95%');
                        $("#map_canvas").css('height', hh+'px');
                        
                    }
                }
                else
                {
                    if(isMaximized==1){
                        $("#draggable").css('width',  '200%');
                        var hh=$("#map_canvas").height();
                        $("#map_canvas").css('cssText','top : 15% !important');
                        $("#map_canvas").css('height', hh+'px');
                        $("#map_canvas").css('width', '190%');
                    }
                }
            }
             NProgress.set(0.0);
               NProgress.start();
              var mnc=0;
        </script>
        <%
	    String currentSensor = "", currentSensorType = "";%>

        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>  
        </div>
      
        <div class="container">  


	    <div class="navbar navbar-default" role="navigation" style="background: none repeat scroll 0% 0% #11020B; color: #FFF;  ">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="color: #FFF"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active" ><a href="#" id="li1" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;"><img src="img/icon-dashboard.png" width="22" height="22" />Dashboard</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="li1" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;"><img src="img/icon-form-style.png" width="22" height="22"/>Configuration&nbsp;&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu" style="background: none repeat scroll 0% 0% #11020B;">
                                <li ><a href="addNewEndDevice.jsp" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;">End Device</a> </li>
                                <li ><a href="addNewSensor.jsp" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;"> Sensor</a> </li>
                                <li><a href="addNewSensorType.jsp" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;">Sensor Type</a></li>
                                <li><a href="addNewGraphProfile.jsp" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;">Graph Profile</a></li>
                            </ul>
                        </li>
                        <li id="li1">
			    <a href="panelView.jsp" id="li1" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;"><img src="img/icon-charts-graphs.png" width="22" height="22"/>Analytics&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li id="li1"><a href="logout.jsp" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;"><img src="img/img-profile.jpg" width="28" height="28"/>Logout</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6" id="leftColtop" style="z-index: 75;" >
		    <div class="panel panel-primary" id="draggable" style=" background-color: #fff;width: 100%;height: 300px;">
			<div class="panel-heading">Map
			    <img src="images/maximize.png" id="maximizeBtn" name="maximizeBtn" width="40" height="40" style="float: right; margin-top: -1%;">
			</div>
                        <div class="panel-body">
                            <%@ include file="geomaptest.jsp" %>
                        </div>
                    </div>
                </div>

              <%                                               int cntsensors=0; try {

		                                            URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorList");
		                                            URLConnection urlcon = jsonpage.openConnection();

		                                            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		                                            DocumentBuilder db = dbf.newDocumentBuilder();

		                                            Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
		                                            NodeList sensorname = doc.getElementsByTagName("sensorname");
		                                            NodeList sensortype = doc.getElementsByTagName("sensortype");
		                                            NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
		                                            NodeList sensorstatus = doc.getElementsByTagName("status");
		                                            NodeList lastuptime = doc.getElementsByTagName("lastuptime");

		                                            int j = 0;

		                                            for (int i = 0; i <= sensorname.getLength() - 1; i++) {
		                                                if (!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("Relay")) {
		                                                    if (currentSensor == "") {
		                                                        currentSensor = sensorname.item(i).getFirstChild().getNodeValue();
		                                                        currentSensorType = sensortype.item(i).getFirstChild().getNodeValue();
		                                                      
		                                                    }
		                                                     cntsensors++;
		                                                    
		                                                                   String[] sensor = currentSensor.split("_");

		                                         jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensor[0] + "/GetEndPointDeviceByName");
		                                         urlcon = jsonpage.openConnection();
		                                         dbf = DocumentBuilderFactory.newInstance();
		                                         db = dbf.newDocumentBuilder();
		                                         doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensor[0] + "/GetEndPointDeviceByName");

		                                        NodeList reportInterval = doc.getElementsByTagName("timeintervel");
	if(i==0)
	{                                          %> 
               				<div class="col-lg-6 mycol1" id="rightColtop" style="z-index: 1;">
				    <div class="panel panel-primary mypanel1" id="draggable1" style=" background-color: #fff;height: 300px;" >
				        <div class="panel-heading"  style="font-size: 16">Live Data:
				                <label id="sensorlabel" name="sensorlabel" style="font-size: 15"><% String senval1 = currentSensor;
		                            out.print(senval1);%></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reporting Interval:<label id="interlabel" style="font-size: 15"><%  out.print(reportInterval.item(0).getFirstChild().getNodeValue());%> Seconds</label>
                                         &nbsp;&nbsp;&nbsp;<INPUT type="button" class="myButton" id="getValue" value="Get Value" name="getValue" onclick="getCurrentValue();" style="position:relative;float: right;margin-right: 0%; ">
                                        </div>
				        <div class="panel-body">
				            <%@ include file="newjsp2.jsp" %>
				        </div>
				    </div>
				</div>



            </div>


	    <div class="row">

                <div class="col-lg-6 myclass" id="leftColDown">
                    <form>
                    <div class="panel panel-primary mypanel" id="draggable2" style="height: 300px; background-color: #fff;">
                        <div class="panel-heading">My Sensors
			    <div class="search" style="float: right;">
				<select id="columns" onchange="sorter.search('query')" style="color: black;width: 100px;"></select>&nbsp;
				<input type="text" id="query" class="txtsearch" onkeyup="sorter.search('query')" style="color:black;" />
			    </div>
			    <span class="details" style="visibility: hidden;">
				<div style="float: left;" >Records <span id="startrecord"></span>-<span id="endrecord"></span> of <span id="totalrecords"></span></div>
				<div style="float: left;" ><a href="javascript:sorter.reset()">reset</a></div>
			    </span>
			</div>
                        <div class="panel-body">
                            <form name="form1" method="post">
                                <div id="mytab2-div" class="table-responsive mytable1" style="height: 230px;/* overflow: hidden;*/ width: 100%;">
                                    <table class="scroll table tinytable" id="mytab2"  border="0" cellpadding="0" cellspacing="0" width="100%"   >
					<thead >
                                            <tr class="tiny_tab_tr" style=" 
						border: 1px solid #337AB7;
						color:white;
						background-color: #337AB7;
						-moz-border-radius: 5px;
						-webkit-border-radius: 5px; 
						background-image: -webkit-linear-gradient(top,#337ab7 0,#2e6da4 100%);" >
                                                <th style="text-align: center;" id="th1"><h3>End Device</h3></th>
					<th style="text-align: center" id="th2" ><h3>Sensor Type</h3></th>
					<th style="text-align: center" id="th3" ><h3>Location</h3></th>
					<th style="text-align: center" id="th4"  ><h3>Last Change</h3></th>
                                        <th style="text-align: center" id="th5" ><h3>Status</h3></th>
					</tr>
                                        </thead>
                                        <tbody  style="overflow: scroll; height: 200px;">

      
		                                    <% }
        String senval = sensorname.item(i).getFirstChild().getNodeValue();%>
		                                    <tr id="<%=senval.trim()%>" style="background-color:<%=(j++ == 0) ? "#48D1CC" : "#fff"%>" class="mysensor" onclick="rowSelect(this, '<%=sensorname.item(i).getFirstChild().getNodeValue()%>')">

		                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%
		                                            senval = senval.substring(0, senval.lastIndexOf("_"));
		                                            out.print(senval);
		                                            %></td>
		                                        <td  style="border: #599bb3; border-style: dashed; border-width: 1px;"><%=sensortype.item(i).getFirstChild().getNodeValue()%></td>

		                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%=sensorinfo.item(i).getFirstChild().getNodeValue()%></td>

		                             

		                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;">
		                                            <%
		                                                if (lastuptime.item(i).getChildNodes().getLength() != 0) {
		                                            %>

		                                            <%=lastuptime.item(i).getFirstChild().getNodeValue()%>
		                                            <% } else {
		                                            %>
		                                            <%out.print("");%>
		                                            <%}
		                                            %>

		                                        </td>        
                                                                  <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center;">
                                                    <% //String val=sensorstatus.item(i).getFirstChild().getNodeValue();
                                                        //  String val="1";
                                                        if (sensorstatus.item(i).getFirstChild().getNodeValue().trim().equalsIgnoreCase("1")) //      if(val.equals("0"))
                                                        {%>
                                                    <img name="im" src="images/live1.png" alt="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" name="" style="width: 15px;">
                                                    <%
                                                    } else {
                                                    %>
                                                    <img name="im" src="images/red_offline_icon.png" alt="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" name="" style="width: 15px;">
                                                    <%
                                                        }
                                                    %>
                                                </td>
		                                    </tr>
		                                    <%
		                                                }
		                                            }
		                                        } catch (Exception e) {
		                                        }


		                                    %> 
		                                </tbody>

                                    </table>
				    <div id="tablefooter" style="display:none">
					<div id="tablenav">
					    <div>
						<img src="images/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
						<img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
						<img src="images/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
						<img src="images/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
					    </div>
					    <div>
						<select id="pagedropdown"></select>
					    </div>
					    <div>
						<a href="javascript:sorter.showall()">view all</a>
					    </div>
					</div>
					<div id="tablelocation">
					    <div>
						<select onchange="sorter.size(this.value)">
						    <option value="5">5</option>
						    <option value="10" selected="selected">10</option>
						    <option value="20">20</option>
						    <option value="50">50</option>
						    <option value="100">100</option>
						</select>
						<span>Entries Per Page</span>
					    </div>
					    <div class="page">Page <span id="currentpage"></span> of <span id="totalpages"></span></div>
					</div>
					<script type="text/javascript" src="js/script.js"></script>
					<script type="text/javascript">
							var sorter = new TINY.table.sorter('sorter', 'mytab2', {
							    headclass: 'head',
							    ascclass: 'asc',
							    descclass: 'desc',
							    paginate: true,
							    size: <%=cntsensors%>,
							    colddid: 'columns',
							    currentid: 'currentpage',
							    totalid: 'totalpages',
							    startingrecid: 'startrecord',
							    endingrecid: 'endrecord',
							    totalrecid: 'totalrecords',
							    hoverid: 'selectedrow',
							    pageddid: 'pagedropdown',
							    navid: 'tablenav',
							    sortcolumn: 0,
							    sortdir: 1,
							    sum: [8],
							    avg: [6, 7, 8, 9],
							    columns: [{index: 7, format: '%', decimals: 1}, {index: 8, format: '$', decimals: 0}],
							    init: true
							});
							// Change the selector if needed
							 // Trigger resize handler
							 $(document).ready(function() {

								
								if(Number($("body").css("width").replace("px",""))>1200)
								{
                                                                    var mh=Number($("body").height()*.33);
								    $("#mytab2-div").css("height",mh);
                                                        		$("#mytab1-div").css("height",mh);
								}
								

							 });

					</script>
					<style>
					    #mytab2 > tfoot { display: none; }
					</style>

				    </div>

                        </div>
                                                             </form>
                    </div>
                                                                                       

                </div>
                    </form>
	    </div>
	    <div class="col-lg-6" id="rightColDown">
		<form method="post" name="myform" id="myform">
		    <div class="panel panel-primary" id="tables" style="height: 300px; background-color: #fff; ">
			<div class="panel-heading"><img src="images/bell.png" height="20" width="20"/>My Events

			    <INPUT type="button" class="myButton" id="btnClear" value="Clear Alarm" name="btnClear" onclick="OnButton1();" style="position:relative;float: right;margin-right: 0%; ">
			    <INPUT type=button class="myButton" id="btnselectall" value="Select All" onClick="this.value = checks(this.form)" style="position:relative;float:right;margin-right: 3%;">
			    


			</div>
			<div class="panel-body">
			    <div id="mytab1-div" class="table-responsive mytable2" style="height: 230px; /*overflow: hidden;*/width: 100%;">
				<table class="scroll table tinytable" id="mytab1"  border="0" cellpadding="0" cellspacing="0" width="100%"  >
				    <thead>
					<tr class="tiny_tab_tr" style=" 
					    border: 1px solid #337AB7;
					    color:white;
					    background-color: #337AB7;
					    -moz-border-radius: 5px;
					    -webkit-border-radius: 5px; 
					    background-image: -webkit-linear-gradient(top,#337ab7 0,#2e6da4 100%);">

					    <th style="border:none" hidden="true"><h3><input  type="checkbox" name="rbox[]" id="rbox[]" value="0" style="visibility:hidden;"></h3></th>
				    <th style="text-align: center;" ><h3>End Device</h3></th>
				    <th style="text-align: center;" ><h3>Sensor Type</h3></th>
				    <th style="text-align: center; "><h3>Location</h3></th>
				    <th style="text-align: center;" ><h3>Alarm Count</h3></th>

				    </tr>
				    </thead>

				    <tbody style="overflow: scroll; height: 200px;">

					<%                                                 try {

						URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
						URLConnection urlcon = jsonpage.openConnection();

						DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

						DocumentBuilder db = dbf.newDocumentBuilder();

						Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
						NodeList sensorname = doc.getElementsByTagName("sensorname");
						NodeList sensortype = doc.getElementsByTagName("sensortype");
						NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
						NodeList alarmcount = doc.getElementsByTagName("alarmcount");
						NodeList alarmstatus = doc.getElementsByTagName("alarmstatus");
						int k = 0;
						for (int i = 0; i <= sensorname.getLength() - 1; i++) {
						    if (!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")) {

					%>                                                

					<tr id="<%=sensorname.item(i).getFirstChild().getNodeValue() + "alarm"%>" style="background-color:<%=(k++ == 0) ? "#48D1CC" : "#fff"%>" class="myevent" onclick="rowSelectevent(this, '<%=sensorname.item(i).getFirstChild().getNodeValue()%>'), this.form" >

					    <td style="border:none;" hidden="true"><input type="checkbox" name="rbox[]" id="rbox[]" value="<%=sensorname.item(i).getFirstChild().getNodeValue()%>"  style="visibility:hidden;"></td>
					    <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><% String temsen = sensorname.item(i).getFirstChild().getNodeValue();
						temsen = temsen.substring(0, temsen.lastIndexOf("_"));
						out.print(temsen.trim());
						%></td>
					    <td  style="border: #599bb3; border-style: dashed; border-width: 1px;"><%=sensortype.item(i).getFirstChild().getNodeValue().trim()%></td>

					    <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%=sensorinfo.item(i).getFirstChild().getNodeValue().trim()%></td>

					    <td id="alarmcnt" style="border: #599bb3; border-style: dashed; border-width: 1px;text-align: center;"><%=alarmcount.item(i).getFirstChild().getNodeValue().trim()%></td>


					</tr>
					<%
						    }
						}
					    } catch (Exception e) {

					    }


					%> 
				    </tbody>



				
			    </table>
				    <div id="tablefooter1" style="display:none" >
				    <div id="tablenav1">
					<div>
					    <img src="images/first.gif" width="16" height="16" alt="First Page" onclick="sorter1.move(-1, true)" />
					    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter1.move(-1)" />
					    <img src="images/next.gif" width="16" height="16" alt="First Page" onclick="sorter1.move(1)" />
					    <img src="images/last.gif" width="16" height="16" alt="Last Page" onclick="sorter1.move(1, true)" />
					</div>
					<div>
					    <select id="pagedropdown1"  ></select>
					</div>
					<div>
					    <a href="javascript:sorter1.showall()">view all</a>
					</div>
				    </div>
				    <div id="tablelocation1">
					<div>
					    <select onchange="sorter1.size(this.value)">
						<option value="5">5</option>
						<option value="10" selected="selected">10</option>
						<option value="20">20</option>
						<option value="50">50</option>
						<option value="100">100</option>
					    </select>
					    <span>Entries Per Page</span>
					</div>
					<div class="page">Page <span id="currentpage1"></span> of <span id="totalpages1"></span></div>
					<div class="search" style="float: right;visibility: hidden;">
				<select id="columns1" onchange="sorter1.search('query')" style="color: black;"></select>&nbsp;
				<input type="text" id="query1" class="txtsearch" onkeyup="sorter1.search('query')" style="color:black;" />
			    </div>
			    <span class="details" style="visibility: hidden;">
				<div style="float: left;" >Records <span id="startrecord1"></span>-<span id="endrecord1"></span> of <span id="totalrecords1"></span></div>
				<div style="float: left;" ><a href="javascript:sorter1.reset()">reset</a></div>
			    </span>
				    </div>
				    <script type="text/javascript">
					var sorter1 = new TINY.table.sorter('sorter1', 'mytab1', {
					    headclass: 'head',
					    ascclass: 'asc',
					    descclass: 'desc',
					    paginate: true,
					    size: <%=cntsensors%>,
					    colddid: 'columns1',
					    currentid: 'currentpage1',
					    totalid: 'totalpages1',
					    startingrecid: 'startrecord1',
					    endingrecid: 'endrecord1',
					    totalrecid: 'totalrecords1',
					    hoverid: 'selectedrow1',
					    pageddid: 'pagedropdown1',
					    navid: 'tablenav1',
					    sortcolumn: 1,
					    sortdir: 1,
					    sum: [8],
					    avg: [6, 7, 8, 9],
					    columns: [{index: 7, format: '%', decimals: 1}, {index: 8, format: '$', decimals: 0}],
					    init: true
					});

			    // Change the selector if needed
					// Trigger resize handler

				    </script>
				    <style>
					#mytab1 > tfoot { display: none; }

				    </style>

                                </div>
			    </div>
			</div>
		    </div>
		</form>
	    </div>
	</div>

    </div>



    <%                                            try {
	    String[] values = request.getParameterValues("val");
	    if (values != null) {
		if (values[0].equals("0")) {%>
    <script type="text/javascript">
	document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
	    if (document.readyState == 'loaded' || document.readyState == 'complete')
		alertify.success("Alarm cleared..!");
	    //alert("haiii");

	}

    </script>
    <%
    } else if (values[0].equals("1")) {%>  
    <script type="text/javascript">
	document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
	    if (document.readyState == 'loaded' || document.readyState == 'complete')
		alertify.alert("Can not clear alarm..!");

	}
    </script>
    <%
    } else if (values[0].equals("2")) {%>  
    <script type="text/javascript">
	document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
	    if (document.readyState == 'loaded' || document.readyState == 'complete')
		alertify.success("Password changed successfully..!");
	}
    </script>
    <%
    } else if (values[0].equals("3")) {%>  
    <script type="text/javascript">
	document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
	    if (document.readyState == 'loaded' || document.readyState == 'complete')
		alertify.alert("Old password is incorrect..!");
	}
    </script>
    
    <%
	}
    %>   
    <script>

    </script> 
    <%
	    }
	} catch (Exception ex) {

	}
    %>



    <script src="js/bootstrap.min.js"></script>  
    <script src="js/dialog.js"></script> 
</body>

<%
	    }
	} else {
	    response.sendRedirect("index.jsp");
	}
    } catch (Exception ex) {

    }

%>
<style>
    .txtsearch
    {
	display:inline !important;
    }
    h3{
	margin-top:10px !important;
	margin-bottom: 10px !important;
    }
    input[type="radio"], input[type="checkbox"]
    {
	margin: 0px 0px 0px !important;
    }
    div.panel-body
    {
	height:90%;
	
    }
    .table > thead > tr > th{
        padding: 0px !important;
    }
</style>
<!-- END BODY -->

</html>