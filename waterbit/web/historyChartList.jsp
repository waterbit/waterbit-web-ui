<%-- 
    Document   : historyChart
    Created on : 4 Dec, 2014, 3:00:21 PM
    Author     : nisha
--%>
<%@page import="org.gadgeon.Config"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Vital Herd</title>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="css/main.css" >
	<link rel="stylesheet" href="css/bstrap.css" >
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	

	<style>
	    .col-center {
		position: absolute;
		top: 0;
		left: 10%;
		//margin-left: -12.5%;
		margin-right: 10%;
	    }
	</style>
    </head>
    <body>
	<div class="container">
	    <%if (session.getAttribute("userid") != null) {

		    int userid = (Integer) session.getAttribute("userid");
		    if (userid == 1) {
	    %> 

	    <%@include file="./adminHeader.jsp" %>
	    <%
	    } else {
	    %> 

	    <%@include file="./userHeader.jsp" %>
	    <%
		}
	    //  }
%>
	    <div class="row-fluid">
		<div class="col-lg-12 ">
                    <div class="panel panel-primary" style="height: 1300px; background-color: #dff0d8">
			<div class="panel-heading">
			    History Values
			</div>
			<div class="panel-body">
			    <%@ include file="historylist.jsp" %>
			</div>
		    </div>
		</div>


		<%		} else {
			response.sendRedirect("index.jsp");
		    }
		%>
	    </div>
	</div>
    </body>
</html>