<%-- 
    Document   : editEndDeviceAjax
    Created on : 10 Feb, 2015, 1:18:15 PM
    Author     : nisha
--%>

<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>WaterBit</title>
        <script>
            function validatetxtbox()
            {

                var sName = document.getElementById("sName").value;
                var sinfor = document.getElementById("sinfor").value;
                var txtAddress = document.getElementById("txtAddress").value;
                var city = document.getElementById("city").value;
var reportIntervals = document.getElementById("timeintervel").value;

                if (sName == "")
                {
                    document.getElementById('une').innerHTML = "* Sensor name field should not be empty";
                    document.getElementById("sName").focus();
                    return(false);
                }
                else if (!isNaN(parseInt(sName.charAt(0)))) {
                    document.getElementById('une').innerHTML = "* Sensor name cannot start with number";
                    document.getElementById("sName").focus();
                    return(false);
                }

                else if (/^[a-zA-Z0-9- ]*$/.test(sName) == false)
                {
                    document.getElementById('une').innerHTML = "* Sensor name contains illegal character";
                    document.getElementById("sName").focus();
                    return(false);
                }

                else if (sName.length > 30) {
                    document.getElementById('une').innerHTML = "* Sensor name length exceeded";
                    document.getElementById("sName").focus();
                    return(false);
                }



                else if (sinfor == "")
                {
                    document.getElementById('une').innerHTML = "* Please type sensor information";
                    document.getElementById("sinfor").focus();
                    return(false);
                }
                else if (city == "")
                {
                    document.getElementById('une').innerHTML = "* Please type correct zipcode/pincode and click on get location";
                    document.getElementById("city").focus();
                    return(false);
                }
                else if (prop != "Relay" && maxTemp == "")
                {
                    document.getElementById('une').innerHTML = "* Maximum Threshold value required";
                    document.getElementById("maxTemp").focus();
                    return(false);
                }
  else if (reportIntervals == "")
                {
                    document.getElementById('une').innerHTML = "* Please type Reporting Interval";
                    document.getElementById("city").focus();
                    return(false);
                }

                else
                {

                    return(true);
                }


            }
        </script>
    </head>
    <body>
        <%
            String sensor = request.getParameter("sensor");
            int userid = (Integer) session.getAttribute("userid");

            URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensor + "/GetEndPointDeviceByName");
            URLConnection urlcon = jsonpage.openConnection();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensor + "/GetEndPointDeviceByName");

            NodeList EndPointDeviceId = doc.getElementsByTagName("EndPointDeviceId");
            NodeList DeviceInfo = doc.getElementsByTagName("DeviceInfo");
            NodeList latitude = doc.getElementsByTagName("latitude");
            NodeList longitude = doc.getElementsByTagName("longitude");
            NodeList location = doc.getElementsByTagName("location");
             NodeList reportInterval = doc.getElementsByTagName("timeintervel");


        %>
        <div id="result1">
            <div class="panel panel-primary" style="background-color: #dff0d8; height:400px;">
                <div class="panel-heading">
                    Edit End Device
                </div>
                <form action="editEndPointAction.jsp" method="post"  onsubmit="return(validatetxtbox())">
                    <div class="panel-body">
                        <div class="table-responsive" style="height: 340px;">
                            <table class="table">
                                <tbody>

                                    <tr class="success">
                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;">End Device ID </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="sName" id="sName" value="<%= EndPointDeviceId.item(0).getFirstChild().getNodeValue()%>" readonly="true"/></td>
                                    </tr>

                                    <tr class="success">
                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Information</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <textarea name="sinfor" id="sinfor" cols="30" rows="4"><%=DeviceInfo.item(0).getFirstChild().getNodeValue()%></textarea></td>

                                    </tr>

                              
                                                      <tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Reporting Interval (In Seconds)</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="reportInterval" id="reportInterval" onkeypress="return isNumber(event)" value="<%= reportInterval.item(0).getFirstChild().getNodeValue()%>"/></td>
                                                </tr>


                                    <tr class="success">
                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;">ZIP Code</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type="text" id="txtAddress" name="txtAddress"/>
                                            <input type="hidden" id="sLatitude" name="sLatitude" value="<%=latitude.item(0).getFirstChild().getNodeValue()%>"/>
                                            <input type="hidden" id="sLongitude" name="sLongitude" value="<%=longitude.item(0).getFirstChild().getNodeValue()%>"/>
                                            <input type="button" onclick="GetLocation();" value="Get Location" class="myButton"/></td>
                                    </tr>
                                    <tr class="success">
                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Location</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="city" id="city" value="<%=location.item(0).getFirstChild().getNodeValue()%>" disabled/>
                                            <input type="hidden" name="city1" id="city1" value="<%=location.item(0).getFirstChild().getNodeValue()%>"></td>
                                    </tr>


                                    <tr class="success">
                                        <td style="border-top-color: #599bb3; border-style: dashed; border-width: 1px; border: none"></td>
                                        <td style="border-top: #599bb3; border-style: dashed; border-width: 1px; border: none"><input type="submit" value="Update" class="myButton"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="Clear" onclick="return(OnCancel())" class="myButton"/></td>
                                    </tr>

                                </tbody>
                                <font color='red'> <div id="une"> </div> </font>
                            </table>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </body>
</html>
