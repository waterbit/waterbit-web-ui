
<%@page import="org.gadgeon.Config"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>Gadgeon IOT</title>
	<link rel="stylesheet" href="css/main.css" >
	<link rel="stylesheet" href="css/bstrap.css" >
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="css/dialog.css" >
          <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
                <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <script src="js/alertify.min.js"></script>
	<script src="js/dialog.js"></script>
        <script src="assets/nprogress/nprogress.js"></script>
	<link href="assets/nprogress/nprogress.css" rel="stylesheet" />
                  <style>
    .myButton {
	-moz-box-shadow: 0px 10px 14px -7px #276873;
	-webkit-box-shadow: 0px 10px 14px -7px #276873;
	box-shadow: 0px 10px 14px -7px #276873;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
	background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
	background-color:#599bb3;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:4px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Times New Roman;
	font-size:12px;
	font-weight:bold;
	padding:2px 10px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}
.myButton:active {
	position:relative;
	top:1px;
}



          

        </style>


    
   

    
    <% if (session.getAttribute("userid") != null) { 
         
int userid = (Integer)session.getAttribute( "userid" );
if(userid>1)
{
%>
    <script type="text/javascript">
function validatetxtbox()

{
    var oldpass = document.getElementById("oldpass").value;
            var psswd1 =document.getElementById("newpass").value;
            var psswd2 =document.getElementById("newpass1").value;            
            if(oldpass == "")
            {
                document.getElementById('une').innerHTML = "* Old password field should not be empty";
                document.getElementById("oldpass").focus();
                return(false);
            }
            else if(psswd1 == "")
            {
                document.getElementById('une').innerHTML = "* Please type a password";
                document.getElementById("newpass").focus();
                return(false);
            }
            else if(psswd1 != psswd2)
            {
                 document.getElementById('une').innerHTML = "* Password Must be equal";
                 document.getElementById("newpass").value ="";
                 document.getElementById("newpass1").value="";
                 document.getElementById("newpass").focus();
                 return(false);
            }
            else
            {
                return(true);
            }

}


</script>
    
    
</head>

   
    <body>
        <script>
  NProgress.set(0.0);
               NProgress.start();
              var mnc=0;
        </script>
	<%
	String[] values = request.getParameterValues("val");
		     if(values!=null)
		     {
		     if(values[0].equals("3"))
		     {%>  
		       <script type="text/javascript">
		       document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
			   if(document.readyState=='loaded' || document.readyState=='complete')
				alerify.alert("Old password is incorrect..!");
			}
			 </script>
			 <%
		      }

		     }%>
		     <div id="dialogoverlay"></div>
		<div id="dialogbox">
		  <div>
		    <div id="dialogboxbody"></div>
		    <div id="dialogboxfoot"></div>
		  </div>  
		</div>
	    
	<div class="container">
	     <%@include file="./userHeader.jsp" %>
	     <div class="row">
		 <div class="col-lg-6" >
		     <div class="panel panel-primary">
			 <div class="panel-heading">
			     Change Password
			 </div>
			 <form action="userChangePasswordAction.jsp" method="post" onsubmit="return(validatetxtbox())">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                     <table class="table">
                                      
                                        <tbody>
                                            <tr class="success">
                                                <td>Old Password </td><td> <input type="password" name="oldpass" id="oldpass"/></td>
                                            </tr>
                                            <tr class="success">
                                                <td>New Password </td><td> <input type="password" name="newpass" id="newpass"/></td>
                                            </tr>
                                             <tr class="success">
                                                 <td>Re-enter New Password</td><td> <input type="password" name="newpass1" id="newpass1"/></td>
                                            </tr>
                                            
                                            <tr class="success">
                                                <td></td>
                                                <td><input type="submit" class="myButton" value="   Ok    "/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" class="myButton" value="Clear"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <font color='red'> <div id="une"> </div> </font>
                                </div>
                                
                            </div>
                                </form>
		     </div>
		 </div>
	     </div>
	</div>
 <script>
            if(mnc==0)
    {
	NProgress.set(1);
    }
	mnc=1;
            
        </script>
</body>

<%
        }
      }
else
{
    response.sendRedirect("index.jsp");
            }
        %>
    <!-- END BODY -->
</html>
