
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
 <link rel="shortcut icon" href="images/favicon .ico" type="image/x-icon">
        <link rel="icon" href="images/favicon .ico" type="image/x-icon">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <script>
      function dispDate()
      {
          // $("#dates").style.display="block";
           //alert(document.getElementById("dates").style.display);
           document.getElementById("dates").style.display="block";
      }
      function change()
      {
          // $("#dates").style.display="block";
           //alert(document.getElementById("dates").style.display);
           document.getElementById("dates").style.display="none";
      }
      
    </script>
    <style>
        input{display:none !important;}
        input[ type="checkbox"]{display:inline !important;}
        input[ type="radio"]{display:inline !important;}
        input[ type="button"]{display:inline !important;}
        input[ type="submit"]{display:inline !important;}
        .cls{display:inline !important;}
    </style>
                  <style>
    .myButton {
	-moz-box-shadow: 0px 10px 14px -7px #276873;
	-webkit-box-shadow: 0px 10px 14px -7px #276873;
	box-shadow: 0px 10px 14px -7px #276873;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
	background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
	background-color:#599bb3;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:4px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Times New Roman;
	font-size:10px;
	font-weight:bold;
	padding:2px 10px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}
.myButton:active {
	position:relative;
	top:1px;
}
.square {
    background:#ffffff;
    width:50vw;
    height:10vw;
}
.square h1 {
    color: #599bb3;
}

  /*select {
  margin: 1px;
  border: 1px solid #111;
  background: transparent;
  width: 150px;
  padding: 3px 10px 5px 5px;
  font-size: 10px;
  border: 1px solid #ccc;
  height: 30px;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  background: url(http://www.stackoverflow.com/favicon.ico) 96% / 15% no-repeat #eee;
}*/
</style>
    <%
        String   strTemp;
        strTemp="";
       // session.setAttribute("userid", 1);
        if (session.getAttribute("userid") != null) { 
         
//int userid = (Integer)session.getAttribute( "userid" );

%>
  <div class="square" style="background-color: #dff0d8;height: 50px;">
<form name="frm" method="post" >
     <select name="cmb"  id="cmbsensor" >
      <!--  <option value="gadgeon_temperature_sensor1">gadgeon_temperature_sensor1</option>
        <option value="gadgeon_temperature_sensor2">gadgeon_temperature_sensor2</option>-->
    <%
       try
       {
           if(userid==1)
            {
             
              URL jsonpage = new URL("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getSensorList");
           
        URLConnection urlcon = jsonpage.openConnection();
   
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getSensorList");
        NodeList sensorname =  doc.getElementsByTagName("sensorname");
        NodeList sensortype = doc.getElementsByTagName("sensortype");
        NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
       // NodeList alarmcount = doc.getElementsByTagName("alarmcount");
        
       
    
                                                for(int i=0;i<=sensorname.getLength()-1;i++)
                                                {
                                                    if(!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")){
                                                   
                                                        %> <option value="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" selected="selected"><% String senval= sensorname.item(i).getFirstChild().getNodeValue(); 
                                                senval=senval.substring(0,senval.lastIndexOf("_"));
                                                out.print(senval); %></option>
                                                     <%
                                                    }
                                                }
            }  
           else
           {
               URL jsonpage = new URL("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+userid+"/getSensorListByUserId");
           
        URLConnection urlcon = jsonpage.openConnection();
   
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+userid+"/getSensorListByUserId");
        NodeList sensorname =  doc.getElementsByTagName("sensorname");
        NodeList sensortype = doc.getElementsByTagName("sensortype");
        NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
       // NodeList alarmcount = doc.getElementsByTagName("alarmcount");
        
       
                                                int checkJ=0;
                                                for(int i=0;i<=sensorname.getLength()-1;i++)
                                                {
                                                    if(request.getParameter("cmb")==null)
                                                    {
                                                       
                                                    }
                                                    else
                                                    {
                                                        if(request.getParameter("cmb").equals(sensorname.item(i).getFirstChild().getNodeValue()))
                                                        {checkJ=0;
                                                            %>
                                                       
                                                            <%
                                                        }
                                                    }
                                                    if(!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")){
                                                   
                                                    %> <option <%= checkJ++==0?" selected ":"" %> value="<%=sensorname.item(i).getFirstChild().getNodeValue()%>"><% String senval= sensorname.item(i).getFirstChild().getNodeValue(); 
                                                senval=senval.substring(0,senval.lastIndexOf("_"));
                                                out.print(senval);  %></option>
                                                     <%
                                                    }
                                                }
           }
       
       }catch(Exception e)
       {
         //  out.print(e.toString());
       }
                                               
                                               
                                                %> 
    </select>
     &nbsp;&nbsp;&nbsp;
     <%
            int weekly = 0;
            int monthly = 0;
            int tSer = 0;
            if(request.getParameter("rad")==null)
            { 
                weekly = 1;
            }
            else 
            {
                    if(request.getParameter("rad").equals("weekly"))
                   {
                         weekly = 1;
                   }

                   if(request.getParameter("rad").equals("Monthly"))
                   {
                       monthly =1;
                   }
                   if(request.getParameter("rad").equals("time"))
                   {
                       tSer = 1;
                   }
            }
      %>
    
       <input class="cls" type="radio" id="1" name="rad" value="weekly" onclick="change()" <%= weekly++==1?" checked ":"" %>><label style=font-size:10 for="1"> Weekly</label>
            &nbsp;&nbsp;&nbsp;<input type="radio" id="2" name="rad" value="Monthly" onclick="change()" <%= monthly++==1?" checked ":"" %>><label style=font-size:10 for="2"> Monthly</label>
           &nbsp;&nbsp;&nbsp;<input type="radio" id="3" name="rad" value="time" onclick="dispDate()" <%= tSer++==1?" checked ":"" %>><label style=font-size:10 for="3"> Time Series</label>
    &nbsp;&nbsp;&nbsp;<input class="myButton"  type="submit"  name="btnSubmit" value="Plot Graph" >
      </div>
    <div id="dates" style="display: none;height:50px !important;" >
        Date From: <input type="text" name="from"  class="cls" value ='from' id="from">Date To: <input name="to"  class="cls" value='to' type="text" id="to">
            <br>
            
      </div>
</form>
    
    <script type="text/javascript" src="js/dygraph-dev.js"></script>
    <script>
        var check=0;
    <%
      if(request.getParameter("cmb")==null)
      {
          %>
              check=1;
              <%
      }
            
            %>
    </script> 
    <div id="div_g" style="width:95%; height:520px;"></div>
    <script>
        $(function() {
             $( "#from" ).datepicker();
    $( "#from" ).datepicker("option", "dateFormat","dd-mm-yy");
  });
   $(function() {
       $( "#to" ).datepicker();
    $( "#to" ).datepicker("option", "dateFormat","dd-mm-yy");
  });
    </script>
    <%
       
    //String url="";
      
    URL url;
    SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");
    try{
    if(request.getParameter("rad")==null)
    {
        strTemp="[]";
        %>
        <script>     document.getElementById("div_g").innerHTML="No data Available";</script>
        <%
    }
    else
    {
        
        if(request.getParameter("rad").equals("weekly"))
        {
            //   url="";
            Calendar now = Calendar.getInstance();
             now.add(Calendar.DATE, 1);
            Date dateto= now.getTime();
             now.add(Calendar.DATE, -7);
             Date datefrom=now.getTime();
              url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydatabydate?sensortype="+request.getParameter("cmb")+"&fromdate="+format.format(datefrom) +"&todate="+ format.format( dateto));
          //  out.print(request.getParameter("cmb")+dateto+"**"+datefrom);
	}
        else if(request.getParameter("rad").equals("Monthly"))
        {
               Calendar now = Calendar.getInstance();
                now.add(Calendar.DATE, 1);
            Date dateto= now.getTime();
             now.add(Calendar.MONTH, -1);
             Date datefrom=now.getTime();
             url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydatabydate?sensortype="+request.getParameter("cmb")+"&fromdate="+ format.format( datefrom)+"&todate="+format.format( dateto));
            //out.print(request.getParameter("cmb")+dateto+"**"+datefrom);
        }
        
        else
        {
     //URL 
            url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydatabydate?sensortype="+request.getParameter("cmb")+"&fromdate="+request.getParameter("from")+"&todate="+request.getParameter("to"));
            //out.print(url.toString());

            
        }
        
        
        strTemp=url.toString();
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
        int b2 = 0;
        strTemp = br.readLine();
        
        if(strTemp.trim()=="[]")
        {
          
        }
    
         //   Double b1 = Double.parseDouble(strTemp);
         //  b2 = b1.intValue();
        
        //out.print(strTemp);
        //System.out.println("jjj"+strTemp);
    
    // String strTemp = "";
   
        }
    }
    catch(Exception e)
            {
                strTemp="[]";
            }
        }
        else
        {
        %>
        <script>window.location='index.jsp';</script>
        <%
        }
              
        %>
    <script>  
       // alert('hai');
var liveData;
      var data=[];
       var t = new Date();
       
 var data11='<%=strTemp %>';
 //alert(data11);
 livedata(1,data11);
 <%
        NodeList xlabel=null;
        NodeList ylabel=null;
        NodeList gmin=null;
        NodeList gmax=null;
        try{
     
           DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        DocumentBuilder db1 = dbf1.newDocumentBuilder();
        Document doc1 = db1.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+session.getAttribute("userid")+"/"+request.getParameter("cmb")+"/getSensorListByName");
         xlabel =  doc1.getElementsByTagName("xlabel");
         ylabel = doc1.getElementsByTagName("ylabel");
         gmin = doc1.getElementsByTagName("gmin");
         gmax = doc1.getElementsByTagName("gmax");
        }
        catch(Exception e)
        {}
        %>
var g = new Dygraph(
          document.getElementById("div_g"),
          data,
          {
            
            drawPoints: true,
                            showRoller: true,
                            xlabel: '<%if(request.getParameter("cmb")!=null)out.print(ylabel.item(0).getFirstChild().getNodeValue());%>',
                            ylabel: '<%if(request.getParameter("cmb")!=null)out.print(xlabel.item(0).getFirstChild().getNodeValue());%>',
                            valueRange: [<%if(request.getParameter("cmb")!=null)out.print(gmin.item(0).getFirstChild().getNodeValue());%>,<%if(request.getParameter("cmb")!=null)out.println(gmax.item(0).getFirstChild().getNodeValue());%>],
                            labels: ['Time', 'Value'],
                            title:'<% if(request.getParameter("cmb")!=null)
                                             out.print(request.getParameter("cmb").substring(0, request.getParameter("cmb").lastIndexOf("_")));%>',
            showRangeSelector: true,
            rangeSelectorHeight: 30,
            rangeSelectorPlotStrokeColor: 'green',
            rangeSelectorPlotFillColor: 'lightyellow'
          }
      );
      if(data11.trim().length==2)
    
          if(check==0)
              $("#div_g").html("No data Available");
      else
          $("#div_g").html(""); 
           function livedata(id,data11)
{
    //alert(data11);
   //alert($("#roll14").html());
   data11=data11.trim();
  // alert(data11.length);
   if(data11.length!=2){
       
       //alert('ff');
    if(id==3)
        data=[];
   // alert("10");
    data11=data11.trim();
    //  alert("Data: " + data11 + "\nStatus: " + status);
      // var data11='<%=strTemp%>';
       // alert(data11);
        var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       // var dat =dt.split(",");
       var dates=[];
       var vals=[];
       
        for(var i=0;i<dt.length-2;i++)
        {
          
           if(id==2) data.shift();
            //alert(dt[i].length-dt[i].indexOf("=")+1);
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
          //  alert(dates[i]);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
            dts=dates[i].split("T");
           // alert("***"+dts[0]+"$$$"+dts[1]+"&&");
            dts1=dts[0].split("-");
            dts2=dts[1].split("+");
            dts3=dts2[0].split(":");
         //   dts4=dts2[2].split(".");
           // data.push([new Date(new Date().getTime()-i*1000), vals[i]]);
        //   alert(dts3[0]+"&&"+dts3[1]+"^^"+dts3[2]);
            data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2]), vals[i]]);
          //  alert(new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0));
          // alert(new Date(new Date().getTime()-i*1000));
         //alert(new Date((dates[i].replace(" ","T"))));
        // t=new Date();
        // t=new Date();
        // var x = new Date(t.getTime() - i * 1000);
          //        data.push([x, Math.random()*40]);
        }
     
 }
 else
 {
     $("#div_g").html("No data Available");
    // clearInterval(liveData);
 }
}

        
   
            </script>
            <style>
.dygraph-axis-label-x
{
 // width:130px;
 // height:70px;
 // margin-left: -10px;
 // -ms-transform:rotate(270deg); /* IE 9 */
 // -moz-transform:rotate(270deg); /* Firefox */
 // -webkit-transform:rotate(270deg); /* Safari and Chrome */
 // -o-transform:rotate(270deg); /* Opera */
}
.dygraph-xlabel{
    margin-top: 30px;
}
</style>