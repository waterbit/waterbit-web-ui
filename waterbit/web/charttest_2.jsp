
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>
<!DOCTYPE html>
<html lang="en">
    <head>

        <title>Vital Herd</title>


        <link rel="stylesheet" href="css/main.css" >
        <link rel="stylesheet" href="css/bstrap.css" >
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" >
        <link rel="stylesheet" href="css/dialog.css" >
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
	  <script src="js/smooth-plotter.js"></script>
        <style> input{display:none !important;}
            
            .sensoredit{

            }
            .sensoredit:hover {
                cursor:pointer;
            }


        </style>

        <style>
            .mysensor{


            }
            .myevent{


            }
            .mysensor:hover {
                cursor:pointer;
            }
            .myevent:hover {
                cursor:pointer;
            }
            .col{
                margin: 50px;
            }
        </style>
        <style>
            .myButton {
                -moz-box-shadow: 0px 10px 14px -7px #276873;
                -webkit-box-shadow: 0px 10px 14px -7px #276873;
                box-shadow: 0px 10px 14px -7px #276873;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
                background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
                background-color:#599bb3;
                -moz-border-radius:3px;
                -webkit-border-radius:3px;
                border-radius:4px;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-family:Times New Roman;
                font-size:12px;
                font-weight:bold;
                padding:2px 10px;
                text-decoration:none;
                text-shadow:0px 1px 0px #3d768a;
            }
            .myButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
                background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
                background-color:#408c99;
            }
            .myButton:active {
                position:relative;
                top:1px;
            }
            .loaddiv
            {
                margin-left:37%;margin-top:11%;
            }
            
             @media only screen 
            and (min-width : 320px) 
            and (max-width : 768px)
            {
            /* Styles */
                .loaddiv
                {
                    margin-left:30% !important;margin-top:30% !important;
                }
            }

            /* Smartphones (landscape) ----------- */
            @media only screen 
            and (min-width : 321px) {
            /* Styles */
            
            }

            /* Smartphones (portrait) ----------- */
            @media only screen 
            and (max-width : 320px) {
            /* Styles */
            
            }

            /* iPads (portrait and landscape) ----------- */
            @media only screen 
            and (min-width : 1024px) 
            and (max-width : 1200px) 
             {
            /* Styles */
                .col{
                margin: 20px 10px !important;
            }
            }

            /* iPads (landscape) ----------- */
            @media only screen 
            and (min-width : 768px) 
            and (max-width : 1024px) 
            and (orientation : landscape) {
            /* Styles */
                .loaddiv
                {
                    margin-left:30% !important;margin-top:15% !important;
                }
            }

            /* iPads (portrait) ----------- */
            @media only screen 
            and (min-width : 768px) 
            and (max-width : 1024px) 
            and (orientation : portrait) {
            /* Styles */
            .loaddiv
            {
                margin-left:30% !important;margin-top:15% !important;
            }
            }

            /* Desktops and laptops ----------- */
            @media only screen 
            and (min-width : 1224px) {
            /* Styles */
            }

            /* Large screens ----------- */
            @media only screen 
            and (min-width : 1824px) {
            /* Styles */
            }

            /* iPhone 4 ----------- */
            @media
            only screen and (-webkit-min-device-pixel-ratio : 1.5),
            only screen and (min-device-pixel-ratio : 1.5) {
            /* Styles */
            }
            
        </style>


        <%
            // String datas="[{x1=2015-02-10T14:36:36+05:30, y1=3.1}, {x1=2015-02-10T14:35:53+05:30, y1=3.2}, {x1=2015-02-10T14:35:36+05:30, y1=3.3}, {x1=2015-02-10T14:34:34+05:30, y1=3.4}, {x1=2015-02-10T14:33:56+05:30, y1=3.27419}, {x1=2015-02-10T14:33:42+05:30, y1=3.5}, {x1=2015-02-10T14:26:05+05:30, y1=1.33871}, {x1=2015-02-10T14:25:05+05:30, y1=1.33226}, {x1=2015-02-10T14:24:33+05:30, y1=1.34516}, {x1=2015-02-10T14:24:15+05:30, y1=1.34516}]";
            try {
                System.out.println(session.getAttribute("userid"));
                if (session.getAttribute("userid") != null) {

                    int userid = (Integer) session.getAttribute("userid");
                    System.out.println(userid);
                   // int userid = 25;
                    if (userid > 1) {
        %> 
        <script src="js/alertify.min.js"></script>
        <script type="text/javascript">
            var colors=["red","blue","green","yellow"];
            var g;
            var data = [];
            var t = new Date();

            var xval;
            var yval;
            var min;
            var max;
             var titles;
            var liveDataInt=[];
            var checkflag = "false";
            /*     setInterval(function () {
             // alert("haii");
             $.get("getsensordetails.jsp", function (data11, status) {
             
             // alert(data11.trim()); 
             var cnt = document.getElementsByName("rbox[]");
             var img = document.getElementsByName("im");
             var iconim = document.getElementsByName("icon");
             var uname;
             var sensorstatus = data11.trim().split("*");
             var table = document.getElementById("mytab1");
             
             var x = table.rows.length - 1;
             var y = sensorstatus.length - 1;
             //  alert(x);
             if (y > x)
             {
             //  alert("###");
             $.ajaxSetup ({
             // Disable caching of AJAX responses 
             cache: false
             });
             
             window.setTimeout(function () {
             window.location.reload();
             
             }, 1000);
             alertify.success("New Sensor Found");
             }
             else if (y < x)
             {
             
             $.ajaxSetup ({
             // Disable caching of AJAX responses
             cache: false
             });
             setTimeout(function(){
             window.location.reload();
             }, 1000);
             
             alertify.error("Sensor Deleted");
             }
             else
             {
             // alert(sensorstatus.length);
             for (var i = 0; i < sensorstatus.length-1 ; i++)
             {
             
             var senname = sensorstatus[i].substring(0, sensorstatus[i].indexOf(":"));
             var sentype = sensorstatus[i].substring(sensorstatus[i].indexOf(":") + 1, sensorstatus[i].indexOf(";"));
             var seninfo = sensorstatus[i].substring(sensorstatus[i].indexOf(";") + 1, sensorstatus[i].indexOf("<"));
             var status = sensorstatus[i].substring(sensorstatus[i].indexOf("<") + 1, sensorstatus[i].indexOf(">"));
             var senalarmcnt = sensorstatus[i].substring(sensorstatus[i].indexOf(">") + 1, sensorstatus[i].indexOf("#"));
             var alarmstatus = sensorstatus[i].substring(sensorstatus[i].indexOf("#") + 1, sensorstatus[i].indexOf("$"));
             var lastuptime = sensorstatus[i].substring(sensorstatus[i].indexOf("$") + 1);
             var table = document.getElementById("mytab1");
             
             var name = senname.split("_");
             
             //row.cells[5].innerHTML = alarmstatus;
             var table1 = document.getElementById("mytab2");
             row1 = table1.rows[i + 1];
             row1.cells[0].innerHTML = name[0];
             
             row1.cells[2].innerHTML = seninfo;
             row1.cells[3].innerHTML = lastuptime;
             
             
             for (var j = 0; j < img.length; j++)
             {
             //alert(img[j].alt);
             
             if (img[j].alt.trim() == senname.trim())
             {
             //img[j].src = "images/live1.png";
             if (status.trim() == "0")
             {
             img[j].src = "images/red_offline_icon.png";
             }
             else
             {
             img[j].src = "images/live1.png";
             break;
             }
             }
             }
             
             }
             }
             
             });
             }, 4000);
             */
            function check(field)
            {

                if (checkflag == "false")
                {
                    if (isNaN(document.myform.elements["rbox[]"].length))
                    {
                        document.myform.elements["rbox[]"].checked = true;
                        checkflag = "true";

                    }
                    else
                    {
                        for (i = 0; i < field.length; i++)
                        {
                            field[i].checked = true;
                        }
                        checkflag = "true";
                    }
                    rowSelecteventAll("#48D1CC");

                    return "Clear All";
                }
                else
                {
                    if (isNaN(document.myform.elements["rbox[]"].length))
                    {
                        document.myform.elements["rbox[]"].checked = false;
                        checkflag = "false";

                    }
                    else
                    {
                        for (i = 0; i < field.length; i++)
                        {
                            field[i].checked = false;
                        }
                        checkflag = "false";
                    }
                    rowSelecteventAll("#dff0d8");
                    return "Select All";

                }
            }

            function OnButton1()
            {

                var count = 0;
                // alert(document.myform.elements["rbox[]"].length);
                if (isNaN(document.myform.elements["rbox[]"].length))
                {
                    //  uid = document.myform.elements["rbox[]"].value;
                    count = 1;
                }
                else
                {
                    for (i = 1; i < document.myform.elements["rbox[]"].length; i++)
                    {
                        if (document.myform.elements["rbox[]"][i].checked == true)
                        {

                            count++;
                        }
                    }
                }
                if (count >= 1)
                {
                    //alert("haii");
                    document.myform.action = "userClearAlarm.jsp";
                    var list = document.myform.elements["rbox[]"];
                    var arg = "";
                    for (var icnt = 1; icnt < list.length; icnt++)
                    {
                        if (list[icnt].checked)
                            if (arg == "")
                                arg = "rbox[]=" + list[icnt].value;
                            else
                                arg += "&" + "rbox[]=" + list[icnt].value;
                    }
                    //alert(arg);
                    $.get("userClearAlarm.jsp?" + arg, function (data11, status) {
                        //alert(data11+status);
                        alertify.success("Alarm cleared");
                    });

                    //   document.myform.submit();
//               alertify.alert("haiii");
                    return true;
                }
                else if (count < 1)
                {
                    alertify.alert("Please select atleast one sensor");
                    return false;
                }
            }

            function rewrite(divid, txt,i)
            {
                // alert(txt);

                //  clearInterval(liveDataInt);
                // data=[];
                /*  $.get("graphdetail.jsp?sensor=" + txt, function (data12, status) {
                 data12 = data12.trim().split(":");
                 
                 xval = data12[0];
                 alert(xval);
                 yval = data12[1];
                 min = data12[2];
                 max = data12[3];
                 });
                 */

                $.ajaxSetup({
                    // Disable caching of AJAX responses */
                    cache: false
                });
                $.get("livedatademo.jsp?sensor=" + txt, function (data11, status) {
                        senval = txt;
                        // document.getElementById("sensorlabel").innerHTML = senval;
                        // alert(data11); 
                        // alert(txt+" set");
                    //  alert(divid+data11.trim().length);
                        if(data11.trim().length==2){
                            $("#"+divid).html("<div class='loaddiv' >No Data Available for "+txt.substring(txt.indexOf("_")+1)+" since 5 hours </div> ");
                            
                        }
                        else{
                        $.get("graphdetail.jsp?sensor=" + txt, function (data12, status) {
                    data12 = data12.trim().split(":");
                    //alert("haii@@@i"+data12[0])
                    xval = data12[0];
                    yval = data12[1];
                    min = data12[2];
                    max = data12[3];
                      titles=txt.substring(txt.lastIndexOf("_")+1);
                    livedatadiv(3, data11, divid, txt);
                    
                });
                       }
                        

                    });
                liveDataInt[i] = window.setInterval(function () {

                    $.get("livedatademo.jsp?sensor=" + txt, function (data11, status) {
                        senval = txt;
                        // document.getElementById("sensorlabel").innerHTML = senval;
                        // alert(data11); 
                        // alert(txt+" set");
                    //  alert(divid+data11.trim().length);
                        if(data11.trim().length==2){
                            $("#"+divid).html("<div class='loaddiv' >No Data Available for "+txt.substring(txt.indexOf("_")+1)+" since 5 hours </div>  ");
                            
                        }
                        else{
                        $.get("graphdetail.jsp?sensor=" + txt, function (data12, status) {
                    data12 = data12.trim().split(":");
                    //alert("haii@@@i"+data12[0])
                    xval = data12[0];
                    yval = data12[1];
                    min = data12[2];
                    max = data12[3];
                      titles=txt.substring(txt.lastIndexOf("_")+1);
                    livedatadiv(3, data11, divid, txt);
                    
                });
                       }
                        

                    });
                    //callfunc(yval, xval, min, max);

                    //alert("haiii"+yval);


                }, 60000
                        );
                // alert("!!!"+txt);

                //alert(txt);0a1b3c4d5e6f
            }


            /*  $('tr').click(function (event) {
             
             if (!$('input[type=radio]', this).prop('checked'))
             $('input[type=radio]', this).prop('checked', true);
             else
             $('input[type=radio]', this).prop('checked', false);
             });*/
            function rowSelect(row, sensor)
            {
                //row.className="";
                     
                var se = sensor.trim();
                $.get("livedatalistsensor.jsp?type=" + se, function (data11, status) {
                    // alert(data11+status);
                    // alertify.success("Alarm cleared");

                    //alert(select.options.length);
                    /*for (var i = 0; i <= length; i++) {
                     select.options[i] = null;
                     }*/

                    // alert(select.options.length);


                    var data = data11.trim().split("**");

                    dydiv(data.length - 1, data);




                    // alert("2"+" "+data.length);
                });
                var tr = document.getElementsByClassName("mysensor");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = "#dff0d8";

                }
                row.style.background = "#48D1CC";
                // var str=row.cells[0].innerHTML.trim();
                //    row.cells[0].innerHTML=str.substr(0,str.length-1)+" checked >"
                //alert(str.substr(0,str.length-1));
                //rewrite(sensor);
            }
                 function optionSelect( sensor)
            {
                //row.className="";
                
                     if(sensor=="-1")
                         return;
                     
                var se = sensor.trim();
                $.get("alarmcount.jsp?enddevice=" + se, function (data11, status) {
                            $("#alarmcount").html("Total Event Count :"+data11.trim());
                     });
                $.get("livedatalistsensor.jsp?type=" + se, function (data11, status) {
                    // alert(data11+status);
                    // alertify.success("Alarm cleared");

                    //alert(select.options.length);
                    /*for (var i = 0; i <= length; i++) {
                     select.options[i] = null;
                     }*/

                    // alert(select.options.length);


                    var data = data11.trim().split("**");

                    dydiv(data.length - 1, data);




                    // alert("2"+" "+data.length);
                });
           
                // var str=row.cells[0].innerHTML.trim();
                //    row.cells[0].innerHTML=str.substr(0,str.length-1)+" checked >"
                //alert(str.substr(0,str.length-1));
                //rewrite(sensor);
            }
            function getDataHR (newMinutes) {
    MINS_PER_YEAR = 24 * 365 * 60;
    MINS_PER_MONTH = 24 * 30 * 60;
    MINS_PER_WEEK = 24 * 7 * 60;
    MINS_PER_DAY = 24 * 60;
    MINS_PER_HRS=60;
    minutes = newMinutes;
    /*years = Math.floor(minutes / MINS_PER_YEAR);
    minutes = minutes - years * MINS_PER_YEAR;
    months = Math.floor(minutes / MINS_PER_MONTH);
    minutes = minutes - months * MINS_PER_MONTH;
    weeks = Math.floor(minutes / MINS_PER_WEEK);
    minutes = minutes - weeks * MINS_PER_WEEK;*/
    days = Math.floor(minutes / MINS_PER_DAY);
    minutes = minutes - days * MINS_PER_DAY;
    hours= Math.floor(minutes / MINS_PER_HRS);
    minutes = minutes - hours * MINS_PER_HRS;
    return  days + " days "+hours+" hrs " + minutes + " mins";
    //return hrData; // 1 year, 2 months, 2 week, 2 days, 12 minutes
}
            function dydiv(count, data) {
                var output = document.getElementById('output');
                var i = 0;
                var val = "";
                $("#output").html("");
               // alert(liveDataInt.length);
                for(var j=0;j<liveDataInt.length;j++)
                {
                    clearInterval(liveDataInt[j]);
                }
                   
                liveDataInt=[];
              //  alert(count+"@@@"+liveDataInt.length);
                while (i < count)
                { 

                    if (!document.getElementById("div_g" + i))
                    {
                        var ele = document.createElement("div");
                        ele.setAttribute("id", "div_g" + i);
                        ele.setAttribute("class", "panel panel-primary col");
                        ele.setAttribute("style", "height: 290px;width: 90%; background-color: white;float:left;margin:40px;margin-bottom: 0px;text-transform: capitalize;");
                        ele.innerHTML = "<div class='loaddiv' >"+ data[i].substring(data[i].lastIndexOf("_")+1)+" graph loading ...</div> " ;
                        /* var ele1=document.createElement("div");
                         ele1.setAttribute("id", "timedrpact" + i);
                         ele1.setAttribute("class", "panel panel-primary");
                         ele1.setAttribute("style", " background-color: #dff0d8");
                         var ele2=document.createElement("div");
                         ele2.setAttribute("class", "col-lg-6");
                         var ele3=document.createElement("div");
                         ele3.setAttribute("class", "panel-heading");
                         $('#out').append(ele2);  
                         $(ele2).append(ele);
                         $(ele).append(ele3)
                         
                         */

                        output.appendChild(ele);
                      
                        rewrite("div_g" + i, data[i],i);

                    }

                    i++;


                }
            }

            function livedatadiv(id, data11, divid, txt)
            {
//
                // alert(data11);

                data = [];
                

                //data11='[{x1=2015-02-10T14:36:36+05:30, y1=3.1}, {x1=2015-02-10T14:35:53+05:30, y1=3.2}, {x1=2015-02-10T14:35:36+05:30, y1=3.3}, {x1=2015-02-10T14:34:34+05:30, y1=3.4}, {x1=2015-02-10T14:33:56+05:30, y1=3.27419}, {x1=2015-02-10T14:33:42+05:30, y1=3.5}, {x1=2015-02-10T14:26:05+05:30, y1=1.33871}, {x1=2015-02-10T14:25:05+05:30, y1=1.33226}, {x1=2015-02-10T14:24:33+05:30, y1=1.34516}, {x1=2015-02-10T14:24:15+05:30, y1=1.34516}]';
                //  alert(data11);
                $.ajaxSetup({
                    // Disable caching of AJAX responses */
                    cache: false
                });
                
                data11 = data11.trim();
                var datalist=data11.split("***");
                var lastSeen=0;
                data11=datalist[0];
                if(datalist.length==2)
                    lastSeen=datalist[1];
                // alert(data11.length);
                //alert($("#div_g").html());
                if (data11.length != 2) {
                   
             if(txt.indexOf("rumencontraction")>-1){

        var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       if($("#div_g").html()=="No data Available")data=[];
       if($("#div_g").html()!="No data Available"&&id==3)
           id=2;
       var dates=[];
       var vals=[];
           data=[];
       var lastDate;
        for(var i=0;i<dt.length;i++)
        {
           
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
            dts=dates[i].split("T");
            dts1=dts[0].split("-");
            dts2=dts[1].split("Z");
            dts3=dts2[0].split(":")
       
            data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0), parseFloat(vals[i])]);
            lastDate=new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0);
        }
        
          

           {  
	                     if(titles=="rumencontraction")
        {
           titles="Rumen Contraction";
        }else if(titles=="rumentemperature")
        {
            titles="Rumen Temperature";
        }else if(titles=="gatewayhumidity")
        {
            titles="Gateway Humidity";
        }else if(titles=="gatewaytemperature")
        {
            titles="Gateway Temperature";
        }
               g = new Dygraph(document.getElementById(divid), data,
                          {
                            drawPoints: false,
                            showRoller: true,
                            ylabel:yval,
                            xlabel: xval,
                             stackedGraph: true,
                             title:titles+" <div style='float:right;font-size:16px;font-weight: normal;' >Time since last data : "+getDataHR(parseInt(lastSeen))+"</div> ",
                            valueRange: [min, max],
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			     plotter: smoothPlotter,
                        strokeWidth: 2
                          });
                          g.ready(function() {
                               var annotations = [];
                 for(var i=0;i<dt.length;i++)
                    {
                        

                        dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
                        vals[i]=dt[i].substr(dt[i].indexOf("counter=")+8);
                        dts=dates[i].split("T");
                        dts1=dts[0].split("-");
                        dts2=dts[1].split("Z");
                        dts3=dts2[0].split(":")

                        //data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0), parseFloat(vals[i])]);
                        //lastDate=new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0);
                        
                        annotations.push( 
                        {
                         series:"Value",
                          x: new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0).getTime(),
                          shortText:  parseFloat(vals[i]),
                          text: "Count :"+ parseFloat(vals[i])
                        });
                        
                    }
                     g.setAnnotations(annotations);
                    
                          });
                          }
                          
            }
            else
            {
                    var details = data11.substr(1, data11.length);
                    details = details.substr(0, details.length - 2);
                    var dt = details.split("},");
                    // var dat =dt.split(",");
                    if ($(divid).html() == "No data Available")
                        data = [];
                    if ($(divid).html() != "No data Available" && id == 3)
                        id = 2;
                    var dates = [];
                    var vals = [];
                    if (dt.length != 10)
                        data = [];
                    for (var i = 0; i < dt.length ; i++)
                    {

                        // if(id==2&&dt.length==10){ data.shift();}

                        //alert(dt[i].length-dt[i].indexOf("=")+1);
                        dates[i] = dt[i].substr(dt[i].indexOf("=") + 1, dt[i].indexOf(",") - dt[i].indexOf("=") - 1);
                        //  alert(dates[i]);
                        vals[i] = dt[i].substr(dt[i].indexOf("y1=") + 3);
                        dts = dates[i].split("T");
                        // alert("***"+dts[0]+"$$$"+dts[1]+"&&");
                        dts1 = dts[0].split("-");
                        dts2 = dts[1].split("Z");
                        dts3 = dts2[0].split(":")
                        //   dts4=dts2[2].split(".");
                        // alert(new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0));
                        data.push([new Date(dts1[0], dts1[1] - 1, dts1[2], dts3[0], dts3[1], dts3[2], 0), parseFloat(vals[i])]);
                           lastDate=new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0);
                        //alert(new Date((dates[i].replace(" ","T"))));
                        // t=new Date();
                        // t=new Date();
                        // var x = new Date(t.getTime() - i * 1000);
                        //        data.push([x, Math.random()*40]);
                    }


                    /*   if(!$(divid).html()=="No data Available")
                     {  //alert("h");
                     g = new Dygraph(document.getElementById(divid), data,
                     {
                     drawPoints: true,
                     showRoller: true,
                     ylabel: 'Temperature (C)',
                     xlabel: 'Time',
                     valueRange: [-40, 80],
                     labels: ['Time', 'Random'],
                     interactionModel: Dygraph.Interaction.nonInteractiveModel_
                     });
                     
                     }
                     if(id==2)    g.updateOptions( { 'file': data } );*/
                    //alert(data);
                    
                     if(titles=="rumencontraction")
        {
           titles="Rumen Contraction";
        }else if(titles=="rumentemperature")
        {
            titles="Rumen Temperature";
        }else if(titles=="gatewayhumidity")
        {
            titles="Gateway Humidity";
        }else if(titles=="gatewaytemperature")
        {
            titles="Gateway Temperature";
        }
	smoothPlotter.smoothing =.7;
                    if (data11.length != 0)
                        g = new Dygraph(document.getElementById(divid), data,
                                {
                                    drawPoints: true,
                                    showRoller: true,
                                    ylabel: yval,
                                    xlabel: xval,
                                    valueRange: [min, max],
                                     stackedGraph: true,
                                     title:titles+" <div style='float:right;font-size:16px;font-weight: normal;' >Time since last data: "+getDataHR(parseInt(lastSeen))+"</div>",
                                     color:colors[divid.replace("div_g","")],
                                    labels: ['Time', 'Value'],
                                    interactionModel: Dygraph.Interaction.nonInteractiveModel_,
				    plotter: smoothPlotter,
             strokeWidth: 2
                                });
                    //   alert('new');
                }}
                else
                {
                    clearInterval(liveDataInt);

                    $("#"+divid).html("<div class='loaddiv' >No Data Available</div> ");

                }
            }
            function rowSelectevent(row, sensor)
            {
                //row.className="";

                var tr = document.getElementsByClassName("myevent");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = "#dff0d8";

                }
                row.style.background = "#48D1CC";
                var str = row.cells[0].innerHTML.trim();
                row.cells[0].innerHTML = str.substr(0, str.length - 1) + " checked >"
                //alert(str.substr(0,str.length-1));
                //rewrite(sensor);
            }
            function rowSelecteventAll(color)
            {
                //row.className="";
                var tr = document.getElementsByClassName("myevent");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = color;
                    if (color != "#dff0d8") {
                        var str = tr[i].cells[0].innerHTML.trim();
                        tr[i].cells[0].innerHTML = str.substr(0, str.length - 1) + " checked >"
                    }
                }
                //row.style.background ="#48D1CC";
                //var str=row.cells[0].innerHTML.trim();
                //row.cells[0].innerHTML=str.substr(0,str.length-1)+" checked >"
                //alert(str.substr(0,str.length-1));
                //rewrite(sensor);
            }
            function toUTC(/*Date*/date) {
    return Date.UTC(
        date.getFullYear()
        , date.getMonth()
        , date.getDate()
        , date.getHours()
        , date.getMinutes()
        , date.getSeconds()
        , date.getMilliseconds()
    );
}

        </script>

    </head>
    <body style="height: 100%;">


        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>  
        </div>

        <div class="container">  


            <%@include file="./userHeader.jsp" %>
            <script type="text/javascript" src="js/dygraph-dev.js"></script>


            <div class="row">


                <div class="col-lg-6" style="width:100% !important;">
                    <div class="panel panel-primary" style=" background-color: #dff0d8">
                        <div class="panel-heading">  <select name="cmb"  id="cmbsensor" style="font-size:13px !important;color : black !important" onchange="optionSelect(this.value)"> 
                                <option value="-1" >---Select End Device---</option>
                                <%
                        String firstDevice="";
                                                
                                                    URL jsonpage1 = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/GetEndPointDevice");
                                                    URLConnection urlcon1 = jsonpage1.openConnection();

                                                    DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();

                                                    DocumentBuilder db1 = dbf1.newDocumentBuilder();

                                                    Document doc1 = db1.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/GetEndPointDevice");
                                                    NodeList endDeviceName1 = doc1.getElementsByTagName("EndPointDeviceId");
                                                    NodeList info1 = doc1.getElementsByTagName("DeviceInfo");
                                                    NodeList location1 = doc1.getElementsByTagName("location");

                                                    //    out.println(sensorstatus);
                                                    // NodeList alarmcount = doc.getElementsByTagName("alarmcount");
                                                    int jk = 0;

                                                    for (int ik = 0; ik <= endDeviceName1.getLength() - 1; ik++) {


                                            %> 
                                              { %> <option <%=firstDevice.equals("")?" selected ":" "%> value="<%=endDeviceName1.item(ik).getFirstChild().getNodeValue()%>" ><% String senval= endDeviceName1.item(ik).getFirstChild().getNodeValue(); 
                                              if(firstDevice.equals(""))
                                                  firstDevice=senval;
                                              
                                                out.print(senval); %></option>
                                                     <%  }                                      
                                               
                                                %> 
    </select><div id="alarmcount" style="float:right" >Total Event Count</div>
</div>
               <div id="output" class="out">

                            </div>          
                    </div>
                </div>

               

            </div>
        </div>

                                                <%       if(!firstDevice.equals(""))
                                                {
                                                    %>
                                                    <script>
                                                        optionSelect('<%=firstDevice%>');
                                                    </script>
                                                    
                                                    <%
                                                }
                                                    
                                                    try {
                String[] values = request.getParameterValues("val");
                if (values != null) {
                    if (values[0].equals("0")) {%>
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.success("Alarm cleared..!");
                //alert("haiii");

            }

        </script>
        <%
        } else if (values[0].equals("1")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.alert("Can not clear alarm..!");

            }
        </script>
        <%
        } else if (values[0].equals("2")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.success("Password changed successfully..!");
            }
        </script>
        <%
        } else if (values[0].equals("3")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.alert("Old password is incorrect..!");
            }
        </script>
        <%
            }
        %>   
        <script>

        </script> 
        <%
                }
            } catch (Exception ex) {

            }
        %>



        <script src="js/bootstrap.min.js"></script>  
        <script src="js/dialog.js"></script> 
        <br>
        <!--<div class="col-lg-12" style="background: none repeat scroll 0% 0% #103D5F; color: #FFF; height:30px; text-align: right">Powered by Gadgeon Smart Systems Pvt. Ltd.</div>-->
    </body>

    <%
                }
            } else {
                response.sendRedirect("index.jsp");
            }
        } catch (Exception ex) {

        }

    %>
    <!-- END BODY -->
    <style>
      /*  .dygraph-axis-label-x
        {
            width:130px;
            height:70px;
            margin-left: -10px;
            -ms-transform:rotate(270deg); /* IE 9 */
     //       -moz-transform:rotate(270deg); /* Firefox */
      //      -webkit-transform:rotate(270deg); /* Safari and Chrome */
       //     -o-transform:rotate(270deg); /* Opera */
/*
        }
        .dygraph-axis-label {
            font-size: 13px;
        }
        .dygraph-xlabel{
            margin-top: 30px;
            font-size: 14px;
        }
        .dygraph-ylabel{

            font-size: 15px;
        }*/
    </style>
    
</html>