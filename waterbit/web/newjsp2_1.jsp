
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URL"%>
<%@page import="org.gadgeon.Config"%>
   
    <script type="text/javascript" src="js/dygraph-dev.js"></script>
 
 
    <div id="div_g" style="width:95%; height:234px; background-color: #FFF;"></div>
    <style>
        input{display:none !important;}
        input[ type="checkbox"]{display:inline !important;}
        input[ type="radio"]{display:inline !important;}
         input[ type="button"]{display:inline !important;}
    </style>
    <script type="text/javascript">
        var g;
      var data = [];
      var t = new Date();
      var sensor='<%=currentSensor%>';
      var sensor1;
//alert('currentSensor+currentSensorType');

<%
    String strTemp = "";
    if(!currentSensorType.equalsIgnoreCase("relay")){
    try{
    //  URL url = new URL("http://"+Config.url+":22001/vs/vsensor/"+currentSensor+"/historydemo");
        URL url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydata?sensortype="+currentSensor);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
         
        int b2 = 0;
        strTemp = br.readLine();
        
        if(strTemp.trim()=="[]")
        {
          
        }
         //   Double b1 = Double.parseDouble(strTemp);
         //  b2 = b1.intValue();
        
        //out.print(strTemp);
        //System.out.println("jjj"+strTemp);
    }    
    catch(Exception e){
strTemp="[]";
    
    }
    }
    else
    {
        strTemp="[]";
    }
    // String strTemp = "";
           
    %>

 var data11='<%=strTemp %>';
 
 livedata(1,data11);
       
    /*    var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       // var dat =dt.split(",");
       var dates=[];
       var vals=[];
       
        for(var i=dt.length-1;i>=0;i--)
        {
            
            //l(dt[i].length-dt[i].indexOf("=")+1);
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
            //alert(dates[i]);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
         data.push([new Date((dates[i].replace(" ","T"))), vals[i]]);
         //alert(new Date((dates[i].replace(" ","T"))));
        // var x = new Date(t.getTime() - i * 1000);
                //  data.push([x, Math.random()*40]);
        }

    */  
      //  alert(data.length);
   
     $.get("graphdetail.jsp?sensor="+sensor,function(data12,status){
     data12=data12.trim().split(":");    
     //alert("haii@@@i"+data12[0])
      xval=data12[0];
      yval=data12[1];
      min=data12[2];
      max=data12[3];
     if(data.length!=0)
       g = new Dygraph(document.getElementById("div_g"), data,
                          {
                            drawPoints: true,
                            showRoller: true,
                            ylabel: yval,
                            xlabel: xval,
                            valueRange: [min, max],
                             stackedGraph: true,
                            labels: ['Time', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_
                          });
        });
     
        
     /* setInterval(function() {
        var x = new Date();  // current time
        var y = Math.random()*40;
        data.push([x, y]);
	data.shift();
        g.updateOptions( { 'file': data } );
      }, 1000);*/
  
    liveDataInt=setInterval(function() {
    
        $.get("livedatademo.jsp?sensor="+sensor,function(data11,status){
            
      // alert(data11); 
    livedata(2,data11);
      
        });
    },3000
   );

function callfunc(yv,xv,ma,mi)
{ 
    //alert("#########"+yv)
    //sleep(1000);
    var yvalue=yv;
    var xvalue=xv;
    var max=ma;
    var min=mi;
     if(data.length!=0){
      g = new Dygraph(document.getElementById("div_g"), data,
                          {
                            drawPoints: true,
                            showRoller: true,
                            ylabel: yvalue,
                            xlabel: xvalue,
                            valueRange: [max,min],
                             stackedGraph: true,
                            labels: ['Time', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_
                          });
                      }
                    
}
function livedata(id,data11)
{
     $.ajaxSetup ({
                            // Disable caching of AJAX responses */
                            cache: false
                    });
    var curDate=new Date();
        data11=data11.trim();
        
       // alert(data11.length);
        //alert($("#div_g").html());
        if(data11.length!=2){
    //if(data11)
  //  alert(data11.length);
    //  alert("Data: " + data11 + "\nStatus: " + status);
      // ;
        alert(data11);
        var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       // var dat =dt.split(",");
       if($("#div_g").html()=="No data Available")data=[];
       if($("#div_g").html()!="No data Available"&&id==3)
           id=2;
       var dates=[];
       var vals=[];
       if(id==1)
           data=[];
       if(dt.length!=10)
           data=[];
       var checkMove=0;
       alert(id);
     //  alert(dt.length);
        for(var i=dt.length-1;i>=0;i--)
        {
           
           
       
            //alert(dt[i].length-dt[i].indexOf("=")+1);
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
          //  alert(dates[i]);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
            dts=dates[i].split("T");
           // alert("***"+dts[0]+"$$$"+dts[1]+"&&");
            dts1=dts[0].split("-");
            dts2=dts[1].split("+");
            dts3=dts2[0].split(":")
         //   dts4=dts2[2].split(".");
         //console.log("date  "+curDate+" : "+ new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0)+" * " +((curDate.getTime()-new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0).getTime())/(1000*60)));
         console.log(new Date()+"date "+i+" : +"+Math.ceil(((curDate.getTime()-new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0).getTime())/(1000*60))));
         if(Math.ceil(((curDate.getTime()-new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0).getTime())/(1000*60)))<=11)
         {
             data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0), vals[i]]);
             checkMove=1;
         }
         if(id===2&&checkMove===1&&dt.length===10){ data.shift();}
         checkMove=0;
         //alert(new Date((dates[i].replace(" ","T"))));
        // t=new Date();
        // t=new Date();
        // var x = new Date(t.getTime() - i * 1000);
          //        data.push([x, Math.random()*40]);
        }
       //alert(data.length);
        if(data.length>0&&data.length<10)
        {
            data.push([curDate,0]);
        }
        
        if(data.length==0)
        {
            data.push([new Date(curDate.getTime()-(1000*60*10)),0]);
            data.push([curDate,0]);
        }
        
           if(!$("#div_g").html()=="No data Available")
           {  //alert("h");
               g = new Dygraph(document.getElementById("div_g"), data,
                          {
                            drawPoints: true,
                            showRoller: true,
                            ylabel: 'Temperature (C)',
                            xlabel: 'Time',
                             stackedGraph: true,
                            valueRange: [-40, 80],
                            labels: ['Time', 'Random'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_
                          });
                          
           }
     if(id==2)    g.updateOptions( { 'file': data } );
 }
 else
 {
     clearInterval(liveDataInt);
     
     $("#div_g").html("No data Available");
     
 }
}
    </script>

   
<style>
.dygraph-axis-label-x
{
  width:130px;
  height:70px;
  margin-left: -10px;
  -ms-transform:rotate(270deg); /* IE 9 */
  -moz-transform:rotate(270deg); /* Firefox */
  -webkit-transform:rotate(270deg); /* Safari and Chrome */
  -o-transform:rotate(270deg); /* Opera */
 
}
.dygraph-axis-label {
  font-size: 13px;
}
.dygraph-xlabel{
    margin-top: 30px;
    font-size: 14px;
}
.dygraph-ylabel{
   
    font-size: 15px;
}
</style>

  