<%-- 
    Document   : addNewGraphProfile
    Created on : 12 Jan, 2015, 2:25:55 PM
    Author     : ajith
--%>

<%@page import="java.net.URLEncoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>  
<%@page import="org.gadgeon.Config"%>  
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>WaterBit</title>
	<link rel="stylesheet" href="css/main.css" >
	<link rel="stylesheet" href="css/bstrap.css" >
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="css/dialog.css" >
          <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script src="assets/nprogress/nprogress.js"></script>
	<link href="assets/nprogress/nprogress.css" rel="stylesheet" />
        <script src="js/alertify.min.js"></script>
	<script src="js/dialog.js"></script>
	  <style>
            .sensoredit{

            }
            .sensoredit:hover {
                cursor:pointer;
            }
      
      	  
             ul.nav li.dropdown:hover > ul.dropdown-menu {
		display: block;    
	    }
	    .caret-right {
		display: inline-block;
		width: 0;
		height: 0;
		margin-left: 5px;
		vertical-align: middle;
		border-left: 4px solid;
		border-bottom: 4px solid transparent;
		border-top: 4px solid transparent;
	    }
            #li1:hover
	    {
		background-color: #e7e7e7;
		height:58px;
		color: #000;
		border-radius: 5px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
	    }

        </style>
       
      <style>
    .myButton {
	-moz-box-shadow: 0px 10px 14px -7px #276873;
	-webkit-box-shadow: 0px 10px 14px -7px #276873;
	box-shadow: 0px 10px 14px -7px #276873;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
	background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
	background-color:#599bb3;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	border-radius:4px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Times New Roman;
	font-size:12px;
	font-weight:bold;
	padding:2px 10px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}
.myButton:active {
	position:relative;
	top:1px;
}

</style>
    
   
   
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

         
<% if (session.getAttribute("userid") != null) {

                int userid = (Integer) session.getAttribute("userid");
                if (userid > 1) {
        %>

        <script type="text/javascript">

            var checkflag = "false";
            function check(field) {
                if (checkflag == "false") {
                    for (i = 0; i < field.length; i++) {
                        field[i].checked = true;
                    }
                    checkflag = "true";
                    document.getElementById("btnEdit").style.visibility = "hidden";
                    return "Clear All";
                } else {
                    for (i = 0; i < field.length; i++) {
                        field[i].checked = false;
                    }
                    checkflag = "false";
                    document.getElementById("btnEdit").style.visibility = "visible";
                    return "Select All";
                }
            }
            function OnButton1()
            {
                var count = 0;
                var uid = 0;
                var senname = "";
                var uname;
                var row = 0;
                var cnt = document.getElementsByName("rbox[]");
                if (cnt.length != 0)
                {
                    var uname;

                    for (i = 0; i < document.getElementsByName("rbox[]").length; i++)
                    {
                        if (cnt[i].checked == true)
                        {
                            senname = cnt[i].value;
                            count++;
                            row = i;
                        }
                    }

                    if (count == 1)
                    {
                        document.getElementById("btnEdit").style.visibility = "visible";
                        var result = confirm("Do you want to edit Graph Profile?");
                        if (result == true) {
                            uname = document.getElementById("myTable").rows[row + 1].cells[1].innerHTML;
                            document.myform.action = "editGraphProfile.jsp?uname=" + senname;
                            // Open in a new window

                            document.myform.submit();             // Submit the page

                            return true;
                        }
                        else
                        {
                            window.location.href = addNewGraphProfile.jsp;
                            return false;
                        }
                    }
                    else if (count > 1)
                    {
                        document.getElementById("btnEdit").style.visibility = "hidden";
                       alertify.alert("please select only one Profile to edit");
                        return false;
                    }
                    else if (count < 1)
                    {
                        alertify.alert("please select one Profile to edit");
                        return false;
                    }
                    else
                    {
                        alertify.alert("Please select only one Profile to edit");
                        return false;
                    }
                }
                else
                {
                   alertify.alert("No Profile to edit");
                }
            }
	    
	    function deleteItem(op)
	    {
		document.myform.action = "deleteGraphProfileAction.jsp?uname=" + op;
		document.myform.submit();
		return true;
	    }
	    
            function OnButton2()
            {
                var count = 0;
                var row = 0;
                var senname = "";
                var cnt = document.getElementsByName("rbox[]");
                var uname;
                if (cnt.length != 0)
                {

                    for (i = 0; i < document.getElementsByName("rbox[]").length; i++)
                    {
                        if (cnt[i].checked == true)
                        {
                            senname = cnt[i].value;
                            count++;
                            row = i;
                        }
                    }
                    if (count == 1)

                    {
                        alertify.confirm("Do you want to delete Graph profile?", function (e) {
    if (e) {
       document.myform.action = "deleteGraphProfileAction.jsp?uname=" + senname;
		document.myform.submit();
                return true;
    } else {
        // user clicked "cancel"
        return false;
    }
});
			//Confirm.render('Do you want to delete Graph profile?',senname);
                        /*var result = confirm("Do you want to delete Graph profile?");
                        if (result == true) {

                            uname = document.getElementById("myTable").rows[row + 1].cells[1].innerHTML;
                            document.myform.action = "deleteGraphProfileAction.jsp?uname=" + senname;
                            document.myform.submit();
                            return true;
                        }
                        else
                        {
                            window.location.href = addNewSensorType.jsp;
                            return false;
                        }*/
                    }
                    else if (count > 1)
                    {
                        for (i = 0; i < count; i++)
                        {
                            uname = document.getElementById("myTable").rows[row + i].cells[1].innerHTML;

                            document.myform.action = "deleteGraphProfileAction.jsp?uname=" + senname;
                            document.myform.submit();

                        }
                        //             // Submit the page
                        return true;
                    }
                    else
                    {
                       alertify.alert("Please select atleast one Profile");
                        return false;
                    }
                }
                else
                {
                     alertify.alert("No Profile to delete");
                }

            }
            function enableButton(val)
            {
                //            if(val.checked==true)
                //            {
                //                document.getElementById("btnEdit").disabled = false;
                //                document.getElementById("btnEdit").style.visibility="visible";
                //            }

            }

            function validatetxtbox()
            {
                var profileName = document.getElementById("profileName").value;
                var xLabel = document.getElementById("xLabel").value;
                var yLabel = document.getElementById("yLabel").value;
                var min = document.getElementById("min").value;
                var max = document.getElementById("max").value;

                if (profileName == "")
                {
                    document.getElementById('une').innerHTML = "* Graph Profile name field should not be empty";
                    document.getElementById("profileName").focus();
                    return(false);
                }
		else if(profileName.match(/\s/g)){
                    document.getElementById('une').innerHTML = "* Graph profile name should not contain space";
                    document.getElementById("profileName").focus();
                    return(false);
                }
                else if (xLabel == "")
                {
                    document.getElementById('une').innerHTML = "* X Label field should not be empty";
                    document.getElementById("xLabel").focus();
                    return(false);
                }
                else if (yLabel == "")
                {
                    document.getElementById('une').innerHTML = "* Y Label field should not be empty";
                    document.getElementById("yLabel").focus();
                    return(false);
                }
                else if (min == "")
                {
                    document.getElementById('une').innerHTML = "* Min field should not be empty";
                    document.getElementById("min").focus();
                    return(false);
                }
                else if (max == "")
                {
                    document.getElementById('une').innerHTML = "* Max field should not be empty";
                    document.getElementById("max").focus();
                    return(false);
                }
                
                else if (max < min)
                {

                    document.getElementById('une').innerHTML = "* Maximum  value should be grater than minimum  value";
                    document.getElementById("min").focus();
                    return(false);
                }
		else
                {

                    return(true);
                }
            }
            function rowSelect(row, profile)
            {
                var tr = document.getElementsByClassName("sensoredit");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = "#dff0d8";

                }
                row.style.background = "#48D1CC";
                var str = row.cells[0].innerHTML.trim();
                row.cells[0].innerHTML = str.substr(0, str.length - 1) + " checked >";
                ///  alert(str.substr(0,str.length-1));

                if (window.XMLHttpRequest)
                {
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("result1").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("POST", "editGraphProfileAjax.jsp?profile=" + profile, true);
                xmlhttp.send();


            }


          function OnCancel()
            {

		 window.location.href = "addNewGraphProfile.jsp";

            }

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 45 || charCode > 45)) {
                    return false;
                }
                return true;
            }




        </script>
 
   

	
    </head>
    <body  style="height: 100%;">
        <script>
  NProgress.set(0.0);
               NProgress.start();
              var mnc=0;
        </script>
	<div id="dialogoverlay"></div>
		<div id="dialogbox">
		  <div>
		    <div id="dialogboxbody"></div>
		    <div id="dialogboxfoot"></div>
		  </div>  
		</div>
	    
	<div class="container">
	   <%
                String[] values = request.getParameterValues("val");
                if (values != null) {
                    if (values[0].equals("11")) {
            %>
            <script>
		document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
		    if(document.readyState=='loaded' || document.readyState=='complete')
              alertify.alert("Can not add Graph Profile.!");
		}
            </script>
            <%
            } else if (values[0].equals("12")) {%>  
            <script>
		document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
		    if(document.readyState=='loaded' || document.readyState=='complete')
                alertify.success("Graph Profile added succefully..!");
		}
            </script>
            <%
            } else if (values[0].equals("22")) { %>  
            <script>
		document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
		    if(document.readyState=='loaded' || document.readyState=='complete')
               alertify.success("Graph Profile Deleted succefully..!");
		}
            </script>
            <%
            } else if (values[0].equals("21")) {
            %>  
            <script>
		document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
		    if(document.readyState=='loaded' || document.readyState=='complete')
                 alertify.alert("Can Not Delete Graph Profile..!");
		}
            </script>
            <%
            } else if (values[0].equals("32")) { %>  
            <script>
		document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
		    if(document.readyState=='loaded' || document.readyState=='complete')
                alertify.success("Graph Profile Edited succefully..!");
		}
            </script>
            <%
            } else if (values[0].equals("31")) {
            %>  
            <script>
		document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
		    if(document.readyState=='loaded' || document.readyState=='complete')
                 alertify.alert("Can Not Edit Graph Profiles..!");
		}
            </script>
            <%
                }
		    else if (values[0].equals("33")) {
            %>  
            <script>
		document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
		    if(document.readyState=='loaded' || document.readyState=='complete')
               alertify.alert("Graph profile used in sensor..!");
	}
            </script>
            <%
		    }
                  else if (values[0].equals("34")) {
            %>  
            <script>
		document.onreadystatechange = function(){//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
		    if(document.readyState=='loaded' || document.readyState=='complete')
               alertify.alert("Graph profile name cannot be duplicated..!");
	}
            </script>
            <%
		    }
                }

            %>        
	      
	   
            
            
            <div class="navbar navbar-default" role="navigation" style="background: none repeat scroll 0% 0% #11020B; color: #FFF;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="color: #FFF"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="userDashBoard.jsp" id="li1"><img src="img/icon-dashboard.png" width="22" height="22"/>Dashboard</a></li>
                        <li class="dropdown active" >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="li1"><img src="img/icon-form-style.png" width="22" height="22"/>Configuration&nbsp;&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu" style="background: none repeat scroll 0% 0% #11020B;">
                                <li ><a href="addNewEndDevice.jsp">End Device</a> </li>
                                <li ><a href="addNewSensor.jsp"> Sensor</a> </li>
                                 <li><a href="addNewSensorType.jsp">Sensor Type</a></li>
                                 <li><a href="#">Graph Profile</a></li>
                            </ul>
                        </li>
                        <li>
			    <a href="panelView.jsp" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;" id="li1"><img src="img/icon-charts-graphs.png" width="22" height="22"/>Analytics&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                        <ul class="nav navbar-nav navbar-right">
                        <li><a href="logout.jsp" id="li1"><img src="img/img-profile.jpg" width="28" height="28"/>Logout</a></li>
                    </ul>
                </div>
            </div>
            
            
            
            
            
            
	      <%                                URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorType");
                URLConnection urlcon = jsonpage.openConnection();

                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

                DocumentBuilder db = dbf.newDocumentBuilder();

                Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorType");

                NodeList sensortype = doc.getElementsByTagName("sensortype");

            %>
	      <div class="row">
		  <div id="result1">
		 <div class="col-lg-6">
		    <div class="panel panel-primary" style="background-color: #dff0d8; height:445px;">
                            <div class="panel-heading">
                                Add New Graph Profile
                            </div>
                            <form action="addNewGraphProfileAction.jsp" method="post"  onsubmit="return(validatetxtbox())">
                            <div class="panel-body">
                               <div class="table-responsive"><table class="table">
                                                    <tbody>

                                                        <tr class="success">
                                                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Graph Profile Name</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="profileName" id="profileName"/></td>
       
                                                        </tr>

                                                        <tr class="success">
                                                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Sensor Type </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="prop" id="prop" >
                                                                    <%                                                 for (int i = 0; i <= sensortype.getLength() - 1; i++) {

                                                                    %>
                                                                    <option value="<%=sensortype.item(i).getFirstChild().getNodeValue()%>"
                                                                            selected> <%=sensortype.item(i).getFirstChild().getNodeValue()%>
                                                                    </option>

                                                                    <%
                                                                        }
                                                                    %>
                                                                </select></td>
                                                        </tr>

                                                        <tr class="success">
                                                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">X-Label</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="xLabel" id="xLabel"/>
                                                        </tr>

                                                        <tr class="success">
                                                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Y-Label</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="yLabel" id="yLabel"/></td></td>
                                                        </tr>

                                                        <tr class="success">
                                                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Value Range - Min (Y-axis)</td> <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type= "number" name="min" id="min" width="1" onkeypress="return isNumber(event)"/></td>
                                                        </tr>
							<tr class="success">
                                                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Value Range - Max (Y-axis)</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type= "number" name="max" id="max" onkeypress="return isNumber(event)"/></td>
                                                        </tr>

                                                        <tr class="success">
                                                            <td style="border-top: #599bb3; border-style: dashed; border-width: 1px; border: none"></td>
                                                            <td style="border-top: #599bb3; border-style: dashed; border-width: 1px; border: none"><input type="submit" value="Submit" class="myButton"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="Clear" class="myButton"/></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                    <font color='red'> <div id="une"> </div> </font>
                                </div>
                                
                            </div>
                                </form>
                        </div> 
		 </div>
		  </div>
		   <%                                 jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getProfileList");
                            urlcon = jsonpage.openConnection();

                            dbf = DocumentBuilderFactory.newInstance();

                            db = dbf.newDocumentBuilder();

                            doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getProfileList");
                            NodeList graphprofile = doc.getElementsByTagName("graphprofile");
			    NodeList graphsensortype = doc.getElementsByTagName("sensortype");
                        %>

		 <div class="col-lg-6">
		     <form name="myform" method="post">
		     <div class="panel panel-primary" style="height: 403px;background-color: #dff0d8; height:445px;">
			 <div class="panel-heading">
			     Graph Profiles
			     <input type="button" id="btnDelete" class="myButton" value="Delete Profile" name="btnDelete"onclick="OnButton2()" style="float:right;">
			 </div>
			 <div class="panel-body">
			     
				 <div class="table-responsive" style="height: 400px;">
                                    <table class="table" id="myTable">
                                                <tbody>
						    <tr style=" 
						border: 1px solid #337AB7;
						color:white;
						background-color: #337AB7;
						-moz-border-radius: 5px;
						-webkit-border-radius: 5px; 
						background-image: -webkit-linear-gradient(top,#337ab7 0,#2e6da4 100%);">
							<th style="text-align: center;">Profile Name</th>
							<th style="text-align: center;">Sensor Type</th>
                                                    </tr>
						    <%                                                 for (int i = 0; i <= graphprofile.getLength() - 1; i++) {

                                                    %>

                                                    <tr style="background-color: #dff0d8;" class="sensoredit" id="tr"  onclick="rowSelect(this, '<%=graphprofile.item(i).getFirstChild().getNodeValue()%>')" >


                                                        <td hidden="true"><input type="radio" name="rbox[]" id="rbox[]"  value="<%=graphprofile.item(i).getFirstChild().getNodeValue()%>" onclick="rowSelect(this, '<%=graphprofile.item(i).getFirstChild().getNodeValue()%>')" style="visibility:hidden;" ></td>
                                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%
                                                            out.print(graphprofile.item(i).getFirstChild().getNodeValue());
                                                            %></td>
                                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%
                                                            out.print(graphsensortype.item(i).getFirstChild().getNodeValue());
                                                            %></td>


                                                    </tr>

                                                    <%
                                                        }
                                                    %>

                                                   <!-- <tr class="success">
                                                        <td>
                                                            <input type="button" id="btnDelete" class="myButton" value="Delete Profile" name="btnDelete"onclick="OnButton2()"></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                    </tr>-->
                                                </tbody>
                                            </table>

			     
			 </div>
		     </div>
		 </div>
						    </form>
	     </div>
	</div>
        </div>
	     
             <!--<div class="col-lg-12" style="background: none repeat scroll 0% 0% #103D5F; color: #FFF; height:30px; text-align: right">Powered by Gadgeon Smart Systems Pvt. Ltd.</div>-->
    <script src="js/bootstrap.min.js"></script> 
     <script>
            if(mnc==0)
    {
	NProgress.set(1);
    }
	mnc=1;
            
        </script>    
    </body>
    <% 
                                                }
      }
	else
	{
	    response.sendRedirect("index.jsp");
		    }
		%>
</html>

