<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>

        <style>
            //html { height: auto; }

         //  table { border-collapse: collapse; border-spacing: 0; }
           //p { margin: 0.75em 0; }
            #map_canvas {  position: absolute; bottom: 0; left: 3.5%; right: 0; top: 15%; width:95%; height: 80%;  }
            
        </style>
        <script src="http://maps.google.com/maps/api/js?v=3.9&amp;sensor=false"></script>
        <script src="js/oms.min.js"></script>
        <script>
            var mark = [];
            var check=1;
	    var markerExpand="";
	    function mapPloat()
	    {
		var gm = google.maps;
                var map = new gm.Map(document.getElementById('map_canvas'), {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: new gm.LatLng(50, 0), zoom: 2, // whatevs: fitBounds will override
                    scrollwheel: false, 
                    zoomControl: false

                });
                
                var iw = new gm.InfoWindow();
                var oms = new OverlappingMarkerSpiderfier(map,
                        {markersWontMove: true, markersWontHide: true, keepSpiderfied: true});
                var usualColor = 'eebb22';
                var spiderfiedColor = 'ffee22';
                var iconWithColor = function (color) {
                    return 'http://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin|+|' +
                            color + '|000000|ffff00';
                }
                var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

                var icons = [
                    iconURLPrefix + 'red-dot.png',
                    iconURLPrefix + 'green-dot.png',
                    iconURLPrefix + 'blue-dot.png',
                    iconURLPrefix + 'orange-dot.png',
                    iconURLPrefix + 'purple-dot.png',
                    iconURLPrefix + 'pink-dot.png',
                    iconURLPrefix + 'yellow-dot.png'
                ]
                var iconsLength = icons.length;
                var shadow = new gm.MarkerImage(
                        'https://www.google.com/intl/en_ALL/mapfiles/shadow50.png',
                        new gm.Size(37, 34), // size - for sprite clipping
                        new gm.Point(0, 0), // origin - ditto
                        new gm.Point(10, 34) // anchor - where to meet map location
                        );
                
                $(window).resize(function() {
    // (the 'map' here is the result of the created 'var map = ...' above)
    google.maps.event.trigger(map, "resize");
  });
                oms.addListener('click', function (marker) {
		    
                    iw.setContent(marker.desc);
		    //alert('jj');
                    //if(check==1)
                   // map.setZoom(14);
                    rowSelectGraph(marker.desc.trim(), marker.desc);
                    rowSelecteventgraph(marker.desc.trim(), marker.desc);
                    iw.open(map, marker);
		    //alert(resizedMap);
		    
		    markerExpand=marker;
		   // alert(markerExpand.desc);
		    //alert(resizedMap);
		   if(resizedMap==1)
		   {
		       resizedMap=0;
		        $("#draggable2" ).show();
			$("#tables" ).show();
			var wid = $("#draggable2" ).width();
		       // alert(wid);
			
			var mapWid = wid - 30;
                        $("#map_canvas").css('cssText','top : 15% !important');
                        $("#draggable").css('cssText','height: '+$("#draggable1").css('height')+' !important;');
			$("#draggable").css('width', wid + 'px');
			//$("#draggable").css('height', '300px !important');
			$("#map_canvas").css('width', mapWid + 'px');
                        
                        $("#map_canvas").css('height',  Number($("#draggable1").css('height').replace("px","")*0.8)+'px');
			$("#draggable1").show();
                         g = new Dygraph(document.getElementById("div_g"),livedata_data,livedata_options);
			
			mapPloat();
		   }
                });



                oms.addListener('spiderfy', function (markers) {
                   // alert("sp " + markers.length);
                    for (var i = 0; i < markers.length; i++) {
                        iconCounter = i;
                        markers[i].setIcon(icons[iconCounter]);
                        markers[i].setShadow(null);
                        iconCounter++;
                        // We only have a limited number of possible icon colors, so we may have to restart the counter
                        if (iconCounter >= iconsLength) {
                            iconCounter = 0;
                        }
                    }
                    iw.close();
                });

                oms.addListener('unspiderfy', function (markers) {
                    for (var i = 0; i < markers.length; i++) {
                        iconCounter = i;
                        markers[i].setIcon(icons[6]);
                        markers[i].setShadow(shadow);
                        iconCounter++;
                        // We only have a limited number of possible icon colors, so we may have to restart the counter
                        if (iconCounter >= iconsLength) {
                            iconCounter = 0;
                        }
                    }
                });
                var bounds = new gm.LatLngBounds();
                var iconCounter = 0;
                for (var i = 0; i < window.mapData.length; i++) {
                    iconCounter = i;
                    var datum = window.mapData[i];
                    var loc = new gm.LatLng(datum.lat, datum.lon);
                    bounds.extend(loc);
                    var marker = new gm.Marker({
                        position: loc,
                        title: datum.h,
                        map: map,
        //icon: iconWithColor(usualColor),
                        icon: icons[iconCounter],
                        shadow: shadow
                    });
                    marker.desc = datum.d;
                   // mark[i] = marker;
                    oms.addMarker(marker);


                }
                map.fitBounds(bounds);
        // for debugging/exploratory use in console
                window.map = map;
                window.oms = oms;
            
 /*var x = document.getElementById("mytab2").rows[1].cells[0].innerHTML;
                var x1=document.getElementById("mytab2").rows[1].cells[1].innerHTML;
                alert(x+"_"+x1);
               var sens=x+"_"+x1;
           */     
          //var markers=oms.getMarkers();
          //alert(markers[0].desc);
             //  for(var i=0;i<markers.length;i++)
               {
                  // alert(markers[i]);
                  // if(markers[i].desc==sens)
                   {
               //google.maps.event.trigger(markers[0], 'click');
               //google.maps.event.trigger(markers[0], 'click');
                   }
           } 
                google.maps.event.addListenerOnce(map, 'idle', function(){
                     //loaded fully
                     check=0;
		     
                     var markers=window.oms.getMarkers();
                     map.setZoom(2);
                    // map.setZoom(14);
		    //alert(markerExpand.desc);
		  //  alert(resizedMap);
		    if(typeof resizedMap ==='undefined'){
			resizedMap=212; 
              google.maps.event.trigger(markers[0], 'click');
              google.maps.event.trigger(markers[0], 'click');
	      markerExpand=markers[0];
		    }
		    else{
		//alert(markerExpand.desc);
              showMarker(markerExpand.desc);
		    }
			
              check=1;
	      
	      if($("#draggable" ).css('height')=='540px')
			 resizedMap = 1;
                 });
		 
	    }
            window.onload = function () {
                mapPloat();
            }
            

            function showMarker(sensor)
            {
                check=1;
                
              //  alert(sensor);
               // alert(oms.getMarkers());
                var markers=oms.getMarkers();
               for(var i=0;i<markers.length;i++)
               {
                  // alert(markers[i]); markerExpand
                   if(markers[i].desc==sensor)
                   {
               google.maps.event.trigger(markers[i], 'click');
               google.maps.event.trigger(markers[i], 'click');
	       markerExpand=markers[i];
	       
                   }
               }
                //google.maps.event.trigger(markers[0], 'spiderfy');
      
               
            }
            
        </script>
       
    </head>
   <div id="map_canvas"></div>
    <script>
    // randomize some overlapping map data -- more normally we'd load some JSON data instead
        var locations = [];
        // Define your locations: HTML content for the info window, latitude, longitude
        <%
                    try {

                        URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorList");
                        URLConnection urlcon = jsonpage.openConnection();

                        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

                        DocumentBuilder db = dbf.newDocumentBuilder();

                        Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/"+session.getAttribute( "userid" )+"/getSensorListByUserId");
                        NodeList sensorname = doc.getElementsByTagName("sensorname");

                        NodeList latitude = doc.getElementsByTagName("latitude");
                        NodeList longitude = doc.getElementsByTagName("longitude");

                        for (int i = 0; i <= sensorname.getLength() - 1; i++) {
        %>
        locations[<%=i%>] = ['<%=sensorname.item(i).getFirstChild().getNodeValue()%>', '<%=latitude.item(i).getFirstChild().getNodeValue()%>', '<%=longitude.item(i).getFirstChild().getNodeValue()%>'];

        <%

                                      }
                                  } catch (Exception e) {
                                  System.out.println(e + "hhh");
                                  }%>

        var data = [];
        var loc = [];
        locations.sort();
        var current = null;
        var cnt = -1;
      /*  for (var i = 0; i < locations.length; i++) {
            //alert(current);
            if (locations[i][1] + " " + locations[i][2] != current) {
                if (cnt > 0) {

                }
                current = locations[i][1] + " " + locations[i][2];
                cnt++;

                loc[cnt] = 1;//console.log("ee "+loc[cnt]);
            } else {
                loc[cnt]++;
                // alert(loc[cnt]+" "+cnt);
                //console.log("ee 1"+loc[cnt]);
            }
        }*/
       // alert(loc[0]);
        //cnt = 0;
        //lmt = loc[0];
        for (var i = 0; i < locations.length; i++) {

            data.push({lon: locations[i][2], lat: locations[i][1], d: locations[i][0],h:locations[i][0]});
          /*  if (i > lmt)
            {
                cnt++;
                lmt += loc[cnt];
            }*/
        }

        window.mapData = data;
          
        
    </script>
 