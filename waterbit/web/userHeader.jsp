<%-- 
    Document   : userHeader
    Created on : 14 Jan, 2015, 4:49:11 PM
    Author     : nisha
--%>

<link rel="stylesheet" href="css/main.css" >
<link rel="stylesheet" href="css/bstrap.css" >
<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('ul.nav > li').click(function (e) {
            $('ul.nav > li').removeClass('active');
            $(this).addClass('active');
        });
    });
</script>


<style>
    ul.nav li.dropdown:hover > ul.dropdown-menu {
        display: block; 
	
    }
    .caret-right {
        display: inline-block;
        width: 0;
        height: 0;
        margin-left: 5px;
        vertical-align: middle;
        border-left: 4px solid;
        border-bottom: 4px solid transparent;
        border-top: 4px solid transparent;
    }
    #li1:hover
	    {
		background-color: #e7e7e7;
		height:58px;
		color: #000;
		border-radius: 5px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
	    }

</style>
<div class="navbar navbar-default" role="navigation" style="background: none repeat scroll 0% 0% #11020B; color: #FFF;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#" style="color: #FFF"></a>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li><a href="userDashBoard.jsp" id="li1"><img src="img/icon-dashboard.png" width="22" height="22"/>Dashboard</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="li1"><img src="img/icon-form-style.png" width="22" height="22"/>Configuration&nbsp;&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu" style="background: none repeat scroll 0% 0% #11020B;">
                    <li ><a href="addNewEndDevice.jsp">End Device</a> </li>
                    <li ><a href="addNewSensor.jsp"> Sensor</a> </li>
                    <li><a href="addNewSensorType.jsp">Sensor Type</a></li>
                    <li><a href="addNewGraphProfile.jsp">Graph Profile</a></li>
                        <%-- <li><a href="userChangePasswd.jsp">Change Password</a> </li>
                        <li><a href="liveControls.jsp">Live Controls</a> </li>
                        <li><a href="ipcameraview.jsp">IP Camera</a></li>--%>
                </ul>
            </li>




            <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="li1"><img src="img/icon-charts-graphs.png" width="22" height="22"/>Analytics&nbsp;&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu" style="background: none repeat scroll 0% 0% #11020B;">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">History&nbsp;&nbsp;<b class="caret-right"></b></a>
                        <ul class="dropdown-menu submnu" style="background: none repeat scroll 0% 0% #11020B;margin-top: -5px;">


                            <li ><a href="historyChart.jsp">Graph View</a> </li>
                           <!-- <li><a href="historyChartList.jsp">List View</a></li>-->
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Live Data&nbsp;&nbsp;<b class="caret-right"></b></a>
                        <ul class="dropdown-menu submnu" style="background: none repeat scroll 0% 0% #11020B;margin-top: -5px;">


                            <li ><a href="charttest_2.jsp">Panel View </a> </li>
                            <li><a href="charttest_1.jsp">Tab View</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        </li>










        <!--<li><a href="historyChart.jsp"><img src="img/icon-charts-graphs.png" width="22" height="22"/>History</a></li>-->
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="logout.jsp"><img src="img/img-profile.jpg" width="28" height="28"/>Logout</a></li>
        </ul>
    </div>
</div>

