<%-- 
    Document   : addNewSensor
    Created on : 28 Nov, 2014, 10:25:55 AM
    Author     : nisha
--%>
<%@page import="java.net.URLEncoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>  
<%@page import="org.gadgeon.Config"%>  
<!DOCTYPE html>
<% 
    try{ %>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>WaterBit</title>
        <script src="js/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" href="css/main.css" >
        <link rel="stylesheet" href="css/bstrap.css" >
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" >
        <link rel="stylesheet" href="css/dialog.css">
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        
        <script src="assets/nprogress/nprogress.js"></script>
	<link href="assets/nprogress/nprogress.css" rel="stylesheet" />
       
        <script src="js/alertify.min.js"></script>
        
        <style>
            .sensoredit{

            }
            .sensoredit:hover {
                cursor:pointer;
            }

            
             ul.nav li.dropdown:hover > ul.dropdown-menu {
		display: block;    
	    }
	    .caret-right {
		display: inline-block;
		width: 0;
		height: 0;
		margin-left: 5px;
		vertical-align: middle;
		border-left: 4px solid;
		border-bottom: 4px solid transparent;
		border-top: 4px solid transparent;
	    }
	    #li1:hover
	    {
		background-color: #e7e7e7;
		height:58px;
		color: #000;
		border-radius: 5px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
	    }
        </style>

        <style>
            .myButton {
                -moz-box-shadow: 0px 10px 14px -7px #276873;
                -webkit-box-shadow: 0px 10px 14px -7px #276873;
                box-shadow: 0px 10px 14px -7px #276873;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
                background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
                background-color:#599bb3;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-family:Times New Roman;
                font-size:12px;
                font-weight:bold;
                padding:2px 10px;
                text-decoration:none;
                text-shadow:0px 1px 0px #3d768a;
            }
            .myButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
                background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
                background-color:#408c99;
            }
            .myButton:active {
                position:relative;
                top:1px;
            }

        </style>



        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <% if (session.getAttribute("userid") != null) {

                int userid = (Integer) session.getAttribute("userid");
                if (userid > 1) {
        %>

        <script type="text/javascript">


            function GetLocation() {
                var geocoder = new google.maps.Geocoder();
                var address = document.getElementById("txtAddress").value;
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        document.getElementById("sLatitude").value = latitude;
                        document.getElementById("sLongitude").value = longitude;
                        //alert("Latitude: " + latitude + "\nLongitude: " + longitude);


                        var geocoder;
                        geocoder = new google.maps.Geocoder();
                        var latlng = new google.maps.LatLng(latitude, longitude);
                        //alert("Else loop" + latlng);
                        geocoder.geocode({'latLng': latlng}, function (results, status)
                        {
                            //alert("Else loop1");
                            if (status == google.maps.GeocoderStatus.OK)
                            {
                                if (results[0])
                                {
                                    var add = results[0].formatted_address;
                                    var value = add.split(",");

                                    count = value.length;
                                    country = value[count - 1];
                                    state = value[count - 2];
                                    city = value[count - 3];
                                    //alert("city name is: " + city);
                                    document.getElementById("city").value = city;
                                    document.getElementById("city1").value = city;

                                }
                                else
                                {
                                    alertify.alert("address not found");
                                }
                            }
                            else
                            {
                                //document.getElementById("location").innerHTML="Geocoder failed due to: " + status;
                                //alert("Geocoder failed due to: " + status);
                            }
                        });




                    } else {
                        document.getElementById("city").value = "";
                        alertify.alert("Pincode Entered is not correct.");
                    }

                });
            }

            var checkflag = "false";
            function check(field) {
                if (checkflag == "false") {
                    for (i = 0; i < field.length; i++) {
                        field[i].checked = true;
                    }
                    checkflag = "true";
                    document.getElementById("btnEdit").style.visibility = "hidden";
                    return "Clear All";
                } else {
                    for (i = 0; i < field.length; i++) {
                        field[i].checked = false;
                    }
                    checkflag = "false";
                    document.getElementById("btnEdit").style.visibility = "visible";
                    return "Select All";
                }
            }
            function OnButton1()
            {
                var count = 0;
                var uid = 0;
                var senname = "";
                var uname;
                var row = 0;
                var cnt = document.getElementsByName("rbox[]");
                if (cnt.length != 0)
                {
                    var uname;

                    for (i = 0; i < document.getElementsByName("rbox[]").length; i++)
                    {
                        if (cnt[i].checked == true)
                        {
                            // alert(document.getElementsByName("rbox[]").value);
                            senname = cnt[i].value;
                            count++;
                            row = i;
                        }
                    }

                    if (count == 1)
                    {
                        document.getElementById("btnEdit").style.visibility = "visible";

                        alertify.confirm("Do you want to edit sensor?", function (e) {
                            if (e) {
                                // user clicked "ok"
                                uname = document.getElementById("myTable").rows[row + 1].cells[1].innerHTML;
                                document.myform.action = "editSensor.jsp?uname=" + senname;
                                // document.Form1.target = "_blank";    // Open in a new window

                                document.myform.submit();             // Submit the page

                                return true;
                            } else {
                                // user clicked "cancel"
                                window.location.href = addNewSensor.jsp;
                                return false;
                            }
                        });
                     
                    }
                    else if (count > 1)
                    {
                        document.getElementById("btnEdit").style.visibility = "hidden";
                        Alert.render("please select only one sensor to edit");
                        return false;
                    }
                    else if (count < 1)
                    {
                        Alert.render("please select one sensor to edit");
                        return false;
                    }
                    else
                    {
                        Alert.render("Please select only one sensor to edit");
                        return false;
                    }
                }
                else
                {
                    Alert.render("No sensor to edit");
                }

            }
            function deleteItem(op)
            {
                document.myform.action = "deleteSensorAction.jsp?uname=" + op;
                document.myform.submit();
                return true;
            }



            function OnButton2()
            {

                //alert(res);
                var count = 0;
                var row = 0;
                var senname = "";
                var cnt = document.getElementsByName("rbox[]");
                // alert(document.getElementById("status_sensor").value);

                //var status_sensor= document.getElementById("status_sensor").value;
                //  alert("status_sensor""");
                var status1;

                var uname;
                if (cnt.length != 0)
                {

                    for (i = 0; i < document.getElementsByName("rbox[]").length; i++)
                    {
                        if (cnt[i].checked == true)
                        {
                            status1 = document.getElementsByName("rbox2[]")[i].value;
                            senname = cnt[i].value;
                            count++;
                            row = i;
                        }
                    }

                    if (count == 1)

                    {

                        if (status1 != 1)
                        {
                              alertify.confirm("Do you want to delete sensor?", function (e) {
                            if (e) {
                                // user clicked "ok"
                               document.myform.action = "deleteSensorAction.jsp?uname=" + senname;
                document.myform.submit();
                return true;
                            } else {
                                // user clicked "cancel"
                                window.location.href = addNewSensor.jsp;
                                return false;
                            }
                        });
                           // Confirm.render('Do you want to delete sensor?', senname);


                            //var result = confirm("Do you want to delete sensor?");
                            //var result = Alert2.render("Do you want to delete sensor?");
                            //alert(result);
                            /* if (result == true) {
                          
                             //document.myform.action = "deleteSensorAction.jsp?uname=" + senname;
                             document.myform.submit();
                             return true;
                             }
                             else
                             {
                          
                          
                             window.location.href = addNewSensor.jsp;
                             return false;
                          
                          
                             }*/
                        }
                        else
                        {
                            alertify.alert("Cannot Delete Active Sensor");
                            window.location.href = addNewSensor.jsp;
                            return false;
                        }
                    }
                    else if (count > 1)
                    {
                        for (i = 0; i < count; i++)
                        {
                            uname = document.getElementById("myTable").rows[row + i].cells[1].innerHTML;

                            document.myform.action = "deleteSensorAction.jsp?uname=" + senname;
                            document.myform.submit();

                        }
                        //             // Submit the page
                        return true;
                    }
                    else
                    {
                       alertify.alert("Please select atleast one Sensor");
                        return false;
                    }
                }
                else
                {
                    alertify.alert("No sensor to delete");
                }
                // status_sensor="";
            }
            function validatetxtbox()
            {
                var endDevName = document.getElementById("endDevName").value;
                var prop = document.getElementById("prop").value;
          if(document.getElementById("graphprofile").length!=0)
          {
                var graphprofile = document.getElementById("graphprofile").value;
            }else{
                var graphprofile="";
            }
                var sinfor = document.getElementById("sinfor").value;
                var city = document.getElementById("city").value;
                var maxTemp = document.getElementById("maxTemp").value;
                var minTemp = document.getElementById("minTemp").value;

                if (endDevName == "")
                {
                    document.getElementById('une').innerHTML = "* Please select end device";
                    document.getElementById("endDevName").focus();
                    return(false);
                }
               

               else if(graphprofile == "")
               {
                    document.getElementById('une').innerHTML = "* Please select graph profile";
                    document.getElementById("graphprofile").focus();
                    return(false);
               }
                else if (sinfor == "")
                {
                    document.getElementById('une').innerHTML = "* Please type sensor information";
                    document.getElementById("sinfor").focus();
                    return(false);
                }

                else if(prop.toString().toLocaleLowerCase()!="ap"&&minTemp == "")
                {
                    document.getElementById('une').innerHTML = "* Please enter minimum threshold value";
                    document.getElementById("city").focus();
                    return(false);
                }
                else if(prop.toString().toLocaleLowerCase()!="ap"&&maxTemp == "")
                {
                    document.getElementById('une').innerHTML = "* Please enter maximum threshold value";
                    document.getElementById("city").focus();
                    return(false);
                }
                else if (maxTemp < minTemp)
                {

                    document.getElementById('une').innerHTML = "* Maximum Threshold value should be grater than minimum threshold value";
                    document.getElementById("minTemp").focus();
                    return(false);
                }
               
                else
                {

                    return(true);
                }


            }
            function rowSelect(row, sensor)
            {
                //alert("hi");
                //row.className="";
                var tr = document.getElementsByClassName("sensoredit");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = "#dff0d8";

                }
                row.style.background = "#48D1CC";
                var str = row.cells[0].innerHTML.trim();
                row.cells[0].innerHTML = str.substr(0, str.length - 1) + " checked >";
                ///  alert(str.substr(0,str.length-1));

                if (window.XMLHttpRequest)
                {
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("result1").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("POST", "editSensorAjax.jsp?sensor=" + sensor, true);
                xmlhttp.send();

            }

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 45 || charCode > 45)) {
                    return false;
                }
                return true;
            }

   //Changing profile values according to sensor type
            function graphProfileByType()
            {
                if (document.getElementById("prop").value == "relay")
                {
                    document.getElementById("maxTemp").value = 0;
                    document.getElementById("maxTemp").disabled = true;
                    document.getElementById("minTemp").value = 0;
                    document.getElementById("minTemp").disabled = true;
                }
                else {

                    document.getElementById("maxTemp").disabled = false;
                    document.getElementById("minTemp").disabled = false;

                    uname = document.getElementById("prop").value;
                    if (window.XMLHttpRequest)
                    {
                        xmlhttp = new XMLHttpRequest();
                    }
                    else
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function ()
                    {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                        {
                            document.getElementById("result").innerHTML = xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("POST", "graphProfileAjax.jsp?sensortype=" + uname, true);
                    xmlhttp.send();
                }
            }

            function graphProfileByName()
            {

                uname = document.getElementById("graphprofile").value;
                sensortype = document.getElementById("prop").value;
                if (window.XMLHttpRequest)
                {
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("result").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("POST", "graphProfileByNameAjax.jsp?graphprofile=" + uname + "&sensortype=" + sensortype, true);
                xmlhttp.send();
            }
  function OnCancel()
            {
                
             window.location.href = "addNewSensor.jsp";

            }

function EndDeviceDetail(txt)
            {

             $.get("getEndPoint.jsp?sensor=" + txt, function (data11, status) {
                 var data=data11.trim().split(":");
                 
              //  alert(data);
                    $("#city").val(data[0].trim());
                     $("#city1").val(data[0].trim());
                    $("#sLatitude").val(data[1].trim());
                    $("#sLongitude").val(data[2].trim());
                  //  alert(data[1].trim());
                       
                });
            }


function listProfiles(cmb)
    {
	disableField();
        $.get("graphProfileList.jsp?sensortype="+cmb.value, function (data11, status) {
                         // alert(data11+status);
                         // alertify.success("Alarm cleared");
                        var select = document.getElementById("graphprofile");
                        var length = select.options.length;
                        //alert(select.options.length);
                        /*for (var i = 0; i <= length; i++) {
                          select.options[i] = null;
                        }*/
                        $("#graphprofile").empty();
                       // alert(select.options.length);
                        
                         var data=data11.trim().split("**");
                         for(var i = 0; i < data.length-1; ++i) {
                            var option1 = document.createElement('option');
                            option1.text = data[i];
                            option1.value = data[i];
                            select.add(option1, i);
                        }
                       // alert("2"+" "+data.length);
        });
    }

        function function1()
        {
                var sinfor = document.getElementById("sinfor").value;
                var maxTemp = document.getElementById("maxTemp").value;
                var minTemp = document.getElementById("minTemp").value;

                if (sinfor == "")
                {
                    document.getElementById('une').innerHTML = "* Please type sensor information";
                    document.getElementById("sinfor").focus();
                    return(false);
                }

                else if(minTemp == "")
                {
                    document.getElementById('une').innerHTML = "* Please enter minimum threshold value";
                    document.getElementById("city").focus();
                    return(false);
                }
                else if(maxTemp == "")
                {
                    document.getElementById('une').innerHTML = "* Please enter maximum threshold value";
                    document.getElementById("city").focus();
                    return(false);
                }
                else if (maxTemp < minTemp)
                {

                    document.getElementById('une').innerHTML = "* Maximum Threshold value should be grater than minimum threshold value";
                    document.getElementById("minTemp").focus();
                    return(false);
                }
               
                else
                {

                    return(true);
                }
        }
 function disableField()
            {
                 if (document.getElementById("prop").value == "ap")
                {
                    document.getElementById("maxTemp").value = 0;
                    document.getElementById("maxTemp").disabled = true;
                    document.getElementById("minTemp").value = 0;
                    document.getElementById("minTemp").disabled = true;
                }
                else{
                       document.getElementById("maxTemp").disabled = false;
                        document.getElementById("minTemp").disabled = false;
                }
            }
	    
	    
	    

        </script>




    </head>
    <body>
        <script>
  NProgress.set(0.0);
               NProgress.start();
              var mnc=0;
        </script>
        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>  
        </div>

        <div class="container">
            <%
                String[] values = request.getParameterValues("val");
                if (values != null) {
                    if (values[0].equals("11")) {
            %>
            <script type="text/javascript">
                document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                    if (document.readyState == 'loaded' || document.readyState == 'complete')
                      alertify.alert("Sensor name cannot be duplicated..!");
                }
            </script>
            <%
      } else if (values[0].equals("12")) {%>  
            <script type="text/javascript">
                document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                    if (document.readyState == 'loaded' || document.readyState == 'complete')
                      alertify.success("Sensor added succefully..!");
                }
            </script>
            <%
                  } else if (values[0].equals("22")) { %>  
            <script type="text/javascript">
                document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                    if (document.readyState == 'loaded' || document.readyState == 'complete')
                      alertify.success("Sensor Deleted succefully..!");
                }
            </script>
            <%
            } else if (values[0].equals("21")) {
            %>  
            <script type="text/javascript">
                document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                    if (document.readyState == 'loaded' || document.readyState == 'complete')
                        alertify.alert("Can Not Delete Sensor..!");
                }
            </script>
            <%
                  } else if (values[0].equals("32")) { %>  
            <script type="text/javascript">
                document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                    if (document.readyState == 'loaded' || document.readyState == 'complete')
                        alertify.success("Sensor Edited succefully..!");
                }
            </script>
            <%
            } else if (values[0].equals("31")) {
            %>  
            <script type="text/javascript">
                document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                    if (document.readyState == 'loaded' || document.readyState == 'complete')
                       alertify.alert("Can Not Edit Sensor..!");
                }
            </script>
            <%
                }
            %>   
            <script>

            </script> 
            <%
                }

            %>     
            
            
            
            
            <div class="navbar navbar-default" role="navigation" style="background: none repeat scroll 0% 0% #11020B; color: #FFF;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="color: #FFF"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="userDashBoard.jsp" id="li1"><img src="img/icon-dashboard.png" width="22" height="22"/>Dashboard</a></li>
                        <li class="dropdown active" >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="li1"><img src="img/icon-form-style.png" width="22" height="22"/>Configuration&nbsp;&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu" style="background: none repeat scroll 0% 0% #11020B;">
                                <li ><a href="addNewEndDevice.jsp">End Device</a> </li>
                                <li ><a href="#"> Sensor</a> </li>
                                 <li><a href="addNewSensorType.jsp">Sensor Type</a></li>
                                 <li><a href="addNewGraphProfile.jsp">Graph Profile</a></li>
                            </ul>
                        </li>
                      <li>
			    <a href="panelView.jsp" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;" id="li1"><img src="img/icon-charts-graphs.png" width="22" height="22"/>Analytics&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                        <ul class="nav navbar-nav navbar-right">
                        <li><a href="logout.jsp" id="li1"><img src="img/img-profile.jpg" width="28" height="28"/>Logout</a></li>
                    </ul>
                </div>
            </div>
            
            
            
            
            <%                                URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorType");
                URLConnection urlcon = jsonpage.openConnection();

                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

                DocumentBuilder db = dbf.newDocumentBuilder();

                Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorType");

                NodeList sensortype = doc.getElementsByTagName("sensortype");
		NodeList  profileName=null;
		if(sensortype.getLength()!=0)
		{
				jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype.item(sensortype.getLength() - 1).getFirstChild().getNodeValue() + "/GetProfileByType");
				urlcon = jsonpage.openConnection();

				dbf = DocumentBuilderFactory.newInstance();

				db = dbf.newDocumentBuilder();

				doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype.item(sensortype.getLength() - 1).getFirstChild().getNodeValue() + "/GetProfileByType");

				profileName = doc.getElementsByTagName("graphprofile");
		}
                NodeList xLabel = doc.getElementsByTagName("xlabel");
                NodeList yLabel = doc.getElementsByTagName("ylabel");
                NodeList min = doc.getElementsByTagName("min");
                NodeList max = doc.getElementsByTagName("max");
		
		
		
		
		
		
		URL jsonpage1 = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/GetEndPointDevice");
                URLConnection urlcon1 = jsonpage1.openConnection();    
		//urlcon = jsonpage.openConnection();

                 dbf = DocumentBuilderFactory.newInstance();

                 db = dbf.newDocumentBuilder();

                doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/GetEndPointDevice");
                    NodeList endDeviceName = doc.getElementsByTagName("EndPointDeviceId");
                    NodeList info = doc.getElementsByTagName("DeviceInfo");
                    NodeList location = doc.getElementsByTagName("location");
                    NodeList latitude = doc.getElementsByTagName("latitude");
                    NodeList longitude = doc.getElementsByTagName("longitude");
                    String firstlocation ="";
		     if(location.getLength()!=0)
                    {
                     firstlocation = location.item(0).getFirstChild().getNodeValue();
                    }
		

            %>
            <div class="row">


                <div id="result1">    
                    <div class="col-lg-6" > 

                        <div class="panel panel-primary" style="background-color: #dff0d8; height:545px;">
                            <div class="panel-heading">
                                Add New Sensor
                            </div>
                            <form action="addNewSensorAction.jsp" method="post"  onsubmit="return(validatetxtbox())">
                                <div class="panel-body" id="result2">
                                    <div class="table-responsive"><table class="table">
                                            <tbody>
						
						<tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">End Device Name </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="endDevName" id="endDevName" onclick="EndDeviceDetail(this.value);">
                                                            <% 
					for (int i = 0; i <= endDeviceName.getLength() - 1; i++) {
						String temsen = endDeviceName.item(i).getFirstChild().getNodeValue();
						String endDevName=temsen;
                                                            %>
                                                            <option value="<%=endDeviceName.item(i).getFirstChild().getNodeValue()%>"
                                                                    > 
								<%=endDevName%>
                                                            </option>

                                                            <%
                                                                }
				
                                                            if((endDeviceName.getLength() - 1)<0)
								{
								%>
                                                                    <option > No end device
                                                                    </option>

                                                                    <%
								}
                                                            %>
                                                           
                                                        </select></td>
                                                </tr>

                                                
						          <tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Sensor Type </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="prop" id="prop" onchange="listProfiles(this);">
                                                            <%        String last=null;                                         
                                        for (int i = 0; i <= sensortype.getLength() - 1; i++) {

                                                            %>
                                                            <option value="<%=sensortype.item(i).getFirstChild().getNodeValue()%>"
                                                                    selected> <%=sensortype.item(i).getFirstChild().getNodeValue()%>
                                                            </option>

                                                            <%
                                                            last=sensortype.item(i).getFirstChild().getNodeValue();
                                                                }
								if((sensortype.getLength() - 1)<0)
								{
								%>
                                                                    <option > No sensor type
                                                                    </option>

                                                                    <%
								}
                                                            %>
                                        
                                                        </select></td>

                                                </tr>
                                                <tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Graph Profile</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="graphprofile" id="graphprofile" onchange="graphProfileByType();">
                                                                <%      if(profileName!=null) 
                                                                {                                           for (int i = 0; i <= profileName.getLength() - 1; i++) {

                                                                    %>
                                                                    <option value="<%=profileName.item(i).getFirstChild().getNodeValue()%>"
                                                                            selected> <%=profileName.item(i).getFirstChild().getNodeValue()%>
                                                                    </option>

                                                                    <%
                                                                        }
								}
								else
								{%>
								   <option > No graph profile
                                                                    </option>
								<%}
                                                                    %>
                                        
                                                        </select></td>

                                                </tr>
                    
                                                <tr class="success">
                                                    <!-- <td>Information</td><td> <input type="text" name="sinfor" id="sinfor"/></td>-->
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Information</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <textarea name="sinfor" id="sinfor" cols="30" rows="4"></textarea></td>

                                                </tr>

                                      

                                               
                                                       
                                                       
                                             
                                                <tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Location</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="city" id="city" value="<%=firstlocation.trim() %>" readonly="true"/>
                                                         <%   if(location.getLength()!=0){%>
							<input type="hidden" name="city1" id="city1" value=<%=location.item(0).getFirstChild().getNodeValue()%> />
							 <%   }%>
							  <%   if(latitude.getLength()!=0){%>
                                                        <input type="hidden" id="sLatitude" name="sLatitude" value=<%=latitude.item(0).getFirstChild().getNodeValue()%> />
                                                        <%   }%>
                                                         <%   if(longitude.getLength()!=0){%>
							<input type="hidden" id="sLongitude" name="sLongitude" value=<%=longitude.item(0).getFirstChild().getNodeValue()%> /></td>
                                                <%   }%>
						</tr>
                                                 <% if(last!=null&&last.toLowerCase().compareTo("relay")!=0 && last.toLowerCase().compareTo("ap")!=0)
                                  {
                              %>
                                                <tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Minimum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="minTemp" id="minTemp" onkeypress="return isNumber(event)"/></td>
                                                </tr>
                                                <tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Maximum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="maxTemp" id="maxTemp" onkeypress="return isNumber(event)"/></td>

                                                </tr>
                                                <% 
                                  }else{
                                      %>
                                                    <tr class="success">
                                                        <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Minimum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="minTemp" id="minTemp" disabled="true" onkeypress="return isNumber(event)"/></td>
                                                </tr>
                                                <tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Maximum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input disabled="true" type="text" name="maxTemp" id="maxTemp" onkeypress="return isNumber(event)"/></td>

                                                </tr>
                                                    <% 
                                  }
                                      %>
                                                <tr class="success" style="border-top: none">
                                                <td style="border-top-color: #599bb3; border-style: dashed; border-width: 1px; border: none"></td>
                                                <td style="border-top-color: #599bb3; border-style: dashed; border-width: 1px; border: none"><input type="submit" value="Submit" class="myButton"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="Clear" onclick="return(OnCancel())" class="myButton"/></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <font color='red'> <div id="une"> </div> </font>
                                    </div>

                                    <%                                 jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getProfileList");
                                        urlcon = jsonpage.openConnection();

                                        dbf = DocumentBuilderFactory.newInstance();

                                        db = dbf.newDocumentBuilder();

                                        doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getProfileList");
                                        NodeList graphprofile = doc.getElementsByTagName("graphprofile");

                                    %>

                                </div>
                                
                              
                            </form>

                        </div>

                    </div>

                </div>


                <%
                    jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
                    urlcon = jsonpage.openConnection();

                    dbf = DocumentBuilderFactory.newInstance();

                    db = dbf.newDocumentBuilder();

                    doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
                    NodeList sensorName = doc.getElementsByTagName("sensorname");
                    NodeList sensorType = doc.getElementsByTagName("sensortype");
                    NodeList status_sensor = doc.getElementsByTagName("status");
                    System.out.println("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
                    //System.out.println()
//  System.out.println("id ="+userid.item(i).getFirstChild().getNodeValue()+"\n");
/* out.println(username.item(i).getFirstChild().getNodeValue());
                     System.out.println(userinfo.item(i).getFirstChild().getNodeValue());
                     System.out.println(email.item(i).getFirstChild().getNodeValue());
                     System.out.println(phone.item(i).getFirstChild().getNodeValue());*/

                %>

                <div class="col-lg-6" > 
                    <form name="myform" method="post">
                        <div class="panel panel-primary" style="background-color: #dff0d8;height:545px;">
                            <div class="panel-heading">
                                Sensors <input type="button" id="btnDelete" value="Delete Sensor" name="btnDelete" onclick="OnButton2()" class="myButton" style="float: right;">
                            </div>
                            <div class="panel-body">

                                <div class="table-responsive" style="height:480px;">
                                    <table class="table" id="myTable" style="border: #599bb3; border-style: dashed; border-width: 1px;">
                                        <!-- <input type=button value="Select All" onClick="this.value=check(this.form)">-->
                                        <tbody>
                                            <tr style=" 
						border: 1px solid #337AB7;
						color:white;
						background-color: #337AB7;
						-moz-border-radius: 5px;
						-webkit-border-radius: 5px; 
						background-image: -webkit-linear-gradient(top,#337ab7 0,#2e6da4 100%);">
                                                <th style=" text-align: center;">End Device</th>
                                               
                                                <th style="text-align: center;">Sensor Type</th>
                                            </tr>
                                            <%                                                 for (int i = 0; i <= sensorName.getLength() - 1; i++) {
                                                    if(!sensorType.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay"))
                                                    {

                                            %>

                                            <tr style="background-color: #dff0d8;" class="sensoredit" id="tr"  onclick="rowSelect(this, '<%=sensorName.item(i).getFirstChild().getNodeValue()%>')">


                                                <td hidden="true"><input type="radio" name="rbox[]" id="rbox[]"  value="<%=sensorName.item(i).getFirstChild().getNodeValue()%>" onclick="rowSelect(this, '<%=sensorName.item(i).getFirstChild().getNodeValue()%>')" style="visibility:hidden;"></td>
                                                <td hidden="true"> <input type="radio"  name="rbox2[]"  id="rbox2[]"   value="<%=status_sensor.item(i).getFirstChild().getNodeValue()%>" style="visibility:hidden;">
                                                </td>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%
                                                    String temsen=sensorName.item(i).getFirstChild().getNodeValue();
                                                temsen = temsen.substring(0, temsen.lastIndexOf("_"));
                                                    out.print(temsen);
                                                    %></td>
                                                <td  style="border: #599bb3; border-style: dashed; border-width: 1px;"><%=sensorType.item(i).getFirstChild().getNodeValue()%></td>
                                            </tr>

                                            <% }
                                                }
                                            %>

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>

        <script src="js/bootstrap.min.js"></script> 
        <script src="js/dialog.js"></script>
        <!--<div class="col-lg-12" style="background: none repeat scroll 0% 0% #11020B; color: #FFF; height:30px; text-align: right">Powered by Gadgeon Smart Systems Pvt. Ltd.</div>-->
     <script>
            if(mnc==0)
    {
	NProgress.set(1);
    }
	mnc=1;
            
        </script>
    </body>
    <%
            }
        } else {
            response.sendRedirect("index.jsp");
        }
    }
    catch(Exception e)
    {
        System.out.println("Eror "+e.getCause());
        //e.printStackTrace();
    }
    %>
    
</html>
