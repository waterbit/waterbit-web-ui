<%-- 
    Document   : logout
    Created on : 1 Dec, 2014, 12:49:22 PM
    Author     : sreeja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%
            session.invalidate();
            response.sendRedirect("index.jsp?val=1");
            %>
    </body>
</html>
