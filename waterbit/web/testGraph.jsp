<%-- 
    Document   : testGraph
    Created on : 18 Feb, 2015, 2:22:49 PM
    Author     : ajith
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="js/dygraph-dev.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <div id="div_g" style="width:95%; height:204px; background-color: #FFF;"></div>
        <script type="text/javascript">
            var dt=new Date();
            var data=[];
//data=[dt,-50.0,dt+1,-50.0,dt+2,-50.0,dt+3,-50.0,dt+4,-50.0,dt+5,-50.0,dt+6,-50.0,dt+7,0];            
data.push([dt,-50.0]);data.push([new Date(dt.getTime()+20),50.0]);data.push([new Date(dt.getTime()+30),-50.0]);data.push([new Date(dt.getTime()+40),-50.0]);data.push([new Date(dt.getTime()+60),-50.0]);
  g = new Dygraph(

    // containing div
    document.getElementById("div_g"),

    // CSV or path to a CSV file.
    data,{drawPoints: true,
                            showRoller: true,
                            ylabel: 'Temperature (C)',
                            xlabel: 'Time',
                             stackedGraph: true,
                            valueRange: [-60, 80],
                            labels: ['Time', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_}

  );
</script>
    </body>
</html>
