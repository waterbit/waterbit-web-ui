<%-- 
    Document   : login1
    Created on : 5 Mar, 2015, 8:58:23 AM
    Author     : nisha
--%>

<!doctype html>
<html>
    <head>

        <title>WaterBit</title>


        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="css/bstrap.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/loginStyle.css">
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
	<script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/alertify.min.js"></script>
        <script type="text/javascript">
            function validatetxtbox()
            {
                if (document.getElementById("username").value == "")
                {
                     alertify.alert("Invalid username or password");
                    //document.getElementById("une").innerHTML = "*Username name should not be empty";
                    //document.getElementById('une').innerHTML = "* Please enter valid username";
                    //document.getElementById("username").focus();
                    return(false);

                }
                else if (document.getElementById("password").value == "")
                {
                    //alert("Please enter valid username");
                    alertify.alert("Invalid username or password");
                    //document.getElementById('une').innerHTML = "* Please enter valid password";
                    //document.getElementById("username").focus();
                    return(false);

                }
                else
                {

                    return(true);
                }
            }
        </script>
    </head>
    <body>


        <div class="container" style="margin-top: 7%;">

            <%
                String[] values = request.getParameterValues("val");
                if (values != null) {
                    if (values[0].equals("0")) {
            %>  

            <script>

               
                alertify.alert("Invalid username or password..!");


            </script>
            <%
                }

                if (values[0].equals("1")) {%>  
            <script>
  alertify.success("Successfully logout..!");
                $.ajaxSetup({
                    // Disable caching of AJAX responses */
                    cache: false
                });
                setTimeout(function () {
                    shortURL = top.location.href.substring(0, top.location.href.indexOf('?'));
                    window.location.href = shortURL;
                }, 1000);

              

            </script>
            <%
                    }
                }
            %>

            <div class="omb_login">
                <!--    	<h3 class="omb_authTitle">Login </h3>-->


                <div class="row omb_row-sm-offset-3 omb_loginOr">
                    <div class="col-xs-12 col-sm-6" style="margin-left: 45%;">
                        <img src="images/gadgeon_logo.png" width="101" height="101" >
                    </div>
                </div>

                <div class="row omb_row-sm-offset-3">
                    <div class="col-xs-12 col-sm-6">	
                        <form class="omb_loginForm" action="loginAction.jsp" autocomplete="off" method="POST" onsubmit="return(validatetxtbox())">
                            <div class="input-group" style="margin-left: 20%; width: 60%; height: 50px;">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="username"  id="username" placeholder="User Name" style="height: 50px;">
                            </div>
                            <span class="help-block"></span>

                            <div class="input-group" style="margin-left: 20%; width: 60%; height: 50px;">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input  type="password" class="form-control" name="password" id="password" placeholder="Password" style="height: 50px; ">
                            </div>
                            <br><br>

                            <button class="btn btn-lg btn-primary btn-block" type="submit" style="margin-left: 20%;width: 60%;">Login</button>
                            <font color='red'> <div id="une"> </div> </font>	
                        </form>

                    </div>

                </div>

            </div>



        </div>
    </body>
</html>
