
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>
<!DOCTYPE html>
<html lang="en">
    <head>

        <title>WaterBit</title>

 <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/main.css" >
        <link rel="stylesheet" href="css/bstrap.css" >
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" >
        <link rel="stylesheet" href="css/dialog.css" >
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

        <link href="css/tabcontent.css" rel="stylesheet" type="text/css">
        <script src="js/smooth-plotter.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
       <script src="js/jquery-ui.js"></script>
       <script src="assets/nprogress/nprogress.js"></script>
	<link href="assets/nprogress/nprogress.css" rel="stylesheet" />

        <style> input{display:none !important;}

            .sensoredit{

            }
            .sensoredit:hover {
                cursor:pointer;
            }
            .showDiv{
                margin-top: 0px;
            }
            .hideDiv{
                margin-bottom: 2500px;
            }
              ul.nav li.dropdown:hover > ul.dropdown-menu {
		display: block;    
	    }
	    .caret-right {
		display: inline-block;
		width: 0;
		height: 0;
		margin-left: 5px;
		vertical-align: middle;
		border-left: 4px solid;
		border-bottom: 4px solid transparent;
		border-top: 4px solid transparent;
	    }
	    #li1:hover
	    {
		background-color: #e7e7e7;
		height:58px;
		color: #000;
		border-radius: 5px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
	    }
            .navbar{
                margin-bottom: 0px;
            }
        </style>

        <style>
            .mysensor{


            }
            .myevent{


            }
            .mysensor:hover {
                cursor:pointer;
            }
            .myevent:hover {
                cursor:pointer;
            }
            .col{
                margin: 50px;
            }
        </style>
        <style>
            .myButton {
                -moz-box-shadow: 0px 10px 14px -7px #276873;
                -webkit-box-shadow: 0px 10px 14px -7px #276873;
                box-shadow: 0px 10px 14px -7px #276873;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
                background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
                background-color:#599bb3;
                -moz-border-radius:3px;
                -webkit-border-radius:3px;
                border-radius:4px;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-family:Times New Roman;
                font-size:12px;
                font-weight:bold;
                padding:2px 10px;
                text-decoration:none;
                text-shadow:0px 1px 0px #3d768a;
            }
            .myButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
                background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
                background-color:#408c99;
            }
            .myButton:active {
                position:relative;
                top:1px;
            }
            .loaddiv
            {
                margin-left:37%;margin-top:11%;
            }

            @media only screen 
            and (min-width : 320px) 
            and (max-width : 768px)
            {
                /* Styles */
                .loaddiv
                {
                    margin-left:30% !important;margin-top:30% !important;
                }
            }

            /* Smartphones (landscape) ----------- */
            @media only screen 
            and (min-width : 321px) {
                /* Styles */

            }

            /* Smartphones (portrait) ----------- */
            @media only screen 
            and (max-width : 320px) {
                /* Styles */

            }

            /* iPads (portrait and landscape) ----------- */
            @media only screen 
            and (min-width : 1024px) 
            and (max-width : 1200px) 
            {
                /* Styles */
                .col{
                    margin: 20px 10px !important;
                }
            }

            /* iPads (landscape) ----------- */
            @media only screen 
            and (min-width : 768px) 
            and (max-width : 1024px) 
            and (orientation : landscape) {
                /* Styles */
                .loaddiv
                {
                    margin-left:30% !important;margin-top:15% !important;
                }
            }

            /* iPads (portrait) ----------- */
            @media only screen 
            and (min-width : 768px) 
            and (max-width : 1024px) 
            and (orientation : portrait) {
                /* Styles */
                .loaddiv
                {
                    margin-left:30% !important;margin-top:15% !important;
                }
            }

            /* Desktops and laptops ----------- */
            @media only screen 
            and (min-width : 1224px) {
                /* Styles */
            }

            /* Large screens ----------- */
            @media only screen 
            and (min-width : 1824px) {
                /* Styles */
            }

            /* iPhone 4 ----------- */
            @media
            only screen and (-webkit-min-device-pixel-ratio : 1.5),
            only screen and (min-device-pixel-ratio : 1.5) {
                /* Styles */
            }

        </style>


        <%
            // String datas="[{x1=2015-02-10T14:36:36+05:30, y1=3.1}, {x1=2015-02-10T14:35:53+05:30, y1=3.2}, {x1=2015-02-10T14:35:36+05:30, y1=3.3}, {x1=2015-02-10T14:34:34+05:30, y1=3.4}, {x1=2015-02-10T14:33:56+05:30, y1=3.27419}, {x1=2015-02-10T14:33:42+05:30, y1=3.5}, {x1=2015-02-10T14:26:05+05:30, y1=1.33871}, {x1=2015-02-10T14:25:05+05:30, y1=1.33226}, {x1=2015-02-10T14:24:33+05:30, y1=1.34516}, {x1=2015-02-10T14:24:15+05:30, y1=1.34516}]";
            try {
                System.out.println(session.getAttribute("userid"));
                if (session.getAttribute("userid") != null) {

                    int userid = (Integer) session.getAttribute("userid");
                    System.out.println(userid);
                    // int userid = 25;
                    if (userid > 1) {
        %> 
        <script src="js/alertify.min.js"></script>
        <script type="text/javascript">
            var historygrah="";
            var historydata="";
            var historyoption="";
            var colors = ["red", "blue", "green", "yellow"];
            var g;
            var data = [];
            var t = new Date();

            var xval;
            var yval;
            var min;
            var max;
            var titles;
            var liveDataInt = [];
            var checkflag = "false";
            var flag=0;
            
        
         
        
            function rewrite(divid, txt, i)
            {
                $.ajaxSetup({
                    // Disable caching of AJAX responses */
                    cache: false
                });
                $.get("livedatademo.jsp?sensor=" + txt, function (data11, status) {
                    senval = txt;
                    // document.getElementById("sensorlabel").innerHTML = senval;
                    // alert(data11); 
                    // alert(txt+" set");
                    //  alert(divid+data11.trim().length);
                    if (data11.trim().length == 2) {
                        $("#" + divid).html("<div class='loaddiv' >No Data Available for " + txt.substring(txt.indexOf("_") + 1) + " since 5 hours </div> ");

                    }
                    else {
                        $.get("graphdetail.jsp?sensor=" + txt, function (data12, status) {
                            data12 = data12.trim().split(":");
                            //alert("haii@@@i"+data12[0])
                            xval = data12[0];
                            yval = data12[1];
                            min = data12[2];
                            var interval=data12[3].split("~");
		            max = interval[0];
                            titles = txt.substring(txt.lastIndexOf("_") + 1);
                            livedatadiv(3, data11, divid, txt);

                        });
                    }


                });
                liveDataInt[i] = window.setInterval(function () {

                    $.get("livedatademo.jsp?sensor=" + txt, function (data11, status) {
                        senval = txt;
                        // document.getElementById("sensorlabel").innerHTML = senval;
                        // alert(data11); 
                        // alert(txt+" set");
                        //  alert(divid+data11.trim().length);
                        if (data11.trim().length == 2) {
                            $("#" + divid).html("<div class='loaddiv' >No Data Available for " + txt.substring(txt.indexOf("_") + 1) + " since 5 hours </div>  ");

                        }
                        else {
                            $.get("graphdetail.jsp?sensor=" + txt, function (data12, status) {
                                data12 = data12.trim().split(":");
                                //alert("haii@@@i"+data12[0])
                                xval = data12[0];
                                yval = data12[1];
                                min = data12[2];
                               var interval=data12[3].split("~");
		            max = interval[0];
                                titles = txt.substring(txt.lastIndexOf("_") + 1);
                                livedatadiv(3, data11, divid, txt);

                            });
                        }


                    });
                   

                }, 60000
                        );

            }
            var selectionStatus=0;
            function optionSelect(sensor)
            {
             if(selectionStatus>0)
                 $("#graph").html("");
                selectionStatus++;
                if(flag==3)
                { 
                listSensor(sensor);
            }else{
                if (sensor == "-1")
                    return;

                var se = sensor.trim();
                $.get("alarmcount.jsp?enddevice=" + se, function (data11, status) {
                    $("#alarmcount").html("Total Event Count :" + data11.trim());
                });
                $.get("livedatalistsensor.jsp?type=" + se, function (data11, status) {
           
                    var data = data11.trim().split("**");

                    dydiv(data.length - 1, data);

                });
            }
            }
            function getDataHR(newMinutes) {
                MINS_PER_YEAR = 24 * 365 * 60;
                MINS_PER_MONTH = 24 * 30 * 60;
                MINS_PER_WEEK = 24 * 7 * 60;
                MINS_PER_DAY = 24 * 60;
                MINS_PER_HRS = 60;
                minutes = newMinutes;
                days = Math.floor(minutes / MINS_PER_DAY);
                minutes = minutes - days * MINS_PER_DAY;
                hours = Math.floor(minutes / MINS_PER_HRS);
                minutes = minutes - hours * MINS_PER_HRS;
                return  days + " Days " + hours + " Hrs " + minutes + " Mins";
            }
            function dydiv(count, data) {
                var output = document.getElementById('output');
                var i = 0;
                var val = "";
                $("#output").html("");
                for (var j = 0; j < liveDataInt.length; j++)
                {
                    clearInterval(liveDataInt[j]);
                }

                liveDataInt = [];
                while (i < count)
                {

                    if (!document.getElementById("div_g" + i))
                    {
                        var ele = document.createElement("div");
                        ele.setAttribute("id", "div_g" + i);
                        ele.setAttribute("class", "panel panel-primary col");
                        ele.setAttribute("style", "height: 290px;width: 90%; background-color: white;float:left;margin:40px;margin-bottom: 0px;text-transform: capitalize;");
                        ele.innerHTML = "<div class='loaddiv' >" + data[i].substring(data[i].lastIndexOf("_") + 1) + " graph loading ...</div> ";

                        output.appendChild(ele);
                        rewrite("div_g" + i, data[i], i);
                    }

                    i++;


                }
            }

            function livedatadiv(id, data11, divid, txt)
            {
                data = [];
                $.ajaxSetup({
                    // Disable caching of AJAX responses */
                    cache: false
                });

                data11 = data11.trim();
                var datalist = data11.split("***");
                var lastSeen = 0;
                data11 = datalist[0];
                if (datalist.length == 2)
                    lastSeen = datalist[1];
                if (data11.length != 2) {

                    if (txt.indexOf("rumencontraction") > -1) {

                        var details = data11.substr(1, data11.length);
                        details = details.substr(0, details.length - 2);
                        var dt = details.split("},");
                        if ($(divid).html() == "No data Available")
                            data = [];
                        if ($(divid).html() != "No data Available" && id == 3)
                            id = 2;
                        var dates = [];
                        var vals = [];
                        data = [];
                        var lastDate;
                        for (var i = 0; i < dt.length; i++)
                        {

                            dates[i] = dt[i].substr(dt[i].indexOf("=") + 1, dt[i].indexOf(",") - dt[i].indexOf("=") - 1);
                            vals[i] = dt[i].substr(dt[i].indexOf("y1=") + 3);
                            dts = dates[i].split("T");
                            dts1 = dts[0].split("-");
                            dts2 = dts[1].split("Z");
                            dts3 = dts2[0].split(":")

                            data.push([new Date(dts1[0], dts1[1] - 1, dts1[2], dts3[0], dts3[1], dts3[2], 0), parseFloat(vals[i])]);
                            lastDate = new Date(dts1[0], dts1[1] - 1, dts1[2], dts3[0], dts3[1], dts3[2], 0);
                        }



                        {
                            if (titles == "rumencontraction")
                            {
                                titles = "Rumen Contraction";
                            } else if (titles == "rumentemperature")
                            {
                                titles = "Rumen Temperature";
                            } else if (titles == "gatewayhumidity")
                            {
                                titles = "Gateway Humidity";
                            } else if (titles == "gatewaytemperature")
                            {
                                titles = "Gateway Temperature";
                            }
                            g = new Dygraph(document.getElementById(divid), data,
                                    {
                                        drawPoints: false,
                                        showRoller: true,
                                        ylabel: yval,
                                        xlabel: xval,
                                        stackedGraph: true,
                                        title: titles + " <div style='float:right;font-size:16px;font-weight: normal;' >Time since last data : " + getDataHR(parseInt(lastSeen)) + "</div> ",
                                        valueRange: [min, max],
                                        labels: ['Date', 'Value'],
                                        interactionModel: Dygraph.Interaction.nonInteractiveModel_,
                                        plotter: smoothPlotter,
                                        strokeWidth: 2
                                    });
                            g.ready(function () {
                                var annotations = [];
                                for (var i = 0; i < dt.length; i++)
                                {


                                    dates[i] = dt[i].substr(dt[i].indexOf("=") + 1, dt[i].indexOf(",") - dt[i].indexOf("=") - 1);
                                    vals[i] = dt[i].substr(dt[i].indexOf("counter=") + 8);
                                    dts = dates[i].split("T");
                                    dts1 = dts[0].split("-");
                                    dts2 = dts[1].split("Z");
                                    dts3 = dts2[0].split(":")

                                    annotations.push(
                                            {
                                                series: "Value",
                                                x: new Date(dts1[0], dts1[1] - 1, dts1[2], dts3[0], dts3[1], dts3[2], 0).getTime(),
                                                shortText: parseFloat(vals[i]),
                                                text: "Count :" + parseFloat(vals[i])
                                            });

                                }
                                g.setAnnotations(annotations);

                            });
                        }

                    }
                    else
                    {
                        var details = data11.substr(1, data11.length);
                        details = details.substr(0, details.length - 2);
                        var dt = details.split("},");
                        if ($(divid).html() == "No data Available")
                            data = [];
                        if ($(divid).html() != "No data Available" && id == 3)
                            id = 2;
                        var dates = [];
                        var vals = [];
                        if (dt.length != 10)
                            data = [];
                        for (var i = 0; i < dt.length; i++)
                        {
                            dates[i] = dt[i].substr(dt[i].indexOf("=") + 1, dt[i].indexOf(",") - dt[i].indexOf("=") - 1);
                            vals[i] = dt[i].substr(dt[i].indexOf("y1=") + 3);
                            dts = dates[i].split("T");
                            dts1 = dts[0].split("-");
                            dts2 = dts[1].split("Z");
                            dts3 = dts2[0].split(":")
                     
                            data.push([new Date(dts1[0], dts1[1] - 1, dts1[2], dts3[0], dts3[1], dts3[2], 0), parseFloat(vals[i])]);
                            lastDate = new Date(dts1[0], dts1[1] - 1, dts1[2], dts3[0], dts3[1], dts3[2], 0);
                        }

                        if (titles == "rumencontraction")
                        {
                            titles = "Rumen Contraction";
                        } else if (titles == "rumentemperature")
                        {
                            titles = "Rumen Temperature";
                        } else if (titles == "gatewayhumidity")
                        {
                            titles = "Gateway Humidity";
                        } else if (titles == "gatewaytemperature")
                        {
                            titles = "Gateway Temperature";
                        }
                        smoothPlotter.smoothing = .7;
                       // if (data11.length != 2)
                            g = new Dygraph(document.getElementById(divid), data,
                                    {
                                        drawPoints: true,
                                        showRoller: true,
                                        ylabel: yval,
                                        xlabel: xval,
                                        valueRange: [min, max],
                                        stackedGraph: true,
                                        title: titles + " <div style='float:right;font-size:16px;font-weight: normal;' >Time since last data: " + getDataHR(parseInt(lastSeen)) + "</div>",
                                        color: colors[divid.replace("div_g", "")],
                                        labels: ['Time', 'Value'],
                                        interactionModel: Dygraph.Interaction.nonInteractiveModel_,
                                        plotter: smoothPlotter,
                                        strokeWidth: 2
                                    });
                    }
                }
                else
                {
                    clearInterval(liveDataInt);

                    $("#" + divid).html("<div class='loaddiv' >No Data Available</div> ");

                }
            }
           
            function toUTC(/*Date*/date) {
                return Date.UTC(
                        date.getFullYear()
                        , date.getMonth()
                        , date.getDate()
                        , date.getHours()
                        , date.getMinutes()
                        , date.getSeconds()
                        , date.getMilliseconds()
                        );
            }
            
              function showTab(i,cnt)
            {
                for(var l=0;l<cnt;l++)
                {
                    $("#tab"+l).removeClass("selected");
                }
                $("#tab"+i).addClass("selected");
if(i==1 )
{
    flag=3;
    $("#output").html("");
 document.getElementById("output").style.display="none";
    document.getElementById("history").style.display="block";
    listSensor(document.getElementById("cmbtype").value);
             g = new Dygraph("div_g",historydata,historyoption);
     


   
}else if(i==2)
{
    flag=0;
             document.getElementById("output").style.display="none";
             document.getElementById("history").style.display="none";  
}
else{
    flag=0;
     $("#output").html("");
         document.getElementById("history").style.display="none";
      document.getElementById("output").style.display="block";
        var s = document.getElementById("cmbtype");
        optionSelect(s.value);
   
}
            }

        </script>

    </head>
    <body style="height: 100%;">
        <script>
  NProgress.set(0.0);
               NProgress.start();
              var mnc=0;
        </script>
        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>  
        </div>

        <div class="container">  
<div class="navbar navbar-default" role="navigation" style="background: none repeat scroll 0% 0% #11020B; color: #FFF;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="color: #FFF"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="userDashBoard.jsp" id="li1"><img src="img/icon-dashboard.png" width="22" height="22"/>Dashboard</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="li1"><img src="img/icon-form-style.png" width="22" height="22"/>Configuration&nbsp;&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu" style="background: none repeat scroll 0% 0% #11020B;">
                                <li ><a href="addNewEndDevice.jsp">End Device</a> </li>
                                <li ><a href="addNewSensor.jsp"> Sensor</a> </li>
                                <li><a href="addNewSensorType.jsp">Sensor Type</a></li>
                                <li><a href="addNewGraphProfile.jsp">Graph Profile</a></li>
                            </ul>
                        </li>
                        <li class ="active">
			    <a href="panelView.jsp" style="font: normal normal 16px Verdana, Geneva, Arial, Helvetica, sans-serif;" id="li1"><img src="img/icon-charts-graphs.png" width="22" height="22"/>Analytics&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="logout.jsp" id="li1"><img src="img/img-profile.jpg" width="28" height="28"/>Logout</a></li>
                    </ul>
                </div>
            </div>

            <script type="text/javascript" src="js/dygraph-dev.js"></script>


            <div class="row">


                <div class="col-lg-6" style="width:100% !important;">
                    <div class="panel panel-primary" style=" background-color: white;margin-top:20px;">
                        <div class="panel-heading">  <select name="cmbtype"  id="cmbtype" style="font-size:13px !important;color : black !important" onchange="optionSelect(this.value)"> 
                                <option value="-1" >---Select End Device---</option>
                                <%
                                    String firstDevice = "";

                                    URL jsonpage1 = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/GetEndPointDevice");
                                    URLConnection urlcon1 = jsonpage1.openConnection();

                                    DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();

                                    DocumentBuilder db1 = dbf1.newDocumentBuilder();

                                    Document doc1 = db1.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/GetEndPointDevice");
                                    NodeList endDeviceName1 = doc1.getElementsByTagName("EndPointDeviceId");
                                    NodeList info1 = doc1.getElementsByTagName("DeviceInfo");
                                    NodeList location1 = doc1.getElementsByTagName("location");

                               
                                    int jk = 0;

                                    for (int ik = 0; ik <= endDeviceName1.getLength() - 1; ik++) {


                                 
                                %> <option <%= request.getParameter("cmb")==null?(  firstDevice.equals("") ? " selected " : " "):(request.getParameter("cmb").contains(endDeviceName1.item(ik).getFirstChild().getNodeValue())?" selected ":"")%> value="<%=endDeviceName1.item(ik).getFirstChild().getNodeValue()%>" ><% String senval = endDeviceName1.item(ik).getFirstChild().getNodeValue();
                                    if (firstDevice.equals("")) {
                                        firstDevice = senval;
                                    }

                                                  out.print(senval); %></option>
                                    <%  }

                                    %> 
                            </select>
                            <div id="alarmcount" style="float:right" >Total Event Count</div>
                            <div style="margin-top: 10px;position: inherit;padding-top: 10px;padding-bottom: -1px">
                            <ul class="tabs" data-persist="true">
                                <li id="tab0"  <%= request.getParameter("cmb")==null?" class=\"selected\"":" "%> style="cursor:pointer" onclick="showTab(0, 3);"  ><a>Live Data</a></li>
                                <li id="tab1" style="cursor:pointer" onclick="showTab(1, 3);" ><a>History Data</a></li>
                                
                                </div>
                        </div>
                        <div id="output" class="out">
                        </div>
                                    <div id="history" <%= (request.getParameter("cmb")==null?" style=\"display:none\"":"; \"padding-top: 10px\" ")%>  >
                                 <script>
      var from,to,offset,xlabels,ylabels,titles,valueRanges,count,fromDate,toDate; 
      function dispDate()
      {
           document.getElementById("dates").style.display="block";
      }
      function change()
      {
         
           document.getElementById("dates").style.display="none";
      }
      function showGraph(id)
      {
          if(id==0)
          offset=$("#txtpage").val()-1;
          else if(id==1)
              offset++;
          else if(id==2)
              offset--;
         
          if(offset<0||offset>=(parseInt(count/1000)))
          {
              alertify.alert('No data available');
              return;
          }
          

         $.get("historyoffset.jsp?sensor="+sensor+"&from="+from+"&to="+to+"&offset="+(offset*1000)+"", function (data11, status) {
             $("#div_g").html("");
             livedata(sensor,data11);
             
              $("#pagelist").html("Pages "+(offset+1)+" of "+((parseInt(count/1000)==0)?"1":parseInt(count/1000)));
              if((parseInt(count/1000)==0))
              {
                  $("#btnpage").css("display","none !important");
                  $("#txtpage").css("display","none !important");
              }
         });
         
      }
    </script>
    <style>
        input{display:none !important;}
        input[ type="checkbox"]{display:inline !important;}
        input[ type="radio"]{display:inline !important;}
        input[ type="button"]{display:inline !important;}
        input[ type="submit"]{display:inline !important;}
        .cls{display:inline !important;}
    </style>
                  <style>
    .myButton {
	-moz-box-shadow: 0px 10px 14px -7px #276873;
	-webkit-box-shadow: 0px 10px 14px -7px #276873;
	box-shadow: 0px 10px 14px -7px #276873;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
	background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
	background-color:#599bb3;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:4px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Times New Roman;
	font-size:12px;
	font-weight:bold;
	padding:2px 10px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}
.myButton:active {
	position:relative;
	top:1px;
}

.square h1 {
    color: #599bb3;
}
.loaddiv{
    //margin-top:20%;
    margin-left:38%;
}
.img1
                {
margin-top: 19.4%;
                }
@media only screen 
            and (min-width : 320px) 
            and (max-width : 768px)
            {
            /* Styles */
                .loaddiv{
                    margin-top:50%;margin-left:40%;
                }
             }
             
              @media only screen 
            and (min-width : 768px) 
            and (max-width : 1024px) 
            and (orientation : landscape) {
            /* Styles */
                    .loaddiv{
                    margin-top:30%;margin-left:40%;
                }
                .img1
                {
                    margin-top:26.4%;
                }
            }

            /* iPads (portrait) ----------- */
            @media only screen 
            and (min-width : 768px) 
            and (max-width : 1024px) 
            and (orientation : portrait) {
            /* Styles */
             .loaddiv{
                    margin-top:30%;margin-left:40%;
                }
                .img1
                {
                    margin-top:26.4%;
                }
            }

</style>
    <%
        String   strTemp,strCount;
        strTemp="";strCount="";
        String firstdevice="",firstdeviceComplete="";
        if (session.getAttribute("userid") != null) { 
%>
<script>
    function checkFrm(frm)
    {
        if(document.getElementById("cmbsensor").value==-1||document.getElementById("cmbtype").value==-1)
        {
         
             if(document.getElementById("cmbsensor").value==-1&&document.getElementById("cmbtype").value==-1)
                alertify.alert('Please Select End Device & Sensor ');               
             else
                alertify.alert('Please Select Sensor');
             return false;
        }
        return true;
    }
    function listSensor(cmb)
    {
        $.get("listSensors.jsp?type="+cmb, function (data11, status) {
                        var select = document.getElementById("cmbsensor");
                        var length = select.options.length;
                        $("#cmbsensor").empty();
                        var option = document.createElement('option');
                        option.text = "---select sensor---";
                            option.value = "-1";
                            select.add(option, 0);
                         var data=data11.trim().split("**");
                         for(var i = 0; i < data.length-1; ++i) {
                            var option1 = document.createElement('option');
                            option1.text = data[i];
                            option1.value = cmb+"_"+data[i];
                       
                            select.add(option1, i+1);
                        }
        });
    }
</script>
  <div  style="    background-color: white;margin-top: 20px">
      <form name="frm" method="post" onsubmit="return checkFrm(this)" >

    <%
            
       try
       {
           if(userid==1)
            {
             
              URL jsonpage = new URL("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getSensorList");
           
        URLConnection urlcon = jsonpage.openConnection();
   
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getSensorList");
        NodeList sensorname =  doc.getElementsByTagName("sensorname");
        NodeList sensortype = doc.getElementsByTagName("sensortype");
        NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
        
       
    
                                                for(int i=0;i<=sensorname.getLength()-1;i++)
                                                {
                                                    if(!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")&&!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("ap")){
                                                   
                                                        %> 
                                                            
                                                            <% String senval= sensorname.item(i).getFirstChild().getNodeValue(); 
                                                senval=senval.substring(0,senval.lastIndexOf("_"));
                                                if(firstdevice.equals(""))
                                                {    firstdevice=senval;
                                                }
                                                 %>
                                                     <%
                                                    }
                                                }
            }  
           else
           {
               URL jsonpage = new URL("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+userid+"/getSensorListByUserId");
           
        URLConnection urlcon = jsonpage.openConnection();
   
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+userid+"/getSensorListByUserId");
        NodeList sensorname =  doc.getElementsByTagName("sensorname");
        NodeList sensortype = doc.getElementsByTagName("sensortype");
        NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
        
       
                                                 int checkJ=-1;
                                                String oldsen="";
                                                for(int i=0;i<=sensorname.getLength()-1;i++)
                                                {checkJ=-1;
                                                    if(request.getParameter("cmb")==null)
                                                    {
                                                       
                                                    }
                                                    else
                                                    {
                                                        firstdevice="jj";
                                                        if(request.getParameter("cmb").substring(0,request.getParameter("cmb").lastIndexOf("_")).equals(sensorname.item(i).getFirstChild().getNodeValue().substring(0,sensorname.item(i).getFirstChild().getNodeValue().lastIndexOf("_"))))
                                                        {checkJ=0;
                                                        
                                                            %>
                                                       
                                                            <%
                                                        }
                                                    }
                                                    if(!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")&&!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("ap")){
                                                        String senval= sensorname.item(i).getFirstChild().getNodeValue(); 
                                                senval=senval.substring(0,senval.lastIndexOf("_"));
                                                        if(!oldsen.contains(senval+"*")){
                                                            oldsen+=senval+"*";
                                                   
                                                    %> 
                                                    <% 
                                                 if(firstdevice.equals(""))
                                                 {   firstdevice=senval;
                                                 
                                                 }
                                                  %>
                                                     <%
                                                
                                                        }
                                                    }
                                                }
           }
       
       }catch(Exception e)
       {
       }
  
                                               
                                                %> 
    </select>
    
    <select  name="cmb"  id="cmbsensor" >
        <option value="-1" >---Select Sensor---</option>
        <%
       try
        { firstdeviceComplete="";
        if(request.getParameter("cmb")!=null)
            firstdevice=request.getParameter("cmb").substring(0,request.getParameter("cmb").lastIndexOf("_"));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+firstdevice+"/GetsensorNameByEndPointDevice");
            System.out.println("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+firstdevice+"/GetsensorNameByEndPointDevice");;
            NodeList sensorname =  doc.getElementsByTagName("sensorname");
                                                int checkJ=0;
                                                for(int i=0;i<=sensorname.getLength()-1;i++)
                                                {
                                                    checkJ=-1;
                                                    if(request.getParameter("cmb")==null)
                                                    {
                                                       
                                                    }
                                                    else
                                                    {
                                                        firstdeviceComplete="jj";
                                                        if(request.getParameter("cmb").equals(sensorname.item(i).getFirstChild().getNodeValue()))
                                                            checkJ=1;
                                                    }
                                                    {
                                                   
                                                        String senval= sensorname.item(i).getFirstChild().getNodeValue(); 
                                                senval=senval.substring(senval.lastIndexOf("_")+1,senval.length());
                                                %>
                                                <option <%=firstdeviceComplete.equals("")||checkJ==1?" selected ":""%> value="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" ><%=senval%></option>
                                                <%
                                                
                                                if(firstdeviceComplete.equals(""))
                                                    firstdeviceComplete=sensorname.item(i).getFirstChild().getNodeValue();
                                                
                                                    }
                                                }
        }
        catch(Exception e)
        {
            System.out.println("exception  :  "+e.toString());
        }
        %>
    </select>
     &nbsp;&nbsp;&nbsp;
     <%
        
            int weekly = 0;
            int monthly = 0;
            int tSer = 0;
            if(request.getParameter("rad")==null)
            { 
                weekly = 1;
            }
            else 
            {
                    if(request.getParameter("rad").equals("weekly"))
                   {
                         weekly = 1;
                   }

                   if(request.getParameter("rad").equals("Monthly"))
                   {
                       monthly =1;
                   }
                   if(request.getParameter("rad").equals("time"))
                   {
                       tSer = 1;
                   }
            }
      %>
    
       <input class="cls" type="radio" id="1" name="rad" value="weekly" onclick="change()" <%= weekly++==1?" checked ":"" %>><label style=font-size:10 for="1"> Weekly</label>
            &nbsp;&nbsp;&nbsp;<input type="radio" id="2" name="rad" value="Monthly" onclick="change()" <%= monthly++==1?" checked ":"" %>><label style=font-size:10 for="2"> Monthly</label>
           &nbsp;&nbsp;&nbsp;<input type="radio" id="3" name="rad" value="time" onclick="dispDate()" <%= tSer++==1?" checked ":"" %>><label style=font-size:10 for="3"> Time Series</label>
    &nbsp;&nbsp;&nbsp;<input class="myButton"  type="submit"  name="btnSubmit" value="Plot Graph" >
      </div>
    <div id="dates" style="display: none;height:10px !important;" >
        Date From: <input type="text" name="from"  class="cls" value ='from' id="from">Date To: <input name="to"  class="cls" value='to' type="text" id="to">
            <br>
            
      </div>
</form>
    
    <script type="text/javascript" src="js/dygraph-dev.js"></script>
    <script>
        var check=0;
    <%
      if(request.getParameter("cmb")==null)
      {
          %>
              check=1;
              <%
      }
            
            %>
    </script> 
    <div style="border: #659801;width:815;height:600px;margin-left: 0px;
    border-width: 2px;
    border-style: solid; border-color: #408c99; margin-top: 10px; background-color:#FFF;" id="graph"  >
    <div style="float:left;margin-left:2.5%"><img style="margin-top:460%;cursor: pointer" src="images/pre.png" onclick="showGraph(2);" width="50" height="50"  ></div>
    <div width="120" height="150" id="div_g" style="width:80%; height:520px;float:left;"></div>
    <div style=" height:60%;"><img class="img1" style="cursor: pointer;float:right;margin-right:3%" width="50" height="50" src="images/next.png" onclick="showGraph(1);"></div>
    <div style="margin-top:35px;float: right;" id="page">
        <div id="pagelist" style="float: left;margin-top: 5px">Pages 1 of 100 </div> &nbsp;||  Page <input style="display: inline !important;width:35px;height:25px;" type="text" name="txtpage" id="txtpage" > <input type="button" class="myButton"  name="btnpage" value="Go" id="btnpage" onclick="showGraph(0)" > 
    </div>
    </div>

    <%
       
    URL url=null,urlcount=null;
    SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");
    try{
    if(request.getParameter("rad")==null)
    {    if(!firstdevice.equals("")&&!firstdeviceComplete.equals(""))
        {
            strTemp="[]";
            Calendar timenow= Calendar.getInstance();
              Date toDate=timenow.getTime();
                timenow.add(Calendar.DATE, -7);
                Date fromDate=timenow.getTime();
                
            Calendar now = Calendar.getInstance();
                 now.add(Calendar.DATE, 1);
                Date dateto= now.getTime();
                 now.add(Calendar.DATE, -7);
                 Date datefrom=now.getTime();
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydatabydate?sensortype="+request.getParameter("cmb")+"&fromdate="+format.format(datefrom) +"&todate="+ format.format( dateto));
              //  out.print(request.getParameter("cmb")+dateto+"**"+datefrom);
                   //url = new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/historydatabydate?sensortype=" + request.getParameter("cmb") + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString());
                   url = new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/offsethistory?sensortype=" + firstdeviceComplete + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString()+"&offset=0");
                   %>
                   <script>
                       from='<%=format.format(datefrom).toString()%>';
                       to='<%=format.format(dateto).toString()%>';
                     
                       offset=0;
                       
                       toDate='<%=format.format(toDate).toString()%>';
                       fromDate='<%=format.format(fromDate).toString()%>';
                    </script>
                   <%
                   urlcount=new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/countrow?sensortype="+ firstdeviceComplete + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString());
            %>

            <%
            strTemp=url.toString();
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            int b2 = 0;
            strTemp = br.readLine();

            if(strTemp.trim()=="[]")
            {

            }

            strCount=urlcount.toString();
            BufferedReader br1 = new BufferedReader(new InputStreamReader(urlcount.openStream()));

            strCount = br1.readLine();
            System.out.println("url1 "+url.toString());
            System.out.println("url1 "+urlcount.toString());
        }
    }
    else
    {
        if(!firstdevice.equals("")&&!firstdeviceComplete.equals(""))
        if(request.getParameter("rad").equals("weekly"))
        {
            //   url="";
            
              Calendar timenow= Calendar.getInstance();
              Date toDate=timenow.getTime();
                timenow.add(Calendar.DATE, -7);
                Date fromDate=timenow.getTime();
                
            Calendar now = Calendar.getInstance();
             now.add(Calendar.DATE, 1);
            Date dateto= now.getTime();
             now.add(Calendar.DATE, -7);
             Date datefrom=now.getTime();
               url = new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/offsethistory?sensortype=" + request.getParameter("cmb") + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString()+"&offset=0");
               %>
               <script>
                   from='<%=format.format(datefrom).toString()%>';
                   to='<%=format.format(dateto).toString()%>';
                
                   offset=0;
                    toDate='<%=format.format(toDate).toString()%>';
                       fromDate='<%=format.format(fromDate).toString()%>';
                </script>
               <%
               urlcount=new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/countrow?sensortype="+ request.getParameter("cmb") + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString());
	}
        else if(request.getParameter("rad").equals("Monthly"))
        {
            
            Calendar timenow= Calendar.getInstance();
              Date toDate=timenow.getTime();
                timenow.add(Calendar.MONTH, -1);
                Date fromDate=timenow.getTime();
                
                
               Calendar now = Calendar.getInstance();
                now.add(Calendar.DATE, 1);
            Date dateto= now.getTime();
             now.add(Calendar.MONTH, -1);
             Date datefrom=now.getTime();
             url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/offsethistory?sensortype="+request.getParameter("cmb")+"&fromdate="+ format.format( datefrom)+"&todate="+format.format( dateto)+"&offset=0");
             %>
               <script>
                   from='<%=format.format(datefrom).toString()%>';
                   to='<%=format.format(dateto).toString()%>';
                   
                   offset=0;
                   
                   toDate='<%=format.format(toDate).toString()%>';
                       fromDate='<%=format.format(fromDate).toString()%>';
               </script>
               <%
               urlcount=new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/countrow?sensortype="+ request.getParameter("cmb") + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString());
               System.out.println("err  "+urlcount.toString());
        }
        
        else
        {
     //URL 
        
           
              String dt = request.getParameter("to");
     Calendar c = Calendar.getInstance();
     c.setTime(format.parse(dt));
     c.add(Calendar.DATE, 1);  // number of days to add
     dt = format.format(c.getTime());  // dt is now the new date

            //url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydatabydate?sensortype="+request.getParameter("cmb")+"&fromdate="+request.getParameter("from")+"&todate="+request.getParameter("to"));
            url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/offsethistory?sensortype="+request.getParameter("cmb")+"&fromdate="+request.getParameter("from")+"&todate="+dt+"&offset=0");
            System.out.println("err  "+url.toString());
            //out.print(url.toString());
%>
               <script>
                   from='<%=request.getParameter("from")%>';
                   to='<%=request.getParameter("to")%>';
                   
                   offset=0; 
                   dispDate()
                   
                   toDate='<%=request.getParameter("to")%>';
                       fromDate='<%=request.getParameter("from")%>';
               </script>
               <%
            urlcount=new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/countrow?sensortype="+ request.getParameter("cmb") + "&fromdate="+request.getParameter("from")+"&todate="+request.getParameter("to"));
            System.out.println("err  "+urlcount.toString());
        }
        
        
        strTemp=url.toString();
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
        int b2 = 0;
        strTemp = br.readLine();
        
        if(strTemp.trim()=="[]")
        {
          
        }
    
        strCount=urlcount.toString();
        BufferedReader br1 = new BufferedReader(new InputStreamReader(urlcount.openStream()));
        
        strCount = br1.readLine();
        
         //   Double b1 = Double.parseDouble(strTemp);
         //  b2 = b1.intValue();
        
        //out.print(strTemp);
        //System.out.println("jjj"+strTemp);
    
    // String strTemp = "";
   
        }
    }
    catch(Exception e)
            {
                strTemp="[]";
            }
        }
        else
        {
        %>
        <script>window.location='index.jsp';</script>
        <%
        }
              
        %>
    <script>  
var liveData;
      var data=[];
       var t = new Date();
       
 var data11='<%=strTemp %>';
 
 count='<%=strCount%>';
 $("#pagelist").html("Pages 1 of "+(parseInt(count/1000)==0?"1":parseInt(count/1000)));
 if((parseInt(count/1000)==0))
              {
                  $("#btnpage").css("display","none !important");
                  $("#txtpage").css("display","none !important");
              }

 <%
        NodeList xlabel=null;
            NodeList ylabel=null;
        NodeList gmin=null;
        NodeList gmax=null;
        String sen="";
        try{
            
     if(request.getParameter("cmb")==null)
     {    if(!firstdeviceComplete.equals(""))
         {
             sen=firstdeviceComplete;
         }
     }
     else 
         sen=request.getParameter("cmb");
    %> 
	 livedata('<%=sen%>',data11);
         historygrah=g;
         historydata=data;
	<%
           DocumentBuilderFactory dbf2 = DocumentBuilderFactory.newInstance();
        DocumentBuilder db2 = dbf2.newDocumentBuilder();
        Document doc2 = db2.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+session.getAttribute("userid")+"/"+sen+"/getSensorListByName");
        System.out.println("eeee "+"http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+session.getAttribute("userid")+"/"+sen+"/getSensorListByName");
         xlabel =  doc2.getElementsByTagName("xlabel");
         ylabel = doc2.getElementsByTagName("ylabel");
         gmin = doc2.getElementsByTagName("gmin");
         gmax = doc2.getElementsByTagName("gmax");
        }
        catch(Exception e)
        {}
        %>
            
            
            
             xlabels= '<%if(!sen.equals(""))out.print(xlabel.item(0).getFirstChild().getNodeValue());%>';
                            ylabels= '<%if(!sen.equals(""))out.print(ylabel.item(0).getFirstChild().getNodeValue());%>';
                            valueRanges= [<%if(!sen.equals(""))out.print(gmin.item(0).getFirstChild().getNodeValue());%>,<%if(!sen.equals(""))out.println(gmax.item(0).getFirstChild().getNodeValue());%>];
                            
                            titles='<% if(!sen.equals(""))
                                             out.print(sen.substring(sen.lastIndexOf("_")+1));%>';
                                                 
                                                      if(titles=="rumencontraction")
        {
           titles="Rumen Contraction";
        }else if(titles=="rumentemperature")
        {
            titles="Rumen Temperature";
        }else if(titles=="gatewayhumidity")
        {
            titles="Gateway Humidity";
        }else if(titles=="gatewaytemperature")
        {
            titles="Gateway Temperature";
        }
            
      smoothPlotter.smoothing =.7;
      //alert(data11);
       if(data11.trim().length!=2)
       {
var g = new Dygraph(
          document.getElementById("div_g"),
          data,
          {
            
                            showRoller: true,
                            xlabel: xlabels,
                            ylabel: ylabels,
                            valueRange: valueRanges,
                            labels: ['Time', 'Value'],
                            title:titles,
            showRangeSelector: true,
             stackedGraph: true,
              color:'#337AB7', 
            rangeSelectorHeight: 30,
            rangeSelectorPlotStrokeColor: 'green',
            rangeSelectorPlotFillColor: 'blue',
            plotter: smoothPlotter,
             strokeWidth: 2
          }
      );
       } else
      {
          { $("#div_g").html("No data Available");
              $("#graph").html("<div class='loaddiv'>No data available</div>");
              
          }

      }
    
           function livedata(id,data11)
{
    smoothPlotter.smoothing =.7;
 
   data11=data11.trim();
   if(data11.length!=2){
       if(id.indexOf("rumencontraction")>-1){

        var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       if($("#div_g").html()=="No data Available")data=[];
      
       var dates=[];
       var vals=[];
           data=[];
       var lastDate;
        for(var i=0;i<dt.length;i++)
        {
           
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
            dts=dates[i].split("T");
            dts1=dts[0].split("-");
            dts2=dts[1].split("Z");
            dts3=dts2[0].split(":")
       
            data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0), parseFloat(vals[i])]);
            lastDate=new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0);
        }
        
          titles=id.split("_")[1];

           {  
	                      if(titles=="rumencontraction")
        {
           titles="Rumen Contraction";
        }else if(titles=="rumentemperature")
        {
            titles="Rumen Temperature";
        }else if(titles=="gatewayhumidity")
        {
            titles="Gateway Humidity";
        }else if(titles=="gatewaytemperature")
        {
            titles="Gateway Temperature";
        }
	
               g = new Dygraph(document.getElementById("div_g"), data,
                          {
                            drawPoints: false,
                            showRoller: true,
                           xlabel: xlabels,
                            ylabel: ylabels,
                             stackedGraph: true,
                            title:titles,
                               showRangeSelector: true,
            rangeSelectorHeight: 30,
            rangeSelectorPlotStrokeColor: 'green',
            rangeSelectorPlotFillColor: 'blue',
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			     plotter: smoothPlotter,
                        strokeWidth: 2
                          });
                          historyoption={
                            drawPoints: false,
                            showRoller: true,
                           xlabel: xlabels,
                            ylabel: ylabels,
                             stackedGraph: true,
                            title:titles,
                               showRangeSelector: true,
            rangeSelectorHeight: 30,
            rangeSelectorPlotStrokeColor: 'green',
            rangeSelectorPlotFillColor: 'blue',
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			     plotter: smoothPlotter,
                        strokeWidth: 2
                          };
                          g.ready(function() {
                               var annotations = [];
                 for(var i=0;i<dt.length;i++)
                    {
                        

                        dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
                        vals[i]=dt[i].substr(dt[i].indexOf("counter=")+8);
                        dts=dates[i].split("T");
                        dts1=dts[0].split("-");
                        dts2=dts[1].split("Z");
                        dts3=dts2[0].split(":")

                      
                        annotations.push( 
                        {
                         series:"Value",
                          x: new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0).getTime(),
                          shortText:  parseFloat(vals[i]),
                          text: "Count :"+ parseFloat(vals[i])
                        });
                        
                    }
                     g.setAnnotations(annotations);
                    
                          });
                          }
                          
            }
            else
            { 
       
       
  
    data11=data11.trim();
      
        var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       var dates=[];
       var vals=[];
        for(var i=0;i<dt.length-2;i++)
        {
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
            dts=dates[i].split("T");
            dts1=dts[0].split("-");
            dts2=dts[1].split("Z");
            dts3=dts2[0].split(":");

            data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2]), parseFloat(vals[i])]);
       
        }
    titles=id.split("_")[1];
                   
                        if(titles=="rumencontraction")
        {
           titles="Rumen Contraction";
        }else if(titles=="rumentemperature")
        {
            titles="Rumen Temperature";
        }else if(titles=="gatewayhumidity")
        {
            titles="Gateway Humidity";
        }else if(titles=="gatewaytemperature")
        {
            titles="Gateway Temperature";
        }

                   g = new Dygraph(
          document.getElementById("div_g"),
          data,
          {
            
            drawPoints: true,
                            showRoller: true,
                            xlabel: xlabels,
                            ylabel: ylabels,
                            valueRange: valueRanges,
                            labels: ['Time', 'Value'],
                            title:titles,
                             stackedGraph: true,
            showRangeSelector: true,
            rangeSelectorHeight: 30,
            rangeSelectorPlotStrokeColor: 'green',
            rangeSelectorPlotFillColor: 'blue',
	       plotter: smoothPlotter,
             strokeWidth: 2
          }
      );
      historyoption= {
            
            drawPoints: true,
                            showRoller: true,
                            xlabel: xlabels,
                            ylabel: ylabels,
                            valueRange: valueRanges,
                            labels: ['Time', 'Value'],
                            title:titles,
                             stackedGraph: true,
            showRangeSelector: true,
            rangeSelectorHeight: 30,
            rangeSelectorPlotStrokeColor: 'green',
            rangeSelectorPlotFillColor: 'blue',
	       plotter: smoothPlotter,
             strokeWidth: 2
          };
               
               
     
 }}
 else
 {
     $("#div_g").html("No data Available");
    // clearInterval(liveData);
 }
}

        
   
            </script>
            <style>
.dygraph-axis-label-x
{
 // width:130px;
 // height:70px;
 // margin-left: -10px;
 // -ms-transform:rotate(270deg); /* IE 9 */
 // -moz-transform:rotate(270deg); /* Firefox */
 // -webkit-transform:rotate(270deg); /* Safari and Chrome */
 // -o-transform:rotate(270deg); /* Opera */
}
.dygraph-xlabel{
    margin-top: 30px;
}
.ui-widget-content {
    z-index: 50 !important;
}

</style>
<script>
        $(function() {
             $( "#from" ).datepicker();
    $( "#from" ).datepicker("option", "dateFormat","dd-mm-yy");
    $("#from").val(fromDate);
  });
   $(function() {
       $( "#to" ).datepicker();
    $( "#to" ).datepicker("option", "dateFormat","dd-mm-yy");
    
    $("#to").val(toDate);
  });
    </script>
<%
if(request.getParameter("cmb")==null&&firstdeviceComplete.equals(""))
{
    %>
    <script>$("#div_g").html("");</script>
    <%
}
    
%>
      </div>
   
                        </div> 
                    </div>
                </div>



            </div>
        </div>

        <%       if (!firstDevice.equals("")) {
        %>
        <script>
            optionSelect('<%=firstDevice%>');
        </script>

        <%
            }

            try {
                String[] values = request.getParameterValues("val");
                if (values != null) {
                                                                if (values[0].equals("0")) {%>
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.success("Alarm cleared..!");
                //alert("haiii");

            }

        </script>
        <%
        } else if (values[0].equals("1")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.alert("Can not clear alarm..!");

            }
        </script>
        <%
        } else if (values[0].equals("2")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.success("Password changed successfully..!");
            }
        </script>
        <%
        } else if (values[0].equals("3")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.alert("Old password is incorrect..!");
            }
        </script>
        <%
            }
        %>   
        <script>

        </script> 
        <%
                }
            } catch (Exception ex) {

            }
        %>



        <script src="js/bootstrap.min.js"></script>  
        <script src="js/dialog.js"></script> 
        <br>
        <script>
            if(mnc==0)
    {
	NProgress.set(1);
    }
	mnc=1;
            
        </script>
    </body>

    <%
            if(request.getParameter("rad")!=null)
            {
                %>
                <script>
                    showTab(1,3);
                </script>
                <%
            }
                }
            } else {
                response.sendRedirect("index.jsp");
            }
        } catch (Exception ex) {

        }

    %>
    <!-- END BODY -->
    <style>
        input{display:none !important;}
        input[ type="checkbox"]{display:inline !important;}
        input[ type="radio"]{display:inline !important;}
        input[ type="button"]{display:inline !important;}
        input[ type="submit"]{display:inline !important;}
        .cls{display:inline !important;}
    </style>
    <style>
        
    </style>

</html>