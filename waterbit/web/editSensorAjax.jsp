  <%@page import="org.w3c.dom.Node"%>
    <%@page import="org.w3c.dom.NodeList"%>
    <%@page import="org.w3c.dom.Document"%>
    <%@page import="javax.xml.parsers.DocumentBuilder"%>
    <%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
    <%@page import="java.net.URLConnection"%>
    <%@page import="java.io.InputStreamReader"%>
    <%@page import="java.net.URL"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="org.gadgeon.Config"%>  
<div id="result1">
    <script>
        function validatetxtbox()
            {
               
                var sinfor = document.getElementById("sinfor").value;
                
                var maxTemp = document.getElementById("maxTemp").value;
                var minTemp = document.getElementById("minTemp").value;

            

                else if(minTemp == "")
                {
                    document.getElementById('une').innerHTML = "* Please enter minimum threshold value";
                    document.getElementById("city").focus();
                    return(false);
                }
                else if(maxTemp == "")
                {
                    document.getElementById('une').innerHTML = "* Please enter maximum threshold value";
                    document.getElementById("city").focus();
                    return(false);
                }
                else if (maxTemp < minTemp)
                {

                    document.getElementById('une').innerHTML = "* Maximum Threshold value should be grater than minimum threshold value";
                    document.getElementById("minTemp").focus();
                    return(false);
                }
               
                else
                {

                    return(true);
                }


            }
    </script>
    
    <%
        String sensor = request.getParameter("sensor");
        int userid = (Integer) session.getAttribute("userid");

        URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/" + sensor + "/getSensorListByName");
        URLConnection urlcon = jsonpage.openConnection();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/" + sensor + "/getSensorListByName");

        NodeList sensorname = doc.getElementsByTagName("sensorname");
        NodeList sensortype = doc.getElementsByTagName("sensortype");
        NodeList sensorlocation = doc.getElementsByTagName("sensorinfo");
        NodeList sensorinfo = doc.getElementsByTagName("sensorlocation");
        NodeList maxT = doc.getElementsByTagName("max");
        NodeList minT = doc.getElementsByTagName("min");
        NodeList graphprofiles = doc.getElementsByTagName("graphprofile");
        NodeList xLabel = doc.getElementsByTagName("xlabel");
        NodeList yLabel = doc.getElementsByTagName("ylabel");
        NodeList gmin = doc.getElementsByTagName("gmin");
        NodeList gmax = doc.getElementsByTagName("gmax");
         NodeList latitude = doc.getElementsByTagName("latitude");
        NodeList longitude = doc.getElementsByTagName("longitude");
        String graphprofile = graphprofiles.item(0).getFirstChild().getNodeValue();

 
        jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype.item(0).getFirstChild().getNodeValue() + "/GetProfileByType");
        urlcon = jsonpage.openConnection();

        dbf = DocumentBuilderFactory.newInstance();

        db = dbf.newDocumentBuilder();

        doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype.item(0).getFirstChild().getNodeValue() + "/GetProfileByType");

        NodeList profileName = doc.getElementsByTagName("graphprofile");
        
        
        System.out.println("1   http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/" + sensor + "/getSensorListByName");
        System.out.println("2   http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/" + sensor + "/getSensorListByName");
        System.out.println("3   http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype.item(0).getFirstChild().getNodeValue() + "/GetProfileByType");
        System.out.println("minTemp4   http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype.item(0).getFirstChild().getNodeValue() + "/GetProfileByType");
    %>
  
    <div class="col-lg-6"> 

        <div class="panel panel-primary" style="background-color: #dff0d8; height:545px;    ">
            <div class="panel-heading">
                Update Sensor
            </div>
            <form action="editSensorAction.jsp" method="post"  onsubmit="return(validatetxtbox())">
                <div class="panel-body">
                    <div class="table-responsive"><table class="table">
                            <tbody>
                                
                                <tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Sensor Name </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="sName" id="sName" disabled="true" value="<%=sensorname.item(0).getFirstChild().getNodeValue()%>"/>
                                    <input type="hidden" id="sensorid" name="sensorid" value="<%=sensorname.item(0).getFirstChild().getNodeValue()%>"/>
                                    </td>
                                </tr>
                            
                                <tr class="success">
                                    <!-- <td>Information</td><td> <input type="text" name="sinfor" id="sinfor"/></td>-->
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Information</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <textarea name="sinfor" id="sinfor" cols="30" rows="4" ><%=sensorinfo.item(0).getFirstChild().getNodeValue().trim()%></textarea>  </td>

                                </tr>

                                <tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Sensor Type </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="prop" id="prop" disabled="true" >

                                            <option value="<%=sensortype.item(0).getFirstChild().getNodeValue()%>"
                                                    selected><%=sensortype.item(0).getFirstChild().getNodeValue()%>
                                            </option>


                                        </select></td>

                                </tr>
                                <tr class="success">
                                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Graph Profile</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="graphprofile" id="graphprofile" onchange="graphProfileByType();">
                                                                <%                                                 for (int i = 0; i <= profileName.getLength() - 1; i++) {

                                                                    %>
                                                                    <option value="<%=profileName.item(i).getFirstChild().getNodeValue()%>"
                                                                            <%=graphprofile.equals(profileName.item(i).getFirstChild().getNodeValue())?" selected ":" "%>   > <%=profileName.item(i).getFirstChild().getNodeValue()%>
                                                                    </option>

                                                                    <%
                                                                        }
                                                                    %>
                                        
                                                        </select></td>

                                                </tr>

                                <tr class="success" hidden="true">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;"> Pin Code or Zip Code</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type="text" id="txtAddress" name="txtAddress"/>
                                        <input type="hidden" id="sLatitude" name="sLatitude" value="<%=latitude.item(0).getFirstChild().getNodeValue()%>"/>
                                        <input type="hidden" id="sLongitude" name="sLongitude" value="<%=longitude.item(0).getFirstChild().getNodeValue()%>"/>
                                        <input type="button" onclick="GetLocation();" value="Get Location"  class="myButton"/></td>
                                </tr>
                                <tr class="success">
                                    <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Location</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="city" id="city" disabled="true" value="<%=sensorlocation.item(0).getFirstChild().getNodeValue()%>"/>
                                        <input type="hidden" name="city1" id="city1" value="<%=sensorlocation.item(0).getFirstChild().getNodeValue()%>"></td>
                                </tr>
                            </tr>
                            <% if(sensortype.item(0).getFirstChild().getNodeValue().compareTo("relay")!=0 && sensortype.item(0).getFirstChild().getNodeValue().compareTo("ap")!=0)
                                  {
                              %>
                            
                            <tr class="success">
                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Minimum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="minTemp" id="minTemp" onkeypress="return isNumber(event)" value="<%=minT.item(0).getFirstChild().getNodeValue()%>"/></td>
                        </tr>
                        <tr class="success">
                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Maximum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="maxTemp" id="maxTemp" onkeypress="return isNumber(event)" value="<%=maxT.item(0).getFirstChild().getNodeValue()%>"/></td>
<% } else if(sensortype.item(0).getFirstChild().getNodeValue().compareTo("ap")==0){
                              %>
                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Minimum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="minTemp" id="minTemp" onkeypress="return isNumber(event)" value="0" disabled="true"/></td>
                        </tr>
                        <tr class="success">
                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Maximum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="maxTemp" id="maxTemp" onkeypress="return isNumber(event)" value="0" disabled="true"/></td>
                            <% } else{
                              %>
                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Minimum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="minTemp" id="minTemp" onkeypress="return isNumber(event)" value="<%=minT.item(0).getFirstChild().getNodeValue()%>" disabled="true"/></td>
                        </tr>
                        <tr class="success">
                            <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Maximum Threshold value</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="maxTemp" id="maxTemp" onkeypress="return isNumber(event)" value="<%=maxT.item(0).getFirstChild().getNodeValue()%>" disabled="true"/></td>
                            <% } 
                              %>
                        </tr>
                     <tr class="success" style="border-top: none">
                                                <td style="border-top-color: #599bb3; border-style: dashed; border-width: 1px; border: none"></td>
                                                <td style="border-top-color: #599bb3; border-style: dashed; border-width: 1px; border: none"><input type="submit" value="Update" class="myButton" onclick="return(function1());"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="Clear" onclick="return(OnCancel())" class="myButton"/></td>
                                            </tr>
                                            </tbody>
                                            <font color='red'> <div id="une"> </div> </font>
                                        </table>
                                        
            </div>


        </div>
        
			       
                </table>
            </div>

    </div>
        </div>
			        </form>

  
</div>

</div>