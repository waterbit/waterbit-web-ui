<%-- 
    Document   : responsiveDashBoard
    Created on : 9 Jan, 2015, 12:00:40 PM
    Author     : nisha
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gadgeon IOT </title>


        <link rel="stylesheet" href="css/main.css" >
        <link rel="stylesheet" href="css/bstrap.css" >
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" >
        <link rel="stylesheet" href="css/dialog.css" >
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
        <style>
            .sensoredit{

            }
            .sensoredit:hover {
                cursor:pointer;
            }


        </style>

        <style>
            .mysensor{


            }
            .myevent{


            }
            .mysensor:hover {
                cursor:pointer;
            }
            .myevent:hover {
                cursor:pointer;
            }

        </style>
        <style>
            .myButton {
                -moz-box-shadow: 0px 10px 14px -7px #276873;
                -webkit-box-shadow: 0px 10px 14px -7px #276873;
                box-shadow: 0px 10px 14px -7px #276873;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
                background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
                background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
                background-color:#599bb3;
                -moz-border-radius:3px;
                -webkit-border-radius:3px;
                border-radius:4px;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-family:Times New Roman;
                font-size:10px;
                font-weight:bold;
                padding:2px 10px;
                text-decoration:none;
                text-shadow:0px 1px 0px #3d768a;
            }
            .myButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
                background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
                background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
                background-color:#408c99;
            }
            .myButton:active {
                position:relative;
                top:1px;
            }

        </style>

        <% try {
                if (session.getAttribute("userid") != null) {

                    int userid = (Integer) session.getAttribute("userid");
                    if (userid == 1) {
        %> 
        <script src="js/alertify.min.js"></script>
        <script type="text/javascript">

            var liveDataInt;
            var checkflag = "false";
            setInterval(function () {
                // alert("haii");
                $.get("getsensorlist.jsp", function (data11, status) {

                    // alert(data11.trim()); 
                    var cnt = document.getElementsByName("rbox[]");
                    var img = document.getElementsByName("im");
                    var iconim = document.getElementsByName("icon");
                    var uname;
                    var sensorstatus = data11.trim().split("*");
                    var table = document.getElementById("mytab1");

                    var x = table.rows.length - 1;
                    var y = sensorstatus.length - 1;
                    //  alert(x);
                    if (y > x)
                    {
                        //  alert("###");

                        setTimeout(function () {
                            window.location.reload();

                        }, 1000);
                        alertify.success("New Sensor Found");
                    }
                    else if (y < x)
                    {

                        setTimeout(function () {
                            window.location.reload();

                        }, 1000);

                        alertify.error("Sensor Deleted");
                    }
                    else
                    {
                        for (var i = 0; i < sensorstatus.length - 1; i++)
                        {

                            var senname = sensorstatus[i].substring(0, sensorstatus[i].indexOf(":"));
                            var sentype = sensorstatus[i].substring(sensorstatus[i].indexOf(":") + 1, sensorstatus[i].indexOf(";"));
                            var seninfo = sensorstatus[i].substring(sensorstatus[i].indexOf(";") + 1, sensorstatus[i].indexOf("<"));
                            var status = sensorstatus[i].substring(sensorstatus[i].indexOf("<") + 1, sensorstatus[i].indexOf(">"));
                            var senalarmcnt = sensorstatus[i].substring(sensorstatus[i].indexOf(">") + 1, sensorstatus[i].indexOf("#"));
                            var alarmstatus = sensorstatus[i].substring(sensorstatus[i].indexOf("#") + 1, sensorstatus[i].indexOf("$"));
                            var lastuptime = sensorstatus[i].substring(sensorstatus[i].indexOf("$") + 1);
                            var table = document.getElementById("mytab1");
                            var name = senname.split("_");
                            row = table.rows[i + 1];
                            row.cells[1].innerHTML = name[0];
                            row.cells[3].innerHTML = seninfo;
                            row.cells[4].innerHTML = senalarmcnt;
                            row.cells[5].innerHTML = alarmstatus;
                            var table1 = document.getElementById("mytab2");
                            row1 = table1.rows[i + 1];
                            row1.cells[0].innerHTML = name[0];

                            row1.cells[2].innerHTML = seninfo;
                            row1.cells[4].innerHTML = lastuptime;


                            for (var j = 0; j < img.length; j++)
                            {
                                //alert(img[j].alt);

                                if (img[j].alt.trim() == senname.trim())
                                {
                                    if (status.trim() == "0")
                                    {
                                        img[j].src = "images/red_offline_icon.png";
                                    }
                                    else
                                    {
                                        img[j].src = "images/live1.png";
                                        break;
                                    }
                                }
                            }

                        }
                    }

                });
            }, 4000);

            function check(field)
            {

                if (checkflag == "false")
                {
                    if (isNaN(document.myform.elements["rbox[]"].length))
                    {
                        document.myform.elements["rbox[]"].checked = true;
                        checkflag = "true";

                    }
                    else
                    {
                        for (i = 0; i < field.length; i++)
                        {
                            field[i].checked = true;
                        }
                        checkflag = "true";
                    }
                    rowSelecteventAll("#48D1CC");

                    return "Clear All";
                }
                else
                {
                    if (isNaN(document.myform.elements["rbox[]"].length))
                    {
                        document.myform.elements["rbox[]"].checked = false;
                        checkflag = "false";

                    }
                    else
                    {
                        for (i = 0; i < field.length; i++)
                        {
                            field[i].checked = false;
                        }
                        checkflag = "false";
                    }
                    rowSelecteventAll("#dff0d8");
                    return "Select All";

                }
            }

            function OnButton1()
            {

                var count = 0;
                // alert(document.myform.elements["rbox[]"].length);
                if (isNaN(document.myform.elements["rbox[]"].length))
                {
                    //  uid = document.myform.elements["rbox[]"].value;
                    count = 1;
                }
                else
                {
                    for (i = 0; i < document.myform.elements["rbox[]"].length; i++)
                    {
                        if (document.myform.elements["rbox[]"][i].checked == true)
                        {

                            count++;
                        }
                    }
                }
                if (count >= 1)
                {
                    document.myform.action = "userClearAlarm.jsp";

                    // document.Form1.target = "_blank";    // Open in a new window

                    document.myform.submit();             // Submit the page

                    return true;
                }
                else if (count < 1)
                {
                    alertify.alert("Please select atleast one sensor");
                    return false;
                }
            }
            function rewrite(txt)
            {

                clearInterval(liveDataInt);
                // data=[];
                $.get("graphdetail.jsp?sensor=" + txt, function (data12, status) {
                    data12 = data12.trim().split(":");

                    yval = data12[0];
                    xval = data12[1];
                    min = data12[2];
                    max = data12[3];
                });
                $.get("livedatademo.jsp?sensor=" + txt, function (data11, status) {
                    senval = txt.substring(0, txt.lastIndexOf("_"));
                    document.getElementById("sensorlabel").innerHTML = senval;
                    // alert(data11); 
                    livedata(3, data11);

                });
                liveDataInt = setInterval(function () {
                    $.get("livedatademo.jsp?sensor=" + txt, function (data11, status) {
                        senval = txt.substring(0, txt.lastIndexOf("_"));
                        document.getElementById("sensorlabel").innerHTML = senval;
                        // alert(data11); 
                        livedata(3, data11);

                    });
                    callfunc(yval, xval, min, max);

                    //alert("haiii"+yval);


                }, 3000
                        );
                // alert("!!!"+txt);

                //alert(txt);0a1b3c4d5e6f
            }


            /*  $('tr').click(function (event) {
             
             if (!$('input[type=radio]', this).prop('checked'))
             $('input[type=radio]', this).prop('checked', true);
             else
             $('input[type=radio]', this).prop('checked', false);
             });*/
            function rowSelect(row, sensor)
            {
                //row.className="";

                var tr = document.getElementsByClassName("mysensor");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = "#dff0d8";

                }
                row.style.background = "#48D1CC";
                // var str=row.cells[0].innerHTML.trim();
                //    row.cells[0].innerHTML=str.substr(0,str.length-1)+" checked >"
                //alert(str.substr(0,str.length-1));
                rewrite(sensor);
            }
            function rowSelectevent(row, sensor)
            {
                //row.className="";

                var tr = document.getElementsByClassName("myevent");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = "#dff0d8";

                }
                row.style.background = "#48D1CC";
                var str = row.cells[0].innerHTML.trim();
                row.cells[0].innerHTML = str.substr(0, str.length - 1) + " checked >"
                //alert(str.substr(0,str.length-1));
                //rewrite(sensor);
            }
            function rowSelecteventAll(color)
            {
                //row.className="";
                var tr = document.getElementsByClassName("myevent");
                for (var i = 0; i < tr.length; i++)
                {
                    tr[i].style.background = color;
                    var str = tr[i].cells[0].innerHTML.trim();
                    tr[i].cells[0].innerHTML = str.substr(0, str.length - 1) + " checked >"
                }
                //row.style.background ="#48D1CC";
                //var str=row.cells[0].innerHTML.trim();
                //row.cells[0].innerHTML=str.substr(0,str.length-1)+" checked >"
                //alert(str.substr(0,str.length-1));
                //rewrite(sensor);
            }


        </script>

    </head>
    <body>
        <%
            String currentSensor = "", currentSensorType = "";%>

        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>  
        </div>

        <div class="container">  


            <%@include file="./adminHeader.jsp" %>



            <div class="row">


                <div class="col-lg-6" style="height: 900px;">
                    <div class="panel panel-primary" style="height: 890px; background-color: #dff0d8">
                        <div class="panel-heading">My Sensors</div>
                        <div class="panel-body">
                            <form name="form1" method="post">
                                <div class="table-responsive" style="height: 830px;">
                                    <table class="table table-hover" id="mytab2" >
                                        <thead>
                                            <tr style="background-color: #BFEDAC;">
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px;text-align: center">Sensor Name</th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px;text-align: center">Type</th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px;text-align: center">Location</th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px; text-align: center">Status</th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px;text-align: center">Last Up Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                            <%
                                                try {

                                                    URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorList");
                                                    URLConnection urlcon = jsonpage.openConnection();

                                                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

                                                    DocumentBuilder db = dbf.newDocumentBuilder();

                                                    Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorList");
                                                    NodeList sensorname = doc.getElementsByTagName("sensorname");
                                                    NodeList sensortype = doc.getElementsByTagName("sensortype");
                                                    NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
                                                    NodeList sensorstatus = doc.getElementsByTagName("status");
                                                    NodeList lastuptime = doc.getElementsByTagName("lastuptime");

                                                    //    out.println(sensorstatus);
                                                    // NodeList alarmcount = doc.getElementsByTagName("alarmcount");
                                                    int j = 0;

                                                    for (int i = 0; i <= sensorname.getLength() - 1; i++) {
                                                        if (!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("Relay")) {
                                                            if (currentSensor == "") {
                                                                currentSensor = sensorname.item(i).getFirstChild().getNodeValue();
                                                                currentSensorType = sensortype.item(i).getFirstChild().getNodeValue();
                                                                // currentSensor="gadgeon_temperature_sensor4";
                                                            }

                                            %>       
                                            <% String senval = sensorname.item(i).getFirstChild().getNodeValue();%>
                                            <tr style="background-color:<%=(j++ == 0) ? "#48D1CC" : "#dff0d8"%>" class="mysensor" onclick="rowSelect(this, '<%=sensorname.item(i).getFirstChild().getNodeValue()%>')"  >

                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%
                                                    senval = senval.substring(0, senval.lastIndexOf("_"));
                                                    out.print(senval);
                                                    %></td>
                                                    <%
                                                        if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("Temperature")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img name="icon" src="images/thermo_online1.png" alt="TempSensor" style="width: 10px">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("Relay")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img  name="icon" src="images/switch-turn-off-icon.png" alt="RelaySensor" style="width: 15px;">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("pressure")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img  name="icon" src="images/pressure.png" alt="PressureSensor" style="width: 15px;">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("analog")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;" class="cls"><img  name="icon" src="images/analog.png" alt="AnalogSensor" style="width: 15px;">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("humidity")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img  name="icon" src="images/humidity.png" alt="HumiditySensor" style="width: 15px;">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("light")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img  name="icon" src="images/light.png" alt="LightSensor" style="width: 15px;">
                                                    <%
                                                    } else {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img  name="icon" src="images/other.png" alt="OtherSensor" style="width: 15px;">
                                                    <%
                                                        }
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%=sensorinfo.item(i).getFirstChild().getNodeValue()%></td>

                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center;">
                                                    <% //String val=sensorstatus.item(i).getFirstChild().getNodeValue();
                                                        //  String val="1";
                                                        if (sensorstatus.item(i).getFirstChild().getNodeValue().trim().equalsIgnoreCase("1")) //      if(val.equals("0"))
                                                        {%>
                                                    <img name="im" src="images/live1.png" alt="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" name="" style="width: 15px;">
                                                    <%
                                                    } else {
                                                    %>
                                                    <img name="im" src="images/red_offline_icon.png" alt="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" name="" style="width: 15px;">
                                                    <%
                                                        }
                                                    %>
                                                </td>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">
                                                    <%
                                                        if (lastuptime.item(i).getChildNodes().getLength() != 0) {
                                                    %>

                                                    <%=lastuptime.item(i).getFirstChild().getNodeValue()%>
                                                    <% } else {
                                                    %>
                                                    <%out.print("");%>
                                                    <%}
                                                    %>

                                                </td>        
                                            </tr>
                                            <%
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                }


                                            %> 
                                        </tbody>
                                    </table>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading"  style="font-size: 15">Live Data:
                                <label id="sensorlabel" style="font-size: 15"><% String senval1 = currentSensor.substring(0, currentSensor.lastIndexOf("_"));
                                out.print(senval1);%></label></div>
                        <div class="panel-body">
                            <!--<div class="demo-container">-->
                            <%@ include file="newjsp2.jsp" %>
                            <!--</div>-->
                        </div>
                    </div>
                    <form method="post" name="myform">
                        <div class="panel panel-danger" id="tables" style="height: 560px; background-color: #dff0d8">
                            <div class="panel-heading"><img src="images/bell.png" height="20" width="20"/>My Events


                                <INPUT type="button" class="myButton" id="btnClear" value="Clear Alarm" name="btnClear" onclick="OnButton1();" style="position:relative;float: right;margin-right: 0%;margin-bottom: 3%; ">
                                <INPUT type=button class="myButton" value="Select All" onClick="this.value = check(this.form)" style="position:relative;float:right;margin-right: 3%;margin-bottom: 3%; ">


                            </div>
                            <div class="panel-body">
                                <div class="table-responsive" style="height: 490px;">
                                    <table class="table table-hover" id="mytab1">
                                        <thead>
                                            <tr>
                                                <th style="border:none" hidden="true"></th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px; text-align: center; background-color: #BFEDAC;">Sensor Name</th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px; text-align: center; background-color: #BFEDAC;">Type</th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px; text-align: center; background-color: #BFEDAC;">Location</th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px; text-align: center; background-color: #BFEDAC;">Alarm Count</th>
                                                <th style="border: #599bb3; border-style: solid; border-width: 1px; text-align: center; background-color: #BFEDAC;">Alarm Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <%                                                 try {
                                                    URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorList");
                                                    //   URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + userid + "/getSensorListByUserId");
                                                    URLConnection urlcon = jsonpage.openConnection();

                                                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

                                                    DocumentBuilder db = dbf.newDocumentBuilder();

                                                    Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/getSensorList");
                                                    NodeList sensorname = doc.getElementsByTagName("sensorname");
                                                    NodeList sensortype = doc.getElementsByTagName("sensortype");
                                                    NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
                                                    NodeList alarmcount = doc.getElementsByTagName("alarmcount");
                                                    NodeList alarmstatus = doc.getElementsByTagName("alarmstatus");

                                                    for (int i = 0; i <= sensorname.getLength() - 1; i++) {
                                                        if (!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")) {

                                            %>                                                
                                            <tr style="background-color: #dff0d8;" class="myevent"  onclick="rowSelectevent(this, '<%=sensorname.item(i).getFirstChild().getNodeValue()%>')" >   

                                                <td style="border:none" hidden="true"><input type="checkbox" name="rbox[]" id="rbox[]" value="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" style="visibility:hidden;"></td>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><% String temsen = sensorname.item(i).getFirstChild().getNodeValue();
                                                    out.print(temsen.substring(0, temsen.lastIndexOf("_")));
                                                    %></td>
                                                   <!-- <td><%=sensortype.item(i).getFirstChild().getNodeValue()%></td>-->
                                                <%
                                                    if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("Temperature")) {
                                                %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img src="images/thermo_online1.png" alt="TempSensor" style="width: 10px">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("Relay")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img src="images/switch-turn-off-icon.png" alt="RelaySensor" style="width: 15px;">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("pressure")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;text-align: center" class="cls"><img name="icon" src="images/pressure.png" alt="PressureSensor" style="width: 15px;">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("analog")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><img src="images/analog.png" name="icon" alt="AnalogSensor" style="width: 15px;">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("humidity")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img src="images/humidity.png" name="icon" alt="HumiditySensor" style="width: 15px;">
                                                    <%
                                                    } else if (sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("light")) {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img src="images/light.png" alt="LightSensor" name="icon" style="width: 15px;">
                                                    <%
                                                    } else {
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px; text-align: center" class="cls"><img src="images/other.png" alt="OtherSensor" name="icon" style="width: 15px;">
                                                    <%
                                                        }
                                                    %>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><%=sensorinfo.item(i).getFirstChild().getNodeValue()%></td>
                                                <td id="alarmcnt" style="border: #599bb3; border-style: dashed; border-width: 1px;text-align: center;"><%=alarmcount.item(i).getFirstChild().getNodeValue()%></td>
                                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">


                                                    <%

                                                        if (alarmstatus.getLength() != 0) {
                                                    %>

                                                    <%=alarmstatus.item(i).getFirstChild().getNodeValue()%>
                                                    <% } else {
                                                    %>
                                                    <%out.print("");%>
                                                    <%}
                                                    %>
                                                </td>                                          
                                            </tr>
                                            <%
                                                        }
                                                    }
                                                } catch (Exception e) {

                                                }


                                            %> 
                                        </tbody>


                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <%                                            try {
                String[] values = request.getParameterValues("val");
                if (values != null) {
                    if (values[0].equals("0")) {%>
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.success("Alarm cleared..!");
            }

        </script>
        <%
        } else if (values[0].equals("1")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.alert("Can not clear alarm..!");
            }
        </script>
        <%
        } else if (values[0].equals("2")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.success("Password changed successfully..!");
            }
        </script>
        <%
        } else if (values[0].equals("3")) {%>  
        <script type="text/javascript">
            document.onreadystatechange = function () {//window.addEventListener('readystatechange',function(){...}); (for Netscape) and window.attachEvent('onreadystatechange',function(){...}); (for IE and Opera) also work
                if (document.readyState == 'loaded' || document.readyState == 'complete')
                    alertify.alert("Old password is incorrect..!");
            }
        </script>
        <%
            }
        %>   
        <script>

        </script> 
        <%
                }
            } catch (Exception ex) {

            }
        %>



        <script src="js/bootstrap.min.js"></script>  
        <script src="js/dialog.js"></script> 
    </body>

    <%
                }
            } else {
                response.sendRedirect("index.jsp");
            }
        } catch (Exception ex) {

        }

    %>
    <!-- END BODY -->

</html>