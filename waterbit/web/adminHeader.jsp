<%-- 
    Document   : adminHeader
    Created on : 14 Jan, 2015, 4:49:11 PM
    Author     : nisha
--%>

<link rel="stylesheet" href="css/main.css" >
<link rel="stylesheet" href="css/bstrap.css" >
<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	<style>
	     .dropdown:hover .dropdown-menu {
			display: block;
		}
	</style>
<div class="navbar navbar-default" role="navigation" style="background: none repeat scroll 0% 0% #103D5F; color: #FFF;">
    <div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	    <span class="sr-only">Toggle Navigation</span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" href="#" style="color: #FFF"></a>
    </div>
    <div class="navbar-collapse collapse">
	<ul class="nav navbar-nav">
	    <li class="active"><a href="dashBoard.jsp"><img src="img/icon-dashboard.png" width="22" height="22"/>Dashboard</a></li>
	    <li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="img/icon-form-style.png" width="22" height="22"/>Configuration<b class="caret"></b></a>
		<ul class="dropdown-menu" style="background: none repeat scroll 0% 0% #103D5F;">
		    <li><a href="addNewUser.jsp">Add New User</a> </li>
		    <li><a href="changePasswd.jsp">Change Password</a> </li>
		    <li><a href="liveControls.jsp">Live Controls</a> </li>
		</ul>
	    </li>
	    <li><a href="historyChart.jsp"><img src="img/icon-charts-graphs.png" width="22" height="22"/>History</a></li>
	</ul>
	    <ul class="nav navbar-nav navbar-right">
	    <li><a href="logout.jsp"><img src="img/img-profile.jpg" width="28" height="28"/>Logout</a></li>
	</ul>
    </div>
</div>
      
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>