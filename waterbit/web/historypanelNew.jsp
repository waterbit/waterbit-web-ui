
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <script>
      var from,to,offset,sensor,xlabels,ylabels,titles,valueRanges,count; 
      function dispDate()
      {
          // $("#dates").style.display="block";
           //alert(document.getElementById("dates").style.display);
           document.getElementById("dates").style.display="block";
      }
      function change()
      {
          // $("#dates").style.display="block";
           //alert(document.getElementById("dates").style.display);
           document.getElementById("dates").style.display="none";
      }
      function showGraph(id)
      {
          //$("#div_g").html("<img width=250 height=250 src='images/loading_1.gif' ></img>");
          if(id==0)
          offset=$("#txtpage").val();
          else if(id==1)
              offset++;
          else if(id==2)
              offset--;
          if(offset<0||offset>count)
          {
              alert('No data available');
              return;
          }
         //alert($("#txtpage").val());
         $.get("historyoffset.jsp?sensor="+sensor+"&from="+from+"&to="+to+"&offset="+(offset*1000)+"", function (data11, status) {
            // data=[];
             $("#div_g").html("");
             //alert(data11);
             livedata(3,data11);
              $("#pagelist").html("Pages "+offset+" of "+(parseInt(count/1000)==0)?"1":parseInt(count/1000));
              if((parseInt(count/1000)==0))
              {
                  $("#btnpage").css("display","none !important");
                  $("#txtpage").css("display","none !important");
              }
         });
         
      }
    </script>
    <style>
        input{display:none !important;}
        input[ type="checkbox"]{display:inline !important;}
        input[ type="radio"]{display:inline !important;}
        input[ type="button"]{display:inline !important;}
        input[ type="submit"]{display:inline !important;}
        .cls{display:inline !important;}
    </style>
                  <style>
    .myButton {
	-moz-box-shadow: 0px 10px 14px -7px #276873;
	-webkit-box-shadow: 0px 10px 14px -7px #276873;
	box-shadow: 0px 10px 14px -7px #276873;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #408c99));
	background:-moz-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-webkit-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-o-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:-ms-linear-gradient(top, #599bb3 5%, #408c99 100%);
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#408c99',GradientType=0);
	background-color:#599bb3;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:4px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Times New Roman;
	font-size:12px;
	font-weight:bold;
	padding:2px 10px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}
.myButton:active {
	position:relative;
	top:1px;
}
.square {
    background:#ffffff;
    width:50vw;
 //   height:10vw;
}
.square h1 {
    color: #599bb3;
}

  /*select {
  margin: 1px;
  border: 1px solid #111;
  background: transparent;
  width: 150px;
  padding: 3px 10px 5px 5px;
  font-size: 10px;
  border: 1px solid #ccc;
  height: 30px;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  background: url(http://www.stackoverflow.com/favicon.ico) 96% / 15% no-repeat #eee;
}*/
</style>
    <%
        String   strTemp,strCount;
        strTemp="";strCount="";
       // session.setAttribute("userid", 1);
        if (session.getAttribute("userid") != null) { 
         
//int userid = (Integer)session.getAttribute( "userid" );

%>
  <div class="square">
<form name="frm" method="post" >
     <select name="cmb"  id="cmbsensor" >
       
      <!--  <option value="gadgeon_temperature_sensor1">gadgeon_temperature_sensor1</option>
        <option value="gadgeon_temperature_sensor2">gadgeon_temperature_sensor2</option>-->
    <%
      String type="";
      URL jsonpage1 = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/GetEndPointDevice");
      URLConnection urlcon1 = jsonpage1.openConnection();
      DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
      DocumentBuilder db1 = dbf1.newDocumentBuilder();
      Document doc1 = db1.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/GetEndPointDevice");
      NodeList endDeviceName = doc1.getElementsByTagName("EndPointDeviceId");
      NodeList info = doc1.getElementsByTagName("DeviceInfo");
      NodeList location = doc1.getElementsByTagName("location");
        for(int i=0;i<=endDeviceName.getLength()-1;i++)
                                                { %> <option value="<%=endDeviceName.item(i).getFirstChild().getNodeValue()%>" selected="selected"><% String senval= endDeviceName.item(i).getFirstChild().getNodeValue(); 
                                               if(type=="") type=senval;
                                                out.print(senval); %></option>
                                                     <%  }                                      
                                               
                                                %> 
    </select>
    <select name="cmbtype" id="cmbtype" >
        <%
       try
       {
           if(userid==1)
            {
             
              URL jsonpage = new URL("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getSensorList");
           
        URLConnection urlcon = jsonpage.openConnection();
   
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getSensorList");
        NodeList sensorname =  doc.getElementsByTagName("sensorname");
        NodeList sensortype = doc.getElementsByTagName("sensortype");
        NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
       // NodeList alarmcount = doc.getElementsByTagName("alarmcount");
        
       
    
                                                for(int i=0;i<=sensorname.getLength()-1;i++)
                                                {
                                                    if(!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")){
                                                   
                                                        %> <option value="<%=sensorname.item(i).getFirstChild().getNodeValue()%>" selected="selected"><% String senval= sensorname.item(i).getFirstChild().getNodeValue(); 
                                                senval=senval.substring(0,senval.lastIndexOf("_"));
                                                out.print(senval); %></option>
                                                     <%
                                                    }
                                                }
            }  
           else
           {
               URL jsonpage = new URL("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+userid+"/getSensorListByUserId");
           
        URLConnection urlcon = jsonpage.openConnection();
   
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+userid+"/getSensorListByUserId");
        NodeList sensorname =  doc.getElementsByTagName("sensorname");
        NodeList sensortype = doc.getElementsByTagName("sensortype");
        NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
       // NodeList alarmcount = doc.getElementsByTagName("alarmcount");
        
       
                                                int checkJ=0;
                                                for(int i=0;i<=sensorname.getLength()-1;i++)
                                                {
                                                    if(request.getParameter("cmb")==null)
                                                    {
                                                       
                                                    }
                                                    else
                                                    {
                                                        if(request.getParameter("cmb").equals(sensorname.item(i).getFirstChild().getNodeValue()))
                                                        {checkJ=0;
                                                            %>
                                                       
                                                            <%
                                                        }
                                                    }
                                                    if(!sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay")){
                                                   
                                                    %> <option <%= checkJ++==0?" selected ":"" %> value="<%=sensorname.item(i).getFirstChild().getNodeValue()%>"><% String senval= sensorname.item(i).getFirstChild().getNodeValue(); 
                                                senval=senval.substring(0,senval.lastIndexOf("_"));
                                                out.print(senval);  %></option>
                                                     <%
                                                    }
                                                }
           }
       
       }catch(Exception e)
       {
         //  out.print(e.toString());
       }
       %>
    </select>
     &nbsp;&nbsp;&nbsp;
     <%
            int weekly = 0;
            int monthly = 0;
            int tSer = 0;
            if(request.getParameter("rad")==null)
            { 
                weekly = 1;
            }
            else 
            {
                    if(request.getParameter("rad").equals("weekly"))
                   {
                         weekly = 1;
                   }

                   if(request.getParameter("rad").equals("Monthly"))
                   {
                       monthly =1;
                   }
                   if(request.getParameter("rad").equals("time"))
                   {
                       tSer = 1;
                   }
            }
      %>
    
       <input class="cls" type="radio" id="1" name="rad" value="weekly" onclick="change()" <%= weekly++==1?" checked ":"" %>><label style=font-size:10 for="1"> Weekly</label>
            &nbsp;&nbsp;&nbsp;<input type="radio" id="2" name="rad" value="Monthly" onclick="change()" <%= monthly++==1?" checked ":"" %>><label style=font-size:10 for="2"> Monthly</label>
           &nbsp;&nbsp;&nbsp;<input type="radio" id="3" name="rad" value="time" onclick="dispDate()" <%= tSer++==1?" checked ":"" %>><label style=font-size:10 for="3"> Time Series</label>
    &nbsp;&nbsp;&nbsp;<input class="myButton"  type="submit"  name="btnSubmit" value="Plot Graph" >
      </div>
    <div id="dates" style="display: none;height:50px !important;" >
        Date From: <input type="text" name="from"  class="cls" value ='from' id="from">Date To: <input name="to"  class="cls" value='to' type="text" id="to">
            <br>
            
      </div>
</form>
    
    <script type="text/javascript" src="js/dygraph-dev.js"></script>
    <script>
        var check=0;
    <%
      if(request.getParameter("cmb")==null)
      {
          %>
              check=1;
              <%
      }
            
            %>
    </script> 
    <div style="border: #659801;width:700px;height:600px;margin-left: 70px;
border-width: 9px;
border-style: ridge; margin-top: 30px" id="graph"  >
    <div style="float:left;"><img style="margin-top:460%;cursor: pointer" src="images/pre.png" onclick="showGraph(2);" width="50" height="50"  ></div>
    <div width="120" height="150" id="div_g" style="width:80%; height:520px;float:left;"></div>
    <div style=" height:520px;"><img style="margin-top:34%;cursor: pointer;float:right" width="50" height="50" src="images/next.png" onclick="showGraph(1);"></div>
    <div style="margin-top:35px;float: right;" id="page">
        <div id="pagelist" style="float: left;margin-top: 5px">Pages 1 of 100 </div> &nbsp;||  Page <input style="display: inline !important;width:35px;height:25px;" type="text" name="txtpage" id="txtpage" > <input type="button" name="btnpage" value="Submit" id="btnpage" onclick="showGraph(0)" > 
    </div>
    </div>
    <script>
        $(function() {
             $( "#from" ).datepicker();
    $( "#from" ).datepicker("option", "dateFormat","dd-mm-yy");
  });
   $(function() {
       $( "#to" ).datepicker();
    $( "#to" ).datepicker("option", "dateFormat","dd-mm-yy");
  });
    </script>
    <%
       
    //String url="";
      
    URL url,urlcount;
    SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");
    try{
    if(request.getParameter("rad")==null)
    {
        strTemp="[]";
        %>
        <script>     document.getElementById("div_g").innerHTML="No data Available";</script>
        <%
    }
    else
    {
        
        if(request.getParameter("rad").equals("weekly"))
        {
            //   url="";
            Calendar now = Calendar.getInstance();
             now.add(Calendar.DATE, 1);
            Date dateto= now.getTime();
             now.add(Calendar.DATE, -7);
             Date datefrom=now.getTime();
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydatabydate?sensortype="+request.getParameter("cmb")+"&fromdate="+format.format(datefrom) +"&todate="+ format.format( dateto));
          //  out.print(request.getParameter("cmb")+dateto+"**"+datefrom);
               //url = new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/historydatabydate?sensortype=" + request.getParameter("cmb") + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString());
               url = new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/offsethistory?sensortype=" + request.getParameter("cmb") + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString()+"&offset=0");
               %>
               <script>
                   from='<%=format.format(datefrom).toString()%>';
                   to='<%=format.format(dateto).toString()%>';
                   sensor='<%=request.getParameter("cmb")%>';
                   offset=0;
                </script>
               <%
               urlcount=new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/countrow?sensortype="+ request.getParameter("cmb") + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString());
	}
        else if(request.getParameter("rad").equals("Monthly"))
        {
               Calendar now = Calendar.getInstance();
                now.add(Calendar.DATE, 1);
            Date dateto= now.getTime();
             now.add(Calendar.MONTH, -1);
             Date datefrom=now.getTime();
             //url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydatabydate?sensortype="+request.getParameter("cmb")+"&fromdate="+ format.format( datefrom)+"&todate="+format.format( dateto));
             url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/offsethistory?sensortype="+request.getParameter("cmb")+"&fromdate="+ format.format( datefrom)+"&todate="+format.format( dateto)+"&offset=0");
             %>
               <script>
                   from='<%=format.format(datefrom).toString()%>';
                   to='<%=format.format(dateto).toString()%>';
                   sensor='<%=request.getParameter("cmb")%>';
                   offset=0;
               </script>
               <%
               urlcount=new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/countrow?sensortype="+ request.getParameter("cmb") + "&fromdate="+format.format(datefrom).toString()+"&todate=" + format.format(dateto).toString());
            //out.print(request.getParameter("cmb")+dateto+"**"+datefrom);
        }
        
        else
        {
     //URL 
            //url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/historydatabydate?sensortype="+request.getParameter("cmb")+"&fromdate="+request.getParameter("from")+"&todate="+request.getParameter("to"));
            url = new URL("http://"+Config.url+":8080/scheduler.core/rest/services/offsethistory?sensortype="+request.getParameter("cmb")+"&fromdate="+request.getParameter("from")+"&todate="+request.getParameter("to")+"&offset=0");
            //out.print(url.toString());
%>
               <script>
                   from='<%=request.getParameter("from")%>';
                   to='<%=request.getParameter("to")%>';
                   sensor='<%=request.getParameter("cmb")%>';
                   offset=0; 
               </script>
               <%
            urlcount=new URL("http://" + Config.url + "/scheduler.core/rest/services/countrow?sensortype="+ request.getParameter("cmb") + "&fromdate="+request.getParameter("from")+"&todate="+request.getParameter("to"));
        }
        
        
        strTemp=url.toString();
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
        int b2 = 0;
        strTemp = br.readLine();
        
        if(strTemp.trim()=="[]")
        {
          
        }
    
        strCount=urlcount.toString();
        BufferedReader br1 = new BufferedReader(new InputStreamReader(urlcount.openStream()));
        
        strCount = br1.readLine();
        
         //   Double b1 = Double.parseDouble(strTemp);
         //  b2 = b1.intValue();
        
        //out.print(strTemp);
        //System.out.println("jjj"+strTemp);
    
    // String strTemp = "";
   
        }
    }
    catch(Exception e)
            {
                strTemp="[]";
            }
        }
        else
        {
        %>
        <script>window.location='index.jsp';</script>
        <%
        }
              
        %>
    <script>  
       // alert('hai');
var liveData;
      var data=[];
       var t = new Date();
       
 var data11='<%=strTemp %>';
 count='<%=strCount%>';
 $("#pagelist").html("Pages 1 of "+(parseInt(count/1000)==0?"1":parseInt(count/1000)));
 if((parseInt(count/1000)==0))
              {
                  $("#btnpage").css("display","none !important");
                  $("#txtpage").css("display","none !important");
              }
 //alert(data11);
 livedata(1,data11);
 <%
        NodeList xlabel=null;
            NodeList ylabel=null;
        NodeList gmin=null;
        NodeList gmax=null;
        try{
     
           DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        DocumentBuilder db1 = dbf1.newDocumentBuilder();
        Document doc1 = db1.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+session.getAttribute("userid")+"/"+request.getParameter("cmb")+"/getSensorListByName");
         xlabel =  doc1.getElementsByTagName("xlabel");
         ylabel = doc1.getElementsByTagName("ylabel");
         gmin = doc1.getElementsByTagName("gmin");
         gmax = doc1.getElementsByTagName("gmax");
        }
        catch(Exception e)
        {}
        %>
            
            
            
             xlabels= '<%if(request.getParameter("cmb")!=null)out.print(xlabel.item(0).getFirstChild().getNodeValue());%>';
                            ylabels= '<%if(request.getParameter("cmb")!=null)out.print(ulabel.item(0).getFirstChild().getNodeValue());%>';
                            valueRanges= [<%if(request.getParameter("cmb")!=null)out.print(gmin.item(0).getFirstChild().getNodeValue());%>,<%if(request.getParameter("cmb")!=null)out.println(gmax.item(0).getFirstChild().getNodeValue());%>];
                            
                            titles='<% if(request.getParameter("cmb")!=null)
                                             out.print(request.getParameter("cmb").substring(0, request.getParameter("cmb").lastIndexOf("_")));%>';
            
            
var g = new Dygraph(
          document.getElementById("div_g"),
          data,
          {
            
            drawPoints: true,
                            showRoller: true,
                            xlabel: '<%if(request.getParameter("cmb")!=null)out.print(xlabel.item(0).getFirstChild().getNodeValue());%>',
                            ylabel: '<%if(request.getParameter("cmb")!=null)out.print(ylabel.item(0).getFirstChild().getNodeValue());%>',
                            valueRange: [<%if(request.getParameter("cmb")!=null)out.print(gmin.item(0).getFirstChild().getNodeValue());%>,<%if(request.getParameter("cmb")!=null)out.println(gmax.item(0).getFirstChild().getNodeValue());%>],
                            labels: ['Time', 'Value'],
                            title:'<% if(request.getParameter("cmb")!=null)
                                             out.print(request.getParameter("cmb").substring(0, request.getParameter("cmb").lastIndexOf("_")));%>',
            showRangeSelector: true,
            rangeSelectorHeight: 30,
            rangeSelectorPlotStrokeColor: 'yellow',
            rangeSelectorPlotFillColor: 'lightyellow'
          }
      );
      if(data11.trim().length==2)
    
          //if(check==0)
          { $("#div_g").html("No data Available");
              $("#graph").html("No Data Available");
              
          }
     /* else
          $("#div_g").html(""); */
           function livedata(id,data11)
{
    //alert(data11);
   //alert($("#roll14").html());
   data11=data11.trim();
  // alert(data11.length);
   if(data11.length!=2){
       
       //alert(data);
    if(id==3)
        data=[];
    //alert(data);
   // alert("10");
    data11=data11.trim();
    //  alert("Data: " + data11 + "\nStatus: " + status);
      // var data11='<%=strTemp%>';
       // alert(data11);
        var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       // var dat =dt.split(",");
       var dates=[];
       var vals=[];
       //alert(dt.length);
        for(var i=0;i<dt.length-2;i++)
        {
          
           if(id==2) data.shift();
            //alert(dt[i].length-dt[i].indexOf("=")+1);
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
          //  alert(dates[i]);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
            dts=dates[i].split("T");
           // alert("***"+dts[0]+"$$$"+dts[1]+"&&");
            dts1=dts[0].split("-");
            dts2=dts[1].split("+");
            dts3=dts2[0].split(":");
         //   dts4=dts2[2].split(".");
           // data.push([new Date(new Date().getTime()-i*1000), vals[i]]);
        //   alert(dts3[0]+"&&"+dts3[1]+"^^"+dts3[2]);
            data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2]), vals[i]]);
          //  alert(new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0));
          // alert(new Date(new Date().getTime()-i*1000));
         //alert(new Date((dates[i].replace(" ","T"))));
        // t=new Date();
        // t=new Date();
        // var x = new Date(t.getTime() - i * 1000);
          //        data.push([x, Math.random()*40]);
        }
        if(id==3)
               { 
                   g = new Dygraph(
          document.getElementById("div_g"),
          data,
          {
            
            drawPoints: true,
                            showRoller: true,
                            xlabel: xlabels,
                            ylabel: ylabels,
                            valueRange: valueRanges,
                            labels: ['Time', 'Value'],
                            title:titles,
            showRangeSelector: true,
            rangeSelectorHeight: 30,
            rangeSelectorPlotStrokeColor: 'yellow',
            rangeSelectorPlotFillColor: 'lightyellow'
          }
      );
               }
     
 }
 else
 {
     $("#div_g").html("No data Available");
    // clearInterval(liveData);
 }
}

        
   
            </script>
            <style>
.dygraph-axis-label-x
{
 // width:130px;
 // height:70px;
 // margin-left: -10px;
 // -ms-transform:rotate(270deg); /* IE 9 */
 // -moz-transform:rotate(270deg); /* Firefox */
 // -webkit-transform:rotate(270deg); /* Safari and Chrome */
 // -o-transform:rotate(270deg); /* Opera */
}
.dygraph-xlabel{
    margin-top: 30px;
}
</style>