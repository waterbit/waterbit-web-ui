
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URL"%>
<%@page import="org.gadgeon.Config"%>

<script type="text/javascript" src="js/dygraph-dev.js"></script>

<script src="js/smooth-plotter.js"></script>
<div id="div_g" style="width:95%; height:85%; background-color: #FFF;"></div>
<style>
    input{display:none !important;}
    input[ type="checkbox"]{display:inline !important;}
    input[ type="radio"]{display:inline !important;}
    input[ type="button"]{display:inline !important;}
</style>
<script type="text/javascript">
    var g;
    var data = [];
    var t = new Date();
    var sensor = '<%=currentSensor%>';
    var sensor1;
//alert('currentSensor+currentSensorType');

    <%
String strTemp = "";
if (!currentSensorType.equalsIgnoreCase("relay")) {
    try {
   // URL  url = new URL("http://vitalherd.gadgeon.com:8080/scheduler.core/rest/services/offsethistory?sensortype=ed563415_rumentemperature&fromdate=10-02-2015&todate=19-02-2015&offset=158");
        //  URL url = new URL("http://"+Config.url+":22001/vs/vsensor/"+currentSensor+"/historydemo");
        URL url = new URL("http://" + Config.url + ":8080/scheduler.core/rest/services/historydata?sensortype=" + currentSensor);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        int b2 = 0;
        strTemp = br.readLine();

        if (strTemp.trim() == "[]") {

        }
     //   Double b1 = Double.parseDouble(strTemp);
        //  b2 = b1.intValue();

    //out.print(strTemp);
        //System.out.println("jjj"+strTemp);
    } catch (Exception e) {
        strTemp = "[]";

    }
} else {
    strTemp = "[]";
}
// String strTemp = "";

    %>

    var data11 = '<%=strTemp%>';



    /*    var details=data11.substr(1,data11.length);
     details=details.substr(0,details.length-2);
     var dt=details.split("},");
     // var dat =dt.split(",");
     var dates=[];
     var vals=[];
     
     for(var i=dt.length-1;i>=0;i--)
     {
     
     //l(dt[i].length-dt[i].indexOf("=")+1);
     dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
     //alert(dates[i]);
     vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
     data.push([new Date((dates[i].replace(" ","T"))), vals[i]]);
     //alert(new Date((dates[i].replace(" ","T"))));
     // var x = new Date(t.getTime() - i * 1000);
     //  data.push([x, Math.random()*40]);
     }
     
     */
    //  alert(data.length);
if(sensor.trim()!=''){
    $.get("graphdetail.jsp?sensor=" + sensor, function (data12, status) {
        data12 = data12.trim().split(":");
        //alert("haii@@@i"+data12[0])
        xval = data12[0];
        yval = data12[1];
        min = data12[2];
        max = data12[3];
//     alert(data);
        livedata(sensor, data11);
        /* if(data.length!=0)
         g = new Dygraph(document.getElementById("div_g"), data,
         {
         drawPoints: true,
         showRoller: true,
         ylabel: yval,
         xlabel: xval,
         valueRange: [min, max],
         stackedGraph: true,
         labels: ['Time', 'Value'],
         interactionModel: Dygraph.Interaction.nonInteractiveModel_
         });*/
    });
    }

    /* setInterval(function() {
     var x = new Date();  // current time
     var y = Math.random()*40;
     data.push([x, y]);
     data.shift();
     g.updateOptions( { 'file': data } );
     }, 1000);*/

    liveDataInt = setInterval(function () {

        $.get("livedatademo.jsp?sensor=" + sensor, function (data11, status) {

            // alert(data11); 
            livedata(sensor, data11);

        });
    }, 60000
            );
 smoothPlotter.smoothing =.7;
    function callfunc(yv, xv, ma, mi)
    {
//alert("#########"+yv)
//sleep(1000);
        var yvalue = yv;
        var xvalue = xv;
        var max = ma;
        var min = mi;
        if (data.length != 0) {
            g = new Dygraph(document.getElementById("div_g"), data,
                    {
                        showRoller: true,
                        ylabel: yvalue,
                        xlabel: xvalue,
                        titles: "",
                        valueRange: [max, min],
                        stackedGraph: true,
                        labels: ['Time', 'Value'],
                        interactionModel: Dygraph.Interaction.nonInteractiveModel_,
                        plotter: smoothPlotter,
                        strokeWidth: 2
                    });
                    livedata_data=data;
                    livedata_options={
                        showRoller: true,
                        ylabel: yvalue,
                        xlabel: xvalue,
                        titles: "",
                        valueRange: [max, min],
                        stackedGraph: true,
                        labels: ['Time', 'Value'],
                        interactionModel: Dygraph.Interaction.nonInteractiveModel_,
                        plotter: smoothPlotter,
                        strokeWidth: 2
                    };
        }

    }
   function livedata(id,data11)
{
     smoothPlotter.smoothing =.7;
     $.ajaxSetup ({
                            // Disable caching of AJAX responses */
                            cache: false
                    });
                    
                    //alert(id.indexOf("rumencontraction"));
        data11=data11.trim();
        var datalist=data11.split("***");
        
        data11=datalist[0];
        var  lastSeen=0;
        if(datalist.length==2)
            lastSeen=datalist[1];
        if(data11.length!=2){
            
             if(id.indexOf("rumencontraction")>-1){

        var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       if($("#div_g").html()=="No data Available")data=[];
       if($("#div_g").html()!="No data Available"&&id==3)
           id=2;
       var dates=[];
       var vals=[];
           data=[];
       var lastDate;
        for(var i=0;i<dt.length;i++)
        {
           
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
            dts=dates[i].split("T");
            dts1=dts[0].split("-");
            dts2=dts[1].split("Z");
            dts3=dts2[0].split(":")
       
            data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0), parseFloat(vals[i])]);
            lastDate=new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0);
        }
        
          

           {  
               g = new Dygraph(document.getElementById("div_g"), data,
                          {
                            drawPoints: false,
                            showRoller: true,
                            ylabel:yval,
                            xlabel: xval,
                             stackedGraph: true,
                             title:"<div style='float:right;font-size:16px;font-weight: normal;' >Time since last data : "+getDataHR(parseInt(lastSeen))+"</div> ",
                            valueRange: [min, max],
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			     plotter: smoothPlotter,
                        strokeWidth: 2
                          });
                          livedata_data=data;
                    livedata_options={
                            drawPoints: false,
                            showRoller: true,
                            ylabel:yval,
                            xlabel: xval,
                             stackedGraph: true,
                             title:"<div style='float:right;font-size:16px;font-weight: normal;' >Time since last data : "+getDataHR(parseInt(lastSeen))+"</div> ",
                            valueRange: [min, max],
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			     plotter: smoothPlotter,
                        strokeWidth: 2
                          };
                          g.ready(function() {
                               var annotations = [];
                 for(var i=0;i<dt.length;i++)
                    {
                        

                        dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
                        vals[i]=dt[i].substr(dt[i].indexOf("counter=")+8);
                        dts=dates[i].split("T");
                        dts1=dts[0].split("-");
                        dts2=dts[1].split("Z");
                        dts3=dts2[0].split(":")

                        //data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0), parseFloat(vals[i])]);
                        //lastDate=new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0);
                        
                        annotations.push( 
                        {
                         series:"Value",
                          x: new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0).getTime(),
                          shortText:  parseFloat(vals[i]),
                          text: "Count :"+ parseFloat(vals[i])
                        });
                        
                    }
                     g.setAnnotations(annotations);
                    
                          });
                          }
                          
            }
            else
            {
                var details=data11.substr(1,data11.length);
        details=details.substr(0,details.length-2);
        var dt=details.split("},");
       if($("#div_g").html()=="No data Available")data=[];
       if($("#div_g").html()!="No data Available"&&id==3)
           id=2;
       var dates=[];
       var vals=[];
           data=[];
       var lastDate;
        for(var i=0;i<dt.length;i++)
        {
           
            dates[i]=dt[i].substr(dt[i].indexOf("=")+1,dt[i].indexOf(",")-dt[i].indexOf("=")-1);
            vals[i]=dt[i].substr(dt[i].indexOf("y1=")+3);
            dts=dates[i].split("T");
            dts1=dts[0].split("-");
            dts2=dts[1].split("Z");
            dts3=dts2[0].split(":")
       
            data.push([new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0), parseFloat(vals[i])]);
            lastDate=new Date(dts1[0],dts1[1]-1,dts1[2],dts3[0],dts3[1],dts3[2],0);
        }
        
          
            var sensortype=id.split("_");
             var type=sensortype[1];
             if(type=="ap")
                 {
                     
                g = new Dygraph(document.getElementById("div_g"), data,
                        {
			    axes: {
      y: {
        ticker: function(min, max, pixels, opts, dygraph, vals) {
          return [{v:0, label:"0"}, {v:1, label:"1"}];
        }
      }
  },
  drawGrid:false,
                            stepPlot:true,
                            showRoller: true,
                            ylabel: yval,
                            xlabel: xval,
                            stackedGraph: true,
                            title: "<div style='float:right;font-size:16px;font-weight: normal;' >Time since last data : " + getDataHR(parseInt(lastSeen)) + "</div> ",
                            valueRange: [min, max],
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			    strokeWidth: 2
                           
                        });
                        livedata_data=data;
                    livedata_options={
			    axes: {
                                y: {
                                  ticker: function(min, max, pixels, opts, dygraph, vals) {
                                    return [{v:0, label:"0"}, {v:1, label:"1"}];
                                  }
                                }
                            },
                            drawGrid:false,
                            stepPlot:true,
                            showRoller: true,
                            ylabel: yval,
                            xlabel: xval,
                            stackedGraph: true,
                            title: "<div style='float:right;font-size:16px;font-weight: normal;' >Time since last data : " + getDataHR(parseInt(lastSeen)) + "</div> ",
                            valueRange: [min, max],
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			    strokeWidth: 2
                           
                        };
                    }
                    else
                    {
               g = new Dygraph(document.getElementById("div_g"), data,
                          {
                            drawPoints: false,
                            showRoller: true,
                            ylabel:yval,
                            xlabel: xval,
                             stackedGraph: true,
                             title:"<div style='float:right;font-size:16px;font-weight: normal;' >Time since last data : "+getDataHR(parseInt(lastSeen))+"</div> ",
                            valueRange: [min, max],
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			     plotter: smoothPlotter,
                        strokeWidth: 2
                          });
                          
                      livedata_data=data;
                    livedata_options={
                            drawPoints: false,
                            showRoller: true,
                            ylabel:yval,
                            xlabel: xval,
                             stackedGraph: true,
                             title:"<div style='float:right;font-size:16px;font-weight: normal;' >Time since last data : "+getDataHR(parseInt(lastSeen))+"</div> ",
                            valueRange: [min, max],
                            labels: ['Date', 'Value'],
                            interactionModel: Dygraph.Interaction.nonInteractiveModel_,
			     plotter: smoothPlotter,
                            strokeWidth: 2
                          };
                }
                          
            }
           
       //  g.updateOptions({} );
 }
 else
 {
     
     $("#div_g").html("No data Available since 5 hours");
    
 }
  if(mnc==0)
    {
	NProgress.set(1);
    }
	mnc=1;
	//alert(mnc);
}

    function getDataHR(newMinutes) {
        MINS_PER_YEAR = 24 * 365 * 60;
        MINS_PER_MONTH = 24 * 30 * 60;
        MINS_PER_WEEK = 24 * 7 * 60;
        MINS_PER_DAY = 24 * 60;
        MINS_PER_HRS = 60;
        minutes = newMinutes;
        /*years = Math.floor(minutes / MINS_PER_YEAR);
         minutes = minutes - years * MINS_PER_YEAR;
         months = Math.floor(minutes / MINS_PER_MONTH);
         minutes = minutes - months * MINS_PER_MONTH;
         weeks = Math.floor(minutes / MINS_PER_WEEK);
         minutes = minutes - weeks * MINS_PER_WEEK;*/
        days = Math.floor(minutes / MINS_PER_DAY);
        minutes = minutes - days * MINS_PER_DAY;
        hours = Math.floor(minutes / MINS_PER_HRS);
        minutes = minutes - hours * MINS_PER_HRS;
        return  days + " Days " + hours + " hrs " + minutes + " mins";
//return hrData; // 1 year, 2 months, 2 week, 2 days, 12 minutes
    }
</script>


<style>
    .dygraph-axis-label-x
    {
        width:130px;
        height:70px;
        margin-left: -10px;
        -ms-transform:rotate(270deg); /* IE 9 */
        -moz-transform:rotate(270deg); /* Firefox */
        -webkit-transform:rotate(270deg); /* Safari and Chrome */
        -o-transform:rotate(270deg); /* Opera */

    }
    .dygraph-axis-label {
        font-size: 13px;
    }
    .dygraph-xlabel{
        margin-top: 30px;
        font-size: 14px;
    }
    .dygraph-ylabel{

        font-size: 15px;
    }
</style>

