<%-- 
    Document   : getsensorList
    Created on : 12 Dec, 2014, 10:54:32 AM
    Author     : shamly
--%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>


        <% 
        
        URL jsonpage = new URL("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/getSensorList");
        URLConnection urlcon = jsonpage.openConnection();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse("http://"+Config.url+":"+Config.port+"/GadgeonREST/rest/services/"+session.getAttribute( "userid" )+"/getSensorListByUserId");
        NodeList sensorname =  doc.getElementsByTagName("sensorname");
        NodeList sensortype = doc.getElementsByTagName("sensortype");
        NodeList sensorinfo = doc.getElementsByTagName("sensorinfo");
        NodeList sensorstatus = doc.getElementsByTagName("status");
        String val="";
        for(int i=0;i<sensorname.getLength();i++)
        {
           if( sensortype.item(i).getFirstChild().getNodeValue().equalsIgnoreCase("relay"))
           {val+= sensorname.item(i).getFirstChild().getNodeValue()+":"+sensorstatus.item(i).getFirstChild().getNodeValue()+";";
           }
           
        }
        out.println(val);
        %>
    
   