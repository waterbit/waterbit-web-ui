<%@page import="java.net.URLConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.gadgeon.Config"%>
<%
    try {
        String strUsrName = request.getParameter("username");
        String strPsswd = request.getParameter("password");
        URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + strUsrName + "/" + strPsswd + "/userlogin");
        URLConnection urlcon = jsonpage.openConnection();
        BufferedReader buffread = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));
        String strTemp = "";
        String strs = "";
        int userid = 0;
        while ((strTemp = buffread.readLine()) != null) {
            strs = strTemp;
            int b1 = Integer.parseInt(strTemp);
            userid = b1;
        }
        int id = userid;
        session.setAttribute("userid", id);
        if (userid > 1) {
            response.sendRedirect("userDashBoard.jsp");
        } else if (userid == 1) {
            response.sendRedirect("dashBoard.jsp");
        } else {
            response.sendRedirect("index.jsp?val=0");
        }
    } catch (Exception ex) {
        response.sendRedirect("index.jsp?val=0");
    }
%>

