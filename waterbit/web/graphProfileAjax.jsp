    
<div id="result">
    <%@page import="org.w3c.dom.Node"%>
    <%@page import="org.w3c.dom.NodeList"%>
    <%@page import="org.w3c.dom.Document"%>
    <%@page import="javax.xml.parsers.DocumentBuilder"%>
    <%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
    <%@page import="java.net.URLConnection"%>
    <%@page import="java.io.InputStreamReader"%>
    <%@page import="java.net.URL"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="org.gadgeon.Config"%>
    <%
        int f = 0;
        NodeList profileName = null;
        NodeList xLabel = null;
        NodeList yLabel = null;
        NodeList min = null;
        NodeList max = null;
            String sensortype = request.getParameter("sensortype");

            URL jsonpage = new URL("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype + "/GetProfileByType");
            URLConnection urlcon = jsonpage.openConnection();

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse("http://" + Config.url + ":" + Config.port + "/GadgeonREST/rest/services/" + sensortype + "/GetProfileByType");

            profileName = doc.getElementsByTagName("graphprofile");
            xLabel = doc.getElementsByTagName("xlabel");
            yLabel = doc.getElementsByTagName("ylabel");
            min = doc.getElementsByTagName("min");
            max = doc.getElementsByTagName("max");
       

    %> 
    <div class="panel panel-default"  style="border-bottom: none; background-color: #dff0d8;">
        <div class="panel-heading">
            Graph Profile
        </div>
        <form action="addNewGraphProfileAction.jsp" method="post"  onsubmit="return(validatetxtbox())">
            <div class="panel-body">
                <div class="table-responsive"><table class="table">
                             <%                                               
        if(profileName.getLength()!=0)
        {

                                        %>
                        <tbody>
                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Profile </td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><select name="graphprofile" id="graphprofile" onchange="return(graphProfileByName())">
                                        <%                                                 for (int i = 0; i <= profileName.getLength() - 1; i++) {

                                        %>
                                        <option value="<%=profileName.item(i).getFirstChild().getNodeValue()%>"
                                                selected> <%=profileName.item(i).getFirstChild().getNodeValue()%>
                                        </option>

                                        <%
                                            }
                                        %>
                                    </select></td>
           
                            </tr>
                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">X-Label</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="xLabel" id="xLabel" readonly="true" value="<%=xLabel.item(profileName.getLength() - 1).getFirstChild().getNodeValue()%>"/></td>
                                <td></td>
                            </tr>

                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Y-Label</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"> <input type="text" name="yLabel" id="yLabel" readonly="true" value="<%=yLabel.item(profileName.getLength() - 1).getFirstChild().getNodeValue()%>"/></td>
              
                            </tr>

                            <tr class="success">
                                <td style="border: #599bb3; border-style: dashed; border-width: 1px;">Value Range - Min</td> <td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type= "number" name="min" id="min" readonly="true" value="<%=min.item(profileName.getLength() - 1).getFirstChild().getNodeValue()%>"/></td>
                            </tr>
			    <tr class="success">
				<td style="border: #599bb3; border-style: dashed; border-width: 1px;">Value Range - Max</td><td style="border: #599bb3; border-style: dashed; border-width: 1px;"><input type= "number" name="max" id="max" readonly="true" value="<%=max.item(profileName.getLength() - 1).getFirstChild().getNodeValue()%>"/></td>
			    </tr>

                        </tbody>
                         <%                                               
                         }else{
            
            out.println("No graph profile for selected sensor type");
        }
                                         %>
                    </table>
                    <font color='red'> <div id="une"> </div> </font>
                </div>

            </div>
        </form>
    </div>